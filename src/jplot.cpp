
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jplot.h"

JPlot::JPlot(float w, float h):
    JWidget(w, h), grid_type(JGridType::None), grid_steps(10)
{
    samples_positions.reserve(20);
}

JPlot::JPlot(float x, float y, float w, float h):
    JWidget(x, y, w, h), grid_type(JGridType::None), grid_steps(10)
{
    samples_positions.reserve(20);
}

JPlot::~JPlot() {
    ofRemoveListener(ofEvents().mousePressed, this, &JPlot::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased,  this, &JPlot::releasedCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JPlot::draggedCbk);
    ofRemoveListener(ofEvents().keyPressed, this, &JPlot::keyPressed);
    ofRemoveListener(ofEvents().keyReleased, this, &JPlot::keyReleased);
}

void JPlot::setup() {
    ofAddListener(ofEvents().mousePressed, this, &JPlot::clickedCbk);
    ofAddListener(ofEvents().mouseReleased,  this, &JPlot::releasedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JPlot::draggedCbk);
    ofAddListener(ofEvents().keyPressed, this, &JPlot::keyPressed);
    ofAddListener(ofEvents().keyReleased, this, &JPlot::keyReleased);
}
void JPlot::update() {}

void JPlot::setGridSteps(int gs)
{
    grid_steps = gs;
}

void JPlot::draw()
{
    const float halfX = (posx + (width / 2));
    const float halfY = (posy + (height / 2));

    ofNoFill();
    ofSetColor(JCOLOR_WHITE);
    ofDrawRectangle(posx, posy , width, height);

    drawGrid(halfX, halfY);

    drawSamples();
}

void JPlot::drawGrid(const float halfX, const float halfY)
{
    if(grid_type == JGridType::None)
        return;

    // if grid is enabled, draw grid
    ofSetColor(JCOLOR_WHITE);
    if(grid_type == JGridType::Cross)
    {
        ofDrawLine(posx, halfY, posx + width, halfY);
        ofDrawLine( halfX, posy,  halfX, posy + height);
    } else if(grid_type == JGridType::Full)
    {
        //ofDrawLine(posx, halfY, posx + width, halfY);
        //ofDrawLine( halfX, posy,  halfX, posy + height);

        ofSetLineWidth(0.5);
        ofSetColor(JCOLOR_DARKGREY);

        const float ymult = height / grid_steps;
        const float xmult = width / grid_steps;
        ofNoFill();
        for(int i = 1; i <grid_steps; i++)
        {
            // from bottom to top and left to right
            ofDrawLine(posx + (xmult * i), posy, posx + (xmult * i), posy + height);
            ofDrawLine(posx, (posy + height) - (ymult * i), posx + width, (posy + height) - (ymult * i));

        }
    }
}

void JPlot::drawSamples()
{
    // first sort x positions
    std::sort(samples_positions.begin(), samples_positions.end(), [](glm::vec2 i1, glm::vec2 i2) {
        return i1.x < i2.x;
    });

    ofSetColor(JCOLOR_RED);
    ofFill();
    float px, py;
    for(auto & it : samples_positions)
    {
        px = posx + (it.x * width);
        py = (posy + height) - (it.y * height);
        ofDrawCircle(px, py, 0, 4);
    }
}

void JPlot::setGridType(JGridType grid) {grid_type = grid;}

void JPlot::clear() {samples_positions.clear();}

void JPlot::clickedCbk(ofMouseEventArgs &mouse)
{

}

void JPlot::releasedCbk(ofMouseEventArgs &mouse)
{

}

void JPlot::draggedCbk(ofMouseEventArgs &mouse)
{

}

void JPlot::keyPressed(ofKeyEventArgs &key)
{

}

void JPlot::keyReleased(ofKeyEventArgs &key)
{

}
