
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jmultiscope.h"

template<typename T>
JMultiScope<T>::JMultiScope():
    JWidget(), grid_type(JGridType::None)
{
}

template<typename T>
JMultiScope<T>::JMultiScope(float w, float h):
    JWidget(w, h), grid_type(JGridType::None)
{
}


template<typename T>
JMultiScope<T>::JMultiScope(float x, float y, float w, float h):
    JWidget(x, y, w, h), grid_type(JGridType::None)
{
}

template<typename T>
JMultiScope<T>::~JMultiScope(){}

template<typename T>
void JMultiScope<T>::setGridtype(JGridType t) {grid_type = t;}


template<typename T>
void JMultiScope<T>::addSignal(JScopedSignalDescriptor<T> sig)
{
    signal_list.push_back(sig);
}

template<typename T>
void JMultiScope<T>::drawGrid(const T halfX, const T halfY)
{
    if(grid_type == JGridType::None) return;
    // if grid is enabled, draw grid
    ofSetColor(JCOLOR_WHITE);
    if(grid_type == JGridType::Cross)
    {
        ofDrawLine(posx, halfY, posx + width, halfY);
        ofDrawLine( halfX, posy,  halfX, posy + height);
    } else if(grid_type == JGridType::Full)
    {
        ofDrawLine(posx, halfY, posx + width, halfY);
        ofDrawLine( halfX, posy,  halfX, posy + height);

        ofSetLineWidth(0.5);
        ofSetColor(JCOLOR_DARKGREY);

        const float ymult = height / 10;
        const float xmult = width / 10;
        ofNoFill();
        for(int i = 1; i <= 5; i++)
        {
            // horizontal lines
            ofDrawLine(posx,  halfY + (ymult * i), posx + width, halfY +(ymult * i));
            ofDrawLine(posx,  halfY + -(ymult * i), posx + width, halfY + -(ymult * i));
            ofDrawLine(halfX + (xmult * i), posy, halfX + (xmult * i), posy + height);
            ofDrawLine(halfX + -(xmult * i), posy, halfX + -(xmult * i), posy + height);
        }
    }

}

template<typename T>
void JMultiScope<T>::setup(){

}

template<typename T>
void JMultiScope<T>::update() {}

template<typename T>
void JMultiScope<T>::draw()
{
    ofNoFill();
    ofSetColor(JCOLOR_WHITE);

    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);

    ofDrawRectangle(posx, posy, 0, width, height);
    const T halfY = (float) posy + (height / 2);
    const T halfX = (float) posx + (width / 2);

    // draw sig
    float x1, y1, y2;

    for(size_t s = 0; s < signal_list.size(); s++)
    {

        x1 = posx;
        const T x_grain = (float)width / signal_list[s].data->size();
        ofSetColor(signal_list[s].color);

        // draw legend in the process
        if(draw_legend)
        {
            drawLegend(s);
        }

        for(size_t i = 1; i < signal_list[s].data->size(); i++)
        {
            y1 =  halfY - ((*signal_list[s].data)[i - 1] * (height / 2 ) );
            y2 =  halfY - ((*signal_list[s].data)[i] * (height / 2) );

            // draw segment of signal
            if(getCoord().isInBounds(x1, y1) && getCoord().isInBounds(x1 + x_grain, y2))
                ofDrawLine( glm::vec2(x1, y1), glm::vec2(x1 + x_grain, y2));

            x1 += x_grain;
        }
    }

    drawGrid(halfX, halfY);
}


// In order to make it easier to draw legend, signals names must be 10 chars maximum
template<typename T>
void JMultiScope<T>::drawLegend(const size_t index)
{
    const float offset = posy + 15;
    const float legend_height = 15;

    switch(legend_position)
    {
    case JCornerPosition::TopLeft:
    {
        const float y = offset + (index * legend_height);
        const float x = 10;
        ofDrawBitmapString(signal_list[index].name, x, y, 0);
        ofDrawLine(x + (12 * 8), y, x + (12 * 8) + 40, y);
        break;
    }
    case JCornerPosition::TopRight:
    {
        const float y = offset + (index * legend_height);
        const float x = width - 160;
        ofDrawBitmapString(signal_list[index].name, x, y, 0);
        ofDrawLine(x + (12 * 8), y , x + (12 * 8) + 40, y);
        break;
    }
    case JCornerPosition::BottomLeft:
    {
        const float rev_idx = (signal_list.size() - index);
        const float y  = (posy + height) - (rev_idx * legend_height);
        const float x = 10;
        ofDrawBitmapString(signal_list[index].name, x, y, 0);
        ofDrawLine(x + (12 * 8), y, x + (12 * 8) + 40, y);
        break;
    }
    case JCornerPosition::BottomRight:
    {
        const float rev_idx = (signal_list.size() - index);
        const float y  = (posy + height) - (rev_idx * legend_height);
        const float x = width - 160;
        ofDrawBitmapString(signal_list[index].name, x, y, 0);
        ofDrawLine(x + (12 * 8), y , x + (12 * 8) + 40, y);
        break;
    }
    }
}


template<typename T>
void JMultiScope<T>::setLegend(bool l, JCornerPosition pos) {
    draw_legend = l;
    legend_position = pos;
}


template class JMultiScope<float>;
template class JMultiScope<double>;
