#include "jwaitanimation.h"
#include"jutils.h"

JWaitAnimation::JWaitAnimation( float w, float h) :
    posx(0), posy(0), width(w), height(h),
    mode(JWaitAnimationType::ProgressBar)
{

}

JWaitAnimation::JWaitAnimation(float x, float y, float w, float h) :
    posx(x), posy(y), width(w), height(h),
    mode(JWaitAnimationType::ProgressBar)
{

}

JWaitAnimation::~JWaitAnimation() {}

void JWaitAnimation::setup() {}

void JWaitAnimation::update() {

}

void JWaitAnimation::draw()
{
    // draw background
    ofEnableAlphaBlending();
    ofSetColor(back_color);
    ofFill();
    ofDrawRectangle(posx, posy, width, height);

    // now draw animation

    switch(mode)
    {
    case JWaitAnimationType::ProgressBar:
    {
        // display a load bar (rounded borders)
        const float radius = 20;
        const float bar_width = 250;
        const float bar_height = 25;
        ofNoFill();
        ofSetColor(ofColor::white);
        const float px = posx + (width / 2) - (bar_width / 2);
        const float py = posy + (height / 2)  - (bar_height / 2);
        ofSetCircleResolution(100);
        ofDrawRectRounded(px, py, bar_width, bar_height, radius);

        ofFill();
        ofSetColor(fill_color);
        ofDrawRectRounded(px, py, bar_width * index, bar_height, radius);

        break;
    }
    case JWaitAnimationType::ProgressBarDisplayValue:
    {
        // display a load bar (rounded borders)
        const float radius = 20;
        const float bar_width = 250;
        const float bar_height = 25;
        ofNoFill();
        ofSetColor(ofColor::white);
        const float px = posx + (width / 2) - (bar_width / 2);
        const float py = posy + (height / 2)  - (bar_height / 2);
        ofDrawRectRounded(px, py, bar_width, bar_height, radius);

        ofFill();
        ofSetColor(fill_color);
        ofSetCircleResolution(100);
        ofDrawRectRounded(px, py, bar_width * index, bar_height, radius);

        ofSetColor(text_color);
        text_to_display = ofToString(int(index * 100)) + std::string(" %");
        ofDrawBitmapString(text_to_display, px + (bar_width / 2) - ((text_to_display.size() * 8 )/ 2), py + (bar_height / 2) + 5.5);

        break;
    }

    case JWaitAnimationType::SimpleCircular:
    {
        const float radius = 40.0f;
        const float centerX = posx + (width / 2);
        const float centerY = posx + (height / 2);

        // for now just display a point
        //const float px = centerX + (radius * cos(index * 360));
        //const float py = centerY + (radius * sin(index * 360));
        //ofSetColor(fill_color);
        //ofDrawCircle(px, py, 3);

        // Just draw px py for an amount of - 60 + 60 with different alpha
        ofSetColor(fill_color);
        ofFill();
        ofSetLineWidth(10.0);
        float px, py, ppx, ppy;
        for (int i = 0; i < 10; ++i) {
            px = centerX + (radius * cos( (index + ((float)i  / 100.0) ) * 10.0 ));
            py = centerY + (radius * sin( (index + ((float)i / 100.0)) * 10.0 ));
            ppx = centerX + (radius * cos( (index + ((float)(i + 1)  / 100.0) ) * 10.0 ));
            ppy = centerY + (radius * sin( (index + ((float)(i + 1) / 100.0)) * 10.0 ));

            ofColor c (fill_color);

            int alpha = (::abs(float(float(i))) / 10.0f) * 255.0f;
            c.a = alpha;
            ofSetColor(c);

            ofDrawLine(ppx, ppy, px, py);
        }

        break;
    }
    case JWaitAnimationType::SimpleCircularDisplayValue:
    {
        const float radius = 40.0f;
        const float centerX = posx + (width / 2);
        const float centerY = posx + (height / 2);

        // for now just display a point
        //const float px = centerX + (radius * cos(index * 360));
        //const float py = centerY + (radius * sin(index * 360));
        //ofSetColor(fill_color);
        //ofDrawCircle(px, py, 3);

        ofPolyline line;
        // Just draw px py for an amount of - 60 + 60 with different alpha

        ofSetColor(fill_color);
        ofFill();
        ofSetLineWidth(10.0);
        float px, py, ppx, ppy;
        for (int i = 0; i < 10; ++i) {
            px = centerX + (radius * cos( (index + ((float)i  / 100.0) ) * 10.0 ));
            py = centerY + (radius * sin( (index + ((float)i / 100.0)) * 10.0 ));
            ppx = centerX + (radius * cos( (index + ((float)(i + 1)  / 100.0) ) * 10.0 ));
            ppy = centerY + (radius * sin( (index + ((float)(i + 1) / 100.0)) * 10.0 ));
            ofColor c (fill_color);

            int alpha = (  ( float(float(i))) / 10.0f) * 255.0f;
            c.a = alpha;
            ofSetColor(c);

            ofDrawLine(ppx, ppy, px, py);
        }
        ofSetColor(text_color);
        text_to_display = ofToString(int(index * 100)) + std::string(" %");
        ofDrawBitmapString(text_to_display, centerX  - ((text_to_display.size() * 8 )/ 2), centerY  + 5.5);
    }
    }
}

void JWaitAnimation::draw(float x, float y)
{
   posx = x;
   posy = y;
   draw();
}

void JWaitAnimation::setPosition(float x, float y)
{
    posx = x;
    posy = y;
}

void JWaitAnimation::setColors(ofColor const &back, ofColor const &fill, ofColor const &text)
{
    back_color = back;
    fill_color = fill;
    text_color = text;
}

void JWaitAnimation::setValue(double v)
{
    index = JUtils::Math::limit<double>(v, 0.0, 1.0);
}

void JWaitAnimation::setMode(JWaitAnimationType mode)
{
    this->mode = mode;
}
