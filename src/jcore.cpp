
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jcore.h"


// Core of the GUI System. Draws every components and updates some of their state (is_selected for example).
namespace JCore
{


unordered_map<string , JWidget *> widget_register;
unordered_map<string , JDialog *> dialog_register;

ofColor background_color = ofColor::black;


void loopOnce()
{
    for(auto & it : dialog_register) {
        if(it.second->isVisible()) {
            it.second->update();
            it.second->draw();
        }
    }
}


// registers a widget in the drawing system
void registerWidget(string name, JWidget* wid)
{
    widget_register[name] = wid;
}

// removes a widget from draw system
void unregisterWidget(string name)
{
    widget_register.erase(name);
}

void registerDialog(string name, JDialog *dlg)
{
    dialog_register[name] = dlg;
   // dialog_register[name] = shared_ptr<JDialog>(dlg);
}

void unregisterDialog(string name)
{
    dialog_register.erase(name);
}

// Draw methods

void drawWaitingScreen()
{
    ofEnableAlphaBlending();
    ofFill();
    ofSetColor(255, 255, 255, 100);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    // Rounded shape turning // or waiting bar


    ofSetColor(ofColor::white);
    ofDrawBitmapString("wait", ofGetWidth() / 2 - 8, ofGetHeight() / 2 - 5.5);
    ofDisableAlphaBlending();
}

void drawWaitingScreen(ofColor back, ofColor text, float x, float y, float w, float h)
{
    ofEnableAlphaBlending();
    ofFill();
    ofSetColor(back);
    ofDrawRectangle(x, y, w, h);
    // Rounded shape turning // or waiting bar
    ofSetColor(ofColor::white);
    ofDrawBitmapString("wait", x + (w / 2) - 8, y + (ofGetHeight() / 2) - 5.5);
    ofDisableAlphaBlending();
}


}
