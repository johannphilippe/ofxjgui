
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JCURVEEDITOR_H
#define JCURVEEDITOR_H

#include"jwidget.h"
#include"jplot.h"
#include"jtypes.h"
#include"jutils.h"

/*
 * GUI Editor to draw curves (linera/ exp log, cubic spline, or quadratic bezier)
*/

class JCurveEditor : public JPlot
{
public:
    constexpr static const int DEFAULT_DOMAIN = 2048;

    JCurveEditor(float w = 300, float h = 150);
    JCurveEditor(float x, float y, float w, float h);
    ~JCurveEditor();

    void setup() override;
    void draw() override;
    void update() override;
    void drawSamples() override;
    void processCurve();

    int hasNearSample(float x, float y);

    void setMode(JCurveMode m);
    void setDomain(size_t domain);
    size_t domain;
    JCurveMode curve_mode;

    void addSample(float x, float y);
    void addSample(glm::vec2 pos);

    void clear() override;

protected:

    void clickedCbk(ofMouseEventArgs &mouse) override;
    void releasedCbk(ofMouseEventArgs &mouse) override;
    void draggedCbk(ofMouseEventArgs &mouse) override;
    void keyPressed(ofKeyEventArgs &key) override;
    void keyReleased(ofKeyEventArgs &key) override;

    glm::vec2 getRelativeCoordinates();

    double getCurve(double beg, double ending, int dur, int idx, double type);
    glm::vec2 getBezier(double idx, double *vals);

    vector<glm::vec2> tab_y;

    bool clicked;

    int segment_first_index;
    int sel_index;
    double previous_mouseY;
    JUtils::JChrono<int, chrono::milliseconds> chrono;
    void snapToGrid();

    ofPolyline pline;
};


#endif // JCURVEEDITOR_H
