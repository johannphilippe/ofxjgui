
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jspin.h"
#include "jglobal.h"
#include "jutils.h"


template<typename T>
JSpinBase<T>::JSpinBase(T init, float w, float h) :
    JWidget(w, h), val(init)
{}

template<typename T>
JSpinBase<T>::JSpinBase(T init, float x, float y, float w, float h):
    JWidget(x, y, w, h), val(init)
{}

template<typename T>
JSpinBase<T>::~JSpinBase<T>(){
    ofRemoveListener(ofEvents().mousePressed, this, &JSpinBase::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JSpinBase::releasedCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JSpinBase::draggedCbk);
    ofRemoveListener(ofEvents().mouseScrolled, this, &JSpinBase::scrollCbk);
}

template<typename T>
void JSpinBase<T>::draw()
{
    ofNoFill();
    ofSetColor(JCOLOR_WHITE);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectangle(posx, posy, width, height);
    string valstr = JUtils::String::getString<T>(val);
    const float offset = 5;

    const float halfY = (posy + (height / 2) ) + 5.5;
    ofDrawBitmapString(valstr, posx + offset, halfY );
}

template<typename T>
void JSpinBase<T>::setup(){
    ofAddListener(ofEvents().mousePressed, this, &JSpinBase::clickedCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JSpinBase::releasedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JSpinBase::draggedCbk);
    ofAddListener(ofEvents().mouseScrolled, this, &JSpinBase::scrollCbk);
}
template<typename T>
void JSpinBase<T>::update()
{}

template<typename T>
void  JSpinBase<T>::clickedCbk(ofMouseEventArgs &mouse)
{
    if(is_selected) {
        is_clicked = true;
        y_click_start = (posy + height) - ofGetMouseY();
    }
}

template<typename T>
void JSpinBase<T>::releasedCbk(ofMouseEventArgs &mouse)
{
    is_clicked = false;
}

template<typename T>
void JSpinBase<T>::draggedCbk(ofMouseEventArgs &mouse)
{
    if(is_clicked) {
        const float diff =  ((posy + height) - ofGetMouseY()) - y_click_start;
        if(abs(diff) >= 25) {
            sigDragged(diff > 0);
            y_click_start = (posy + height) - ofGetMouseY();
        }
    }
}

template<typename T>
void JSpinBase<T>::scrollCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    sigScrolled(mouse.scrollY);
}

/**
 * SPINBOX System
 */

template<typename T>
JSpin<T>::JSpin(T init, T min, T max, T step, float w, float h):
    JWidget(w, h), JValue<T>(init), min(min), max(max), step(step),
    minus("-", w/4, h/2), plus("+", w/4, h/2), nbr_box(init, (w/4) * 3, h ),
    vcont(JOrientation::Vertical, &plus, &minus),
    hcont(JOrientation::Horizontal, &nbr_box, &vcont)
{
    plus.radius = 0;
    minus.radius = 0;
    initCallback();
}

template<typename T>
JSpin<T>::JSpin(T init, T min, T max, T step, float x, float y, float w, float h):
    JWidget(x, y, w, h), JValue<T>(init), min(min), max(max), step(step),
    minus("-", w/4, h/2), plus("+", w/4, h/2), nbr_box(init, (w/4) * 3, h ),
    vcont(JOrientation::Vertical, &plus, &minus),
    hcont(JOrientation::Horizontal, &nbr_box, &vcont)
{
    plus.radius = 0;
    minus.radius = 0;
    initCallback();
}

template<typename T>
JSpin<T>::~JSpin(){}

template<typename T>
void JSpin<T>::draw()
{
    hcont.draw();
}


template<typename T>
void JSpin<T>::setup(){


    hcont.setup();
    plus.sigButtonPressed.connect([&](){
       if( (this->value + step) > max ) this->setValue(max);
       else this->setValue(this->value + step);
       nbr_box.val = this->value;
    });

    minus.sigButtonPressed.connect([&](){
       if( (this->value - step) < min ) this->setValue(min);
       else this->setValue(this->value - step);
       nbr_box.val = this->value;
    });

    nbr_box.sigDragged.connect([&](bool v) {
        const float val = (v ? 1 : - 1) * step;
        if(this->value + val > max) this->setValue(max);
        else if(this->value + val < min) this->setValue(min);
        else this->setValue(this->value + val);
        nbr_box.val = this->value;
    });

    nbr_box.sigScrolled.connect([&](int v){
        const T val = v * step;
        if(this->value + val > max) this->setValue(max);
        else if(this->value + val < min) this->setValue(min);
        else this->setValue(this->value + val);
        nbr_box.val = this->value;
    });

    hcont.setPosition(posx, posy);
    sigResized.connect([&](float w, float h){
        hcont.updateChildrenPos();
    });

    sigPositionChanged.connect([&](float x, float y)
    {
        hcont.setPosition(x, y);
    });
}

template<typename T>
void JSpin<T>::update() {
    hcont.update();
}

template<typename T>
void JSpin<T>::initCallback()
{
}

template<typename T>
void JSpin<T>::setValue(T v, bool sig)
{
    T lv = JUtils::Math::limit<T>(v, min, max);
    this->value = lv;
    if(sig)
        this->sigValueChanged(this->value);
}

template<typename T>
JValue<T> JSpin<T>::getValue()
{
    return this->value;
}

template class JSpin<int>;
template class JSpin<float>;
template class JSpin<double>;

