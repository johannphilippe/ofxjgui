
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JCONTAINER_H
#define JCONTAINER_H

#include"jwidget.h"
#include"jtypes.h"
#include"jcore.h"


class JContainerBase : public JWidget
{

public:
    template<typename W>
    JContainerBase(JOrientation o, W *wid) :
        orientation(o), gap(0.0f)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    JContainerBase(JOrientation o, W *wid, Args *...wids) :
        orientation(o), gap(0.0f)
    {
        append(wid);
        push(wids...);
    }

    template<typename W>
    JContainerBase(float w, float h, JOrientation o, W *wid) :
        JWidget(w, h) ,
        orientation(o), gap(0.0f)
    {
        append(wid);
    }

    template<typename W, typename ... Args>
    JContainerBase(float w, float h, JOrientation o, W *wid, Args *... wids) :
        JWidget(w, h) ,
        orientation(o), gap(0.0f)
    {
        append(wid);
        push(wids...);
    }


    template<typename W>
    JContainerBase(float x, float y, float w, float h, JOrientation o, W *wid) :
        JWidget(x, y, w, h),
        orientation(o), gap(0.0f)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    JContainerBase(float x, float y, float w, float h, JOrientation o, W *wid, Args *...wids) :
        JWidget(x, y, w, h),
        orientation(o), gap(0.0f)
    {
        append(wid);
        push(wids...);
    }

    template<typename W>
    void push(W *wid)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    void push(W *wid, Args *...wids)
    {
        append(wid);
        push(wids...);
    }

    void append(JWidget *wid);

    void swap(JWidget *wid1, JWidget *wid2);
    void swap(size_t id1, size_t id2);

    void setGap(float gap);

   bool auto_resize;

   void updateChildren();
   virtual void updateChildrenPos();
   void setupChildren();
   void drawChildren();
   void drawChildren(float x, float y);

   float children_height, children_width;


   vector<JWidget *> *getChildren() {return &children;}

   vector<JWidget *> children;
   JOrientation orientation;
   JAlignment alignment;
   float gap;
protected:
};


/*
 *  --------------------- JContainer -------------------------
 * This is a vertical or horizontal layout allowing to contain multiple children.
 * It adjusts its size to children size if alignment is none. Or it calculates the center
 * if Centered.
 * It can be constructed with children, or they can be added later with push method.
*/


class JContainer : public JWidget
{
public:
    JContainer();
    ~JContainer();

    template<typename W>
    JContainer(W *wid):
        JWidget(), orientation(JOrientation::Vertical),
        gap(0.0f), auto_resize(true), alignment(JAlignment::None)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    JContainer(W *wid, Args*... wids):
        JWidget(),
        orientation(JOrientation::Vertical),
        gap(0.0f), auto_resize(true),
        alignment(JAlignment::None)
    {
        append(wid);
        push(wids...);
    }

    template<typename W>
    JContainer(JOrientation o, W *wid):
        JWidget(),
        orientation(o),
        gap(0.0f), auto_resize(true),
        alignment(JAlignment::None)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    JContainer(JOrientation o , W *wid, Args *...wids):
        JWidget(),
        orientation(o), gap(0.0f),
        auto_resize(true),
        alignment(JAlignment::None)
    {
        append(wid);
        push(wids...);
    }

    template<typename W, typename ... Args>
    static JContainer Centered(JOrientation o, glm::vec2 const &size, W *wid, Args *...wids)
    {
        JContainer c(o, wid, wids...);
        c.setAlignment(JAlignment::Center);
        c.resize(size);
        return c;
    }

    template<typename W>
    void push(W *wid)
    {
        append(wid);
    }

    template<typename W, typename ...Args>
    void push(W *wid, Args *...wids)
    {
        append(wid);
        push(wids...);
    }

    // User must call push method, append should be private or protected
    void append(JWidget *wid);

    void setup() override;
    void update() override;
    void draw() override;

    // Swap two child
    void swap(JWidget *first, JWidget *second);
    void swap(size_t index1, size_t index2);

    // Gap attribute defines spacing between children
    void setGap(float g);
    // None or Center
    void setAlignment(JAlignment a);

    void updateChildrenPos();


   float children_height, children_width;
   JOrientation orientation;
   float gap;
   bool auto_resize;
   JAlignment alignment;
protected:
   vector<JWidget *> children;
};

#endif // JCONTAINER_H
