
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef JEXPANDER_H
#define JEXPANDER_H

#include"jwidget.h"

class JExpander: public JWidget
{
public:

    JExpander(JOrientation o, JWidget *child, float w = 100, float h = 100);
    ~JExpander();

    void setup() override;
    void update() override;
    void draw() override;


    void resizeChild();
    void setBarSize(float bs);


    void updateChildPos();

    lsignal::signal<void(bool state)> sigExpanderStateChanged;
protected:
    JWidget *child;
    JOrientation orientation;
    bool is_expanded;
    ofColor background_color;
    float bar_size;

    void clickedCbk(ofMouseEventArgs &mouse);
    void releasedCbk(ofMouseEventArgs &mouse);
};

#endif // JEXPANDER_H
