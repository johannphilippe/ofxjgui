
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jwidget.h"

#include"jcore.h"

JWidget::JWidget() :
    mouse_offsetX(0.0), mouse_offsetY(0.0),
    is_selected(false), has_parent(false)
{
}

JWidget::JWidget(float w, float h):
    width(w), height(h), mask(0, 0, w, h),
    mouse_offsetX(0.0), mouse_offsetY(0.0), is_selected(false), has_parent(false)
{
}

JWidget::JWidget(float x, float y, float w, float h):
    width(w), height(h), posx(x), posy(y), mask(x, y, w, h),
    mouse_offsetX(0.0), mouse_offsetY(0.0), is_selected(false), has_parent(false)
{
}

JWidget::~JWidget()
{
}

void JWidget::setup()
{

}

void JWidget::update()
{

}

J2DCoord JWidget::getCoord()
{
    return J2DCoord(posx, posy, width, height);
}

void JWidget::setSelected(bool sel_state)
{
    is_selected = sel_state;
}

void JWidget::setChildrenState(bool child_state)
{
    has_parent = child_state;
}

bool JWidget::hasParent()
{
    return has_parent;
}

void JWidget::setMapped(bool map)
{
    is_mapped = map;
}

glm::vec2 JWidget::toRelativeCoordinates(glm::vec2 coord)
{
    return glm::vec2 ( (coord.x - posx) / width, 1 - ((coord.y - posy) / height));
}

glm::vec2 JWidget::toPixelCoordinates(glm::vec2 relative)
{
    return glm::vec2( (relative.x * width) + posx, (posy + height) - (relative.y * height) );
}

bool JWidget::isPixelInWidget(glm::vec2 pix)
{
    if(pix.x < posx || pix.x > (posx + width)
            || pix.y > posy || pix.y < (posy - height )) return false;
    return true;
}

void JWidget::setPosition(float x, float y)
{
    posx = x;
    posy = y;
    updateMask(getCoord());
    sigPositionChanged(posx, posy);
}

void JWidget::setPosition(glm::vec2 pos)
{
    setPosition(pos.x, pos.y);
}

void JWidget::resize(float w, float h)
{
    width = w;
    height = h;

    updateMask(getCoord());

    sigResized(width, height);
}

void JWidget::resize(glm::vec2 const &size)
{
    resize(size.x, size.y);
}

void JWidget::setPosX(const float px) {
    this->posx = px;
    sigPositionChanged(posx, posy);
}
void JWidget::setPosY(const float py) {
    this->posy = py;
    sigPositionChanged(posx, posy);
}

void JWidget::setWidth(const float w)
{
    this->width = w;
    sigResized(width, height);
}

void JWidget::setHeight(const float h)
{
    this->height = h;
    sigResized(width, height);
}

void JWidget::updateMask(float x, float y,  float w, float h)
{

    mask.set(x, y, w, h);
}

void JWidget::updateMask(const J2DCoord &coord)
{
    mask = coord;
}

float JWidget::getMouseX()
{
    return (ofGetMouseX() - mouse_offsetX);
}

float JWidget::getMouseY()
{
    return (ofGetMouseY() - mouse_offsetY);
}

/*
 * ----------------------------- JThreadedWidget -------------------------------------
 * A virtual facility to create threaded widgets.
 *
 *
*/

JThreadedWidget::JThreadedWidget(float w, float h):
    JWidget(w, h), wait_animation(w, h)
{

}

JThreadedWidget::JThreadedWidget(float x, float y, float w, float h):
    JWidget(x, y, w, h), wait_animation(x, y, w, h)
{

}

void JThreadedWidget::wait(bool w)
{
    should_wait = w;
}

bool JThreadedWidget::isWaiting()
{
    return should_wait;
}
