
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JGUI_H
#define JGUI_H

// CORE
#include"jcore.h"

// VALUE
#include"jvalue.h"

// WIDGETS
#include"jwidget.h"
#include"jbutton.h"
#include"jslider.h"
#include"jtoggle.h"
#include"jspin.h"
#include"jknob.h"

// CONTAINERS
#include"jcontainer.h"
#include"jscrollview.h"
#include"jexpander.h"

// SPACING WIDGETS
#include"jfill.h"

// ICONS
#include"jicon.h"

// AUDIO WIDGETS
#include"jsimplescope.h"
#include"jmultiscope.h"
#include"jplot.h"
#include"jcurveeditor.h"
#include"jfftview.h"

// UTILITIES AND GLOBAL
#include"jglobal.h"
#include"jtypes.h"
#include"jutils.h"

// ANIMATIONS
#include"janimationbase.h"
#include"janimations.h"
#include"jwaitanimation.h"

// Text display
#include"jtoastbase.h"
#include"jtextinputbase.h"
#include"jtextinput.h"

// Dialog
#include"jdialog.h"

// Resources
#include"resources/core.h"
#include"resources/user.h"


#endif // JGUI_H
