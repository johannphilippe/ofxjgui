
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JTOGGLE_H
#define JTOGGLE_H

#include"jwidget.h"
#include"Dependencies/lsignal/lsignal.h"


/*
 *  --------------------- JToggle -----------------------
 * Simple toggle with two states.
 *
*/

class JToggle : public JWidget
{
public:
    JToggle(float w = 100, float  h = 100);
    JToggle(float x, float y , float w, float h);
    ~JToggle();

    void setup() override;
    void draw() override;
    void update() override;

    void setBackgroundColor(const ofColor &color);
    void setSelectedColor(const ofColor &color);

    lsignal::signal<void(bool)> sigStateChanged;
protected:
    bool state;
    // Selected color is the background color when state is true
    ofColor background_color, selected_solor;

    void clickedCbk(ofMouseEventArgs &mouse);
};

/*
 *  --------------------- JMultiToggle -----------------------
 * Toggle with multiple states (no limit) represented by different colors.
 *
*/

class JMultiToggle : public JWidget
{
public:
    JMultiToggle(vector<ofColor> list, float w = 40, float h = 40 );
    JMultiToggle(vector<ofColor> list, float x, float y, float w, float h);
    ~JMultiToggle();

    void setup() override;
    void update() override;
    void draw() override;

    lsignal::signal<void(int)> sigStateChanged;
protected:
    vector<ofColor> color_list;
    size_t state;
    void clickedCbk(ofMouseEventArgs &mouse);
};

#endif // JTOGGLE_H
