
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jdialog.h"
#include"jcore.h"
#include"GLFW/glfw3.h"


/*
 *
 * JDIALOG
 *
*/

JDialog::JDialog() :
   settings(string("Uninitialized dialog"), OF_WINDOW, glm::vec2(400, 400), glm::vec2(0, 0))
{
    visible = false;
}

JDialog::JDialog(JDialogSettings s, JWidget *child, bool show_now) : this_ptr(this),
    settings(s), child(child),
    showable(true), is_ready(true),
    background_color(JCore::background_color),
    visible(false)
{
    JCore::registerDialog(settings.name, this);
    child->setChildrenState(true);
    if(show_now)
    {
        window = ofCreateWindow(settings.window_settings);
        ofRunApp(window, this_ptr);
        visible = true;
    }
}

// Base methods
void JDialog::setup()
{
    sigPreSetup();
    ofSetWindowTitle(settings.name);
    ofAddListener(ofEvents().mousePressed, this, &JDialog::click);
    child->setup();
    sigPostSetup();
}

void JDialog::update() {
    sigPreUpdate();
    if(! is_ready) return;


    for(auto & it : animation_register) {
        if(it->isRunning()) it->call();
    }

    mouseX = ofGetMouseX();
    mouseY = ofGetMouseY();

    J2DCoord pos = child->getCoord();
        if(mouseX >= pos.x && mouseX <= (pos.x + pos.w) &&
                mouseY >= pos.y && mouseY <= (pos.y + pos.h)) {
            child->setSelected(true);
        } else
            child->setSelected(false);
    ofSetBackgroundColor(background_color);
    child->update();

    checkEvents();

    sigPostUpdate();
}

void JDialog::draw() {
    ofSetColor(background_color);
    ofFill();
    ofDrawRectangle(settings.window_settings.getPosition().x, settings.window_settings.getPosition().y,
                    settings.window_settings.getWidth(), settings.window_settings.getHeight());

    child->draw();
    for(size_t i = 0; i < toast_register.size(); i++) {
        if(toast_register[i]->chrono.timeSinceStart() < toast_register[i]->duration_ms) {
            toast_register[i]->draw();
        } else {
            toast_register.erase(toast_register.begin() + i);
        }
    }

    if(wait_v) {
        JCore::drawWaitingScreen();
    }
}

void JDialog::exit()
{
    visible = false;
    cout << "exited " << endl;
}

#include"jcontainer.h"

// Core methods
void JDialog::show() {
    if(visible) return;
    shared_ptr<ofAppBaseWindow> w_ptr= ofCreateWindow(settings.window_settings);
    window.swap(w_ptr);
    ofRunApp(window, this_ptr);
    visible = true;

}

void JDialog::hide()
{
    window->setWindowShouldClose();
    visible = false;
}

shared_ptr<ofAppBaseWindow> JDialog::getWindow()
{
    return window;
}

// Toasts and animations
void JDialog::showToast(string text, int dur_ms, float x, float y)
{
    toast_register.push_back(shared_ptr<JToastBase>(new JToastBase(text, dur_ms, x, y)));
}

void JDialog::showToast(shared_ptr<JToastBase> toast)
{
    for(auto & it : toast_register) {
        if(toast.get() == it.get()) {
            it->pos = toast->pos;
            it->text = toast->text;
            it->duration_ms = toast->duration_ms;
            return;
        }
    }
    toast_register.push_back(toast);
}

void JDialog::registerAnimation(shared_ptr<JAnimationBase> animation)
{
    this->animation_register.push_back(animation);
}

void JDialog::unregisterAnimation(shared_ptr<JAnimationBase> animation)
{

    for(size_t i = 0; i < animation_register.size(); i++)
    {
        if(animation.get() == animation_register[i].get()) {
            animation_register.erase(animation_register.begin() + i);
            return;
        }
    }
}

void JDialog::wait(bool w)
{
    wait_v = w;
}

void JDialog::pushLoopEvent(const JLoopEvent &event)
{
    task_list.push(event);
}

void JDialog::checkEvents()
{
    JLoopEvent e;
    while(task_list.try_pop(e)) {
        e.event();
    }
}

bool JDialog::isVisible() {return visible;}

void JDialog::setBackgroundColor(const ofColor &c)
{
    background_color = c;
}

// CAllbacks
void JDialog::click(ofMouseEventArgs &mouse)
{
}

