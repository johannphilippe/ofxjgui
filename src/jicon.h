#ifndef JICON_H
#define JICON_H

#include"jtypes.h"
#include<functional>

class JIcon
{
public:
    JIcon(std::function<void(float, float, float ,float)> func, bool fill);

    static JIcon Square(bool fill = true);
    static JIcon Circle(bool fill = true);
    static JIcon Stop(bool fill = true);
    static JIcon Record(bool fill = true);
    static JIcon Play(bool fill = true);
    static JIcon Pause(bool fill = true);

    static void drawSquare(float x, float y, float w, float h);
    static void drawCircle(float x, float y, float w, float h);
    static void drawPlay(float x, float y, float w, float h);
    static void drawPause(float x, float y, float w, float h);

    void setup();
    void update();
    void draw(float x, float y, float w, float h);

    void setColor(ofColor const &c);
    void setFill(bool const fill);
protected:
    std::function<void(float, float, float, float)> draw_func;;
    ofColor color;
    bool fill;

};

#endif // JICON_H
