
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jbutton.h"

JButton::JButton(std::string txt, float w, float h) :
    JWidget(w, h), text(txt), color(255, 255, 255),
    radius(15), text_width(8 * txt.size()), clicked(false)
{
}

JButton::JButton(std::string txt, float x, float y, float w, float h) :
    JWidget(x, y, w, h), text(txt), color(255, 255, 255),
    radius(15), text_width(8 * txt.size()), clicked(false)
{
}

JButton::~JButton()
{
    ofRemoveListener(ofEvents().mousePressed, this, &JButton::buttonClicked);
    ofRemoveListener(ofEvents().mouseReleased, this, &JButton::buttonReleased);
}


void JButton::setRadius(float radius)
{
    this->radius = radius;
}

void JButton::setColor(ofColor c)
{
    this->color = c;
}

void JButton::setColor(int r, int g, int b)
{
    this->color = ofColor(r,g,b);
}

void JButton::setup() {
    ofAddListener(ofEvents().mousePressed, this, &JButton::buttonClicked);
    ofAddListener(ofEvents().mouseReleased, this, &JButton::buttonReleased);
}

void JButton::update() {

}

// method to draw with static position
void JButton::draw()
{
    if(is_selected && !clicked) {
        ofFill();
    } else {
        ofNoFill();
    }
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofSetColor(color);
    ofDrawRectRounded(posx, posy ,0, width, height, radius, radius, radius, radius);

    text_x = posx + ((width / 2 ) - (text_width / 2));
    text_y = posy + ((height / 2) + (5.5));
    if(is_selected && ! clicked)
        ofSetColor(255 - color.r, 255 - color.g, 255 - color.b);

    ofDrawBitmapString(this->text, text_x, text_y, 0);
}

void JButton::click()
{
    sigButtonPressed();
    sigButtonReleased();
}

// Callbacks

void JButton::buttonClicked(ofMouseEventArgs &mouse)
{
    if(is_selected) {
        clicked = true;
        sigButtonPressed();
    }
}

void JButton::buttonReleased(ofMouseEventArgs &mouse)
{
        if(clicked) sigButtonReleased();
        clicked = false;
}



/*
 *  JPRettyButton : an animated and filled button, with customizable text color, background color, selected_color,  radius, and click animation time.
 * Its animation changes the color of the button slowly.
*/

JPrettyButton::JPrettyButton(std::string txt, float w, float h):
    JButton(txt, w, h), animation_ms(500), animate(true)
{
}

JPrettyButton::JPrettyButton(std::string txt, float x, float y , float w, float h):
    JButton(txt, x, y , w , h), animation_ms(500), animate(true)
{
}

JPrettyButton::~JPrettyButton()
{
    ofRemoveListener(ofEvents().mousePressed, this, &JPrettyButton::buttonClicked);
    ofRemoveListener(ofEvents().mouseReleased, this, &JPrettyButton::buttonReleased);
}

void JPrettyButton::setup(){
    ofAddListener(ofEvents().mousePressed, this, &JPrettyButton::buttonClicked);
    ofAddListener(ofEvents().mouseReleased, this, &JPrettyButton::buttonReleased);
}
void JPrettyButton::update()
{
}

void JPrettyButton::draw()
{
    if(is_animating) {
        const int ms = chrono.timeSinceStart();
        if(ms >= animation_ms) {
            is_animating = false;
        }
        const double index = JUtils::Math::getCurve(0.0, 1.0, 1024,  (double(ms) / double(animation_ms)) * 1024, 5);
        const double inv = 1.0 - index;
        // current is interpolation between back and selected
        if(index <= 1.0) {
            if(clicked)
            {
                current = ofColor( (color.r * inv) + (sel_color.r * index),
                                   (color.g * inv) + (sel_color.g * index),
                                   (color.b * inv) + (sel_color.b * index));
                c_text = ofColor( (text_color.r * inv) + (inv_text.r * index),
                                  (text_color.g * inv) + (inv_text.g * index),
                                  (text_color.b * inv) + (inv_text.b * index));
            }
            else
            {
                current = ofColor( (color.r * index) + (b_from.r * inv),
                                   (color.g * index) + (b_from.g * inv),
                                   (color.b * index) + (b_from.b * inv));
                c_text = ofColor( (text_color.r * index) + (t_from.r * inv),
                                  (text_color.g * index) + (t_from.g * inv),
                                  (text_color.b * index) + (t_from.b * inv));
            }

        } else if(index >= 1.0)
        {
            if(clicked) {
                current = sel_color;
                c_text = inv_text;
            } else {
                current = color;
                c_text = text_color;
            }
        }
    }

    ofFill();

    ofSetColor(current);
    ofDrawRectRounded(posx, posy ,0, width, height, radius, radius, radius, radius);

    text_x = posx + ((width / 2 ) - (text_width / 2));
    text_y = posy + ((height / 2) + (5.5));
    ofSetColor(c_text);
    ofDrawBitmapString(this->text, text_x, text_y, 0);
}


void JPrettyButton::buttonClicked(ofMouseEventArgs &mouse)
{
    if(is_selected) {
        clicked = true;
        sigButtonPressed();
        if(animate)
        {
            chrono.start();
            is_animating = true;
        } else {
            current = sel_color;
            c_text = inv_text;
        }
    }
}

void JPrettyButton::buttonReleased(ofMouseEventArgs &mouse)
{
    if(clicked) {
        sigButtonReleased();
        if(animate)
        {
            chrono.start();
            is_animating = true;
            b_from = current;
            t_from = c_text;
        } else {
            current = color;
            c_text = text_color;
        }

    }
    clicked = false;
}

void JPrettyButton::setColors(ofColor back, ofColor sel, ofColor txt)
{
    this->color = back;
    this->sel_color = sel;
    this->text_color = txt;
    this->current = back;
    this->c_text = text_color;
    inv_text = ofColor(255 - text_color.r, 255 - text_color.g, 255 - text_color.b);
}

void JPrettyButton::setAnimate(bool a)
{
    animate = a;
}

void JPrettyButton::setAnimationTimeMs(int ms)
{
    animation_ms = ms;
}


// JICON BUTTON

JIconButton::JIconButton(JIcon const &i, float w, float h):
    JWidget(w, h), icon(i), clicked(false), radius(20.0f)
{

}

JIconButton::JIconButton(JIcon const &i, float x, float y , float w, float h):
    JWidget(x, y, w, h), icon(i), clicked(false), radius(20.0f)
{

}

JIconButton::~JIconButton()
{
    ofRemoveListener(ofEvents().mousePressed, this, &JIconButton::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JIconButton::releasedCbk);
}

void JIconButton::setColor(const ofColor &c)
{
    color = c  ;
}

void JIconButton::setRadius(float rad)
{
    radius = rad;
}

void JIconButton::setIcon(const JIcon &icon)
{
    this->icon = icon;
}

void JIconButton::setup()
{
    ofAddListener(ofEvents().mousePressed, this, &JIconButton::clickedCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JIconButton::releasedCbk);
}

void JIconButton::update(){

}

void JIconButton::draw()
{
    if(is_selected && !clicked) {
        ofFill();
    } else {
        ofNoFill();
    }
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofSetColor(color);
    ofDrawRectRounded(posx, posy ,0, width, height, radius, radius, radius, radius);

    const float icon_x = posx + ((width / 6) * 2) ;
    const float icon_y = (posy) + ((height / 6) * 2);
    if(is_selected && ! clicked)
    {
        ofColor c (255 - color.r, 255 - color.g, 255 - color.b);
        icon.setColor(c);
    } else
        icon.setColor(color);

    icon.draw(icon_x, icon_y, width / 3, height  / 3  );
    //ofDrawBitmapString(this->text, text_x, text_y, 0);
}

void JIconButton::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    clicked = true;
    sigButtonPressed();
}

void JIconButton::releasedCbk(ofMouseEventArgs &mouse)
{
    clicked = false;
    sigButtonReleased();
}
