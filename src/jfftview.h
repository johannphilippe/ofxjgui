
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef JFFTVIEW_H
#define JFFTVIEW_H

#include"jwidget.h"
#include"Dependencies/ofxPixelsShape/src/ofxPixelsShape.h"
/*
 * ----------------- JFFTView --------------------
 * A non realtime FFT representation.
 * Should it copy data ? Try with a yes.
*/

template<typename T>
class JFFTView: public JThreadedWidget
{
public:
    JFFTView();
    JFFTView(float w = 400, float h = 400);
    JFFTView(float x, float y, float w, float h);

    void setup() override;
    void update() override;
    void draw() override;

    void drawFbo();
    void processFFTAndDraw();


    void setMode(JFFTDisplayMode m);


    void setSoundfile(string & path, size_t fft_size, size_t overlap = 1);

    void threadedFunction() override;
//protected:
    ofThreadChannel<ofPixels> channel;
    ofTexture texture;
    ofImage img;
    ofPixels pix, toscreen_pix;
    ofxPixelsShape shape;

    vector< vector<T> > real_data, imag_data;
    JFFTDisplayMode mode;
    int samplerate;

    std::condition_variable cond;
    bool new_frame;

    size_t fft_size;
    size_t overlap;
    string file_path;

    atomic<bool> process_fft;

    bool show_tooltip;
};

#endif // JFFTVIEW_H
