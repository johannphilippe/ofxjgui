
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef JSCROLLVIEW_H
#define JSCROLLVIEW_H

#include"jwidget.h"
#include"jcontainer.h"
#include"ofEasyCam.h"
#include"jtypes.h"

/*
 * ---------------------------- SCROLLVIEW -----------------------------------
 * A basic container (can contain several child) enabling to scroll vertically or horizontally.
 *
 * Example :
 *
 * JButton but("mybut", 200, 50);
 * JSlider slid(200, 50);
 *
 * JScrollView sv(200, 50, JOrientation::Vertical, &but, &slid);
*/

class JScrollBar;

class JScrollView : public JContainerBase
{
public:
    constexpr static const float DEFAULT_SCROLL_MULTIPLIER = 3.0f;

    template<typename W, typename ...Args>
    JScrollView(float w, float h, JOrientation o, W *wid) :
        JContainerBase(w, h, o, wid),
        scroll_mult(DEFAULT_SCROLL_MULTIPLIER),
        scroll_idx(0.0f), scrollbar(nullptr)
    {
    }

    template<typename W, typename ...Args>
    JScrollView(float w, float h, JOrientation o, W *wid, Args*... wids) :
        JContainerBase(w, h, o, wid, wids...),
        scroll_mult(DEFAULT_SCROLL_MULTIPLIER),
        scroll_idx(0.0f), scrollbar(nullptr)
    {

    }

    template<typename W>
    JScrollView(float x, float y, float w, float h, JOrientation o, W *wid) :
        JContainerBase(x, y, w, h, o, wid),
        scroll_mult(DEFAULT_SCROLL_MULTIPLIER),
        scroll_idx(0.0f), scrollbar(nullptr)
    {
    }


    template<typename W, typename ...Args>
    JScrollView(float x, float y, float w, float h, JOrientation o, W *wid, Args*... wids) :
        JContainerBase(x, y, w, h, o, wid, wids...),
        scroll_mult(DEFAULT_SCROLL_MULTIPLIER),
        scroll_idx(0.0f), scrollbar(nullptr)
    {
    }

    ~JScrollView();

    void setScrollBar(JScrollBar *bar);

    void update() override;
    void setup() override;
    void draw() override;

    void setOrientation(JOrientation o);

    void updateCameraPosition();

    // Multiplier for scrolling. Default = 3.
    float scroll_mult;

    // Scrolls to an index betwen 0.0 and 1.0
    void scrollToIndex(float index, bool sync = true);

    // Scrolls to a child
    void scrollToChild(JWidget *child, bool sync = true);

    void syncScrollbar();

    void updateChildrenPos() override;
protected:
    ofEasyCam cam;
    ofFbo fbo;

    void scrolledCbk(ofMouseEventArgs &mouse);
    JContainer cont;

    float scroll_idx;

    JScrollBar *scrollbar;
};


class JScrollBar : public JWidget
{
public:

    JScrollBar(JOrientation o, float w = 30, float h = 200);
    JScrollBar(JOrientation o, float x, float y, float w, float h);
    ~JScrollBar();

    void setup() override;
    void update() override;
    void draw() override;

    void setScrollPosition(float index, bool sync = true);
    void setScrollView(JScrollView *view);
protected:

    void clickedCbk(ofMouseEventArgs &mouse);
    void draggedCbk(ofMouseEventArgs &mouse);
    void releasedCbk(ofMouseEventArgs &mouse);
    void scrolledCbk(ofMouseEventArgs &mouse);

    JOrientation orientation;
    JScrollView *view;
    bool clicked;
    void updateValue();
    float value;
};

#endif // JSCROLLVIEW_H
