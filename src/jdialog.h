
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JDIALOG_H
#define JDIALOG_H

#include"ofMain.h"
#include"ofAppGLFWWindow.h"
#include"jwidget.h"
#include"jtoastbase.h"
#include"Dependencies/lsignal/lsignal.h"
#include"janimationbase.h"
#include"jtypes.h"

/*
 * ------------------ JDIALOG -----------------
 * Parent of a container, or any other child. It is necessary to have at least one dialog to use ofxJGUI.
 *
 *
*/

class JDialog : public ofBaseApp
{
public:
    JDialog();
    JDialog(JDialogSettings settings, JWidget *child, bool show_now = false);
    //~JDialog();

    void draw() override;
    void update() override;
    void setup() override;
    void exit() override;

    void show();
    void hide();

    void showToast(string text, int dur_ms, float x, float y);
    void showToast(shared_ptr<JToastBase> toast);

    void registerAnimation(shared_ptr<JAnimationBase> animation);
    void unregisterAnimation(shared_ptr<JAnimationBase> animation);

    shared_ptr<ofAppBaseWindow> getWindow();

    lsignal::signal<void(void)> sigPreSetup;
    lsignal::signal<void(void)> sigPostSetup;

    lsignal::signal<void(void)> sigPreUpdate;
    lsignal::signal<void(void)> sigPostUpdate;

    lsignal::signal<void(void)> sigShowDialog;
    lsignal::signal<void(void)> sigHideDialog;

    void wait(bool w);

    void pushLoopEvent(JLoopEvent const &event);

    void setBackgroundColor(ofColor const &c);

    bool isVisible();
protected:

    void click(ofMouseEventArgs &mouse);

    shared_ptr<JDialog> this_ptr;

    JDialogSettings settings;
    shared_ptr<ofAppBaseWindow> window;
    JWidget *child;
    bool showable;
    bool is_ready;
    ofColor background_color;

    vector<shared_ptr<JToastBase>> toast_register;
    vector<shared_ptr<JAnimationBase>> animation_register;

    bool visible;

    bool wait_v;

    JConcurrentQueue<JLoopEvent> task_list;
    void checkEvents();

};




#endif // JDIALOG_H

