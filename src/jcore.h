
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JCORE_H
#define JCORE_H

#include"jtypes.h"
#include"ofMain.h"
#include"jwidget.h"
#include"jglobal.h"
#include"janimationbase.h"
#include"jtoastbase.h"
#include"jdialog.h"
#include"jtypes.h"

namespace JCore {

    extern unordered_map<string, JDialog *> dialog_register;
    extern void registerDialog(string name, JDialog *d);
    extern void unregisterDialog(JDialog *d);

    extern unordered_map<string, JWidget *> widget_register;

    extern void registerWidget(JWidget *wid);
    extern void unregisterWidget(JWidget *wid);

    extern ofColor background_color;


    // Drawing methods
    extern void drawWaitingScreen();
    extern void drawWaitingScreen(ofColor background, ofColor text, float x, float y, float w, float h);


    extern void loopOnce();
}


#endif // JCORE_H
