
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JTYPES_H
#define JTYPES_H

#include<iostream>
#include<queue>
#include<condition_variable>
#include<mutex>
#include"ofColor.h"
#include"ofAppGLFWWindow.h"
#include"ofWindowSettings.h"

#include"ofMain.h"
// Orientation for some widgets that can be horizontal or vertical
enum class JOrientation
{
    Vertical= 0,
    Horizontal= 1,
};

enum class JAlignment
{
    None = 0,
    Center = 1,
};

// Grid types for plots, curve editor, scopes...
enum class JGridType
{
    None = 0,
    Cross = 1,
    Full = 2
};

// Cornerposition used for legends (in scope for example)
enum class JCornerPosition
{
    TopLeft = 0,
    TopRight = 1,
    BottomLeft = 2,
    BottomRight = 3
};

// Curve mode for curve editor
enum class JCurveMode
{
    LinearLogExp = 0,
    Spline = 1,
    Bezier = 2
};

// Animation mode, for animations
enum class JAnimationMode
{
    Linear = 0,
    Log = 1,
    Exp = 2
    // add other types
};

// Multiline mode for text editors and labels
enum class JMultilineMode
{
    Disabled = 0,
    Fixed = 1,
    AutoResize = 2,
};

// How to display FFT
enum class JFFTDisplayMode
{
    Linear = 0,
    Log = 1,
    Bark = 2,
};

// Kind of animations for waiting a thread to finish a work
enum class JWaitAnimationType
{
    // Some of them to implement
    ProgressBar = 0,
    ProgressBarDisplayValue = 1,
    SimpleCircular = 2,
    SimpleCircularDisplayValue = 3,

};

// 2D coordinates (x y width height)
struct J2DCoord
{
    J2DCoord() : x(0.0f), y(0.0f), w(0.0f), h(0.0f) {}
    J2DCoord(float x, float y, float w, float h) : x(x), y(y), w(w), h(h) {}
    float x, y, w, h;

    bool isInBounds(float cx, float cy)
    {
        if(cx < x || cx > (x + w))
            return false;

        if( cy < y || cy > (y + h) )
            return false;
        return true;
    }

    void set(float x, float y, float w, float h)
    {
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
    }

    J2DCoord &operator=(const J2DCoord &c)
    {
        this->x = c.x;
        this->y = c.y;
        this->w = c.w;
        this->h = c.h;
        return *this;
    }
};

// Used or JMultiScope to describe signals names and color, and hold a pointer to signals data.
template<typename T>
struct JScopedSignalDescriptor
{
    JScopedSignalDescriptor(std::string name, std::vector<T> *data, ofColor col) : name(name), data(data), color(col)
    {

    }
    ofColor color;
    std::string name;
    std::vector<T> *data;

};

// Multitoggle state
// TODO : add icon
struct JMultiToggleState
{

    ofColor color;
};

struct JLoopEvent
{
    JLoopEvent() {}
    JLoopEvent(std::function<void(void)> e) : event(e) {}
    std::function<void(void)> event;
};

struct JDialogSettings
{
    JDialogSettings(std::string n, ofWindowMode mode, glm::vec2 size, glm::vec2 pos ) :
        name(n)
    {
        window_settings.windowMode = mode;
        window_settings.setSize(size.x, size.y);
        window_settings.setPosition(pos);
    }
    std::string name;
    ofGLFWWindowSettings window_settings;


};

// Concurrent queue
template <typename T>
class JConcurrentQueue
{
public:
    ~JConcurrentQueue() {}

    void push(T const &v)
    {
        std::unique_lock<std::mutex> lock(mtx);
        queue.push(v);
        lock.unlock();
        cond.notify_one();
    }

    bool empty()
    {
        std::unique_lock<std::mutex> lock(mtx);
        return queue.empty();
    }

    bool try_pop(T & popped)
    {
        std::unique_lock<std::mutex> lock(mtx);
        if(queue.empty()) {
            lock.unlock();
            return false;
        }
        popped = queue.front();
        queue.pop();
        lock.unlock();
        return true;
    }

    void wait_and_pop(T &popped)
    {
        std::unique_lock<std::mutex> lock(mtx);
        while(queue.empty()) {
            cond.wait(lock);
        }
        popped = queue.front();
        queue.pop();
    }

private:
    std::queue<T> queue;
    std::mutex mtx;
    std::condition_variable cond;
};


struct JIconData
{
    JIconData(const unsigned char *arr,const int wid, const int hei,const ofImageType typ) :
        array(arr), width(wid), height(hei), type(typ)
    {

    }
    const unsigned char *array;
    const int width, height;
    const ofImageType type;
};


#endif // JTYPES_H
