
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jknob.h"

JKnob::JKnob()
{

}

JKnob::JKnob(double min, double max, float w, float h):
    JWidget(w, h), min(min), max(max), value(min), val_str(JUtils::String::getString<double>(value)), y_click_start(0.0)
{

}

JKnob::JKnob(double min, double max, float x, float y, float w, float h):
    JWidget(x, y , w, h), min(min), max(max), value(min), val_str(JUtils::String::getString<double>(value)), y_click_start(0.0)
{

}

JKnob::~JKnob(){
   ofRemoveListener(ofEvents().mousePressed, this, &JKnob::clickedCbk);
   ofRemoveListener(ofEvents().mouseReleased, this, &JKnob::releasedCbk);
   ofRemoveListener(ofEvents().mouseDragged, this, &JKnob::draggedCbk);
   ofRemoveListener(ofEvents().mouseScrolled, this, &JKnob::scrolledCbk);
}

void JKnob::setup()
{
   ofAddListener(ofEvents().mousePressed, this, &JKnob::clickedCbk);
   ofAddListener(ofEvents().mouseReleased, this, &JKnob::releasedCbk);
   ofAddListener(ofEvents().mouseDragged, this, &JKnob::draggedCbk);
   ofAddListener(ofEvents().mouseScrolled, this, &JKnob::scrolledCbk);
}

void JKnob::update()
{}

void JKnob::draw()
{
    ofFill();
    ofSetColor(color);
    const float radius = (std::min(width, height) / 2.0 - 10);
    ofDrawCircle(posx + radius + 10, posy + radius, radius);
    const float centerX = posx + (width / 2);
    const float centerY = posy + (height / 2 - 10);
    // value represents a point on the circle.
    //first find the percentage of the value
    const float range = max - min;
    // base1 %
    const float angle = (((value - min) / range) * 5.4)+ 2.1;
    const float px = centerX + (radius * cos(angle));
    const float py = centerY + (radius * sin(angle));

    ofSetLineWidth(2);
    ofSetColor(ofColor(JCOLOR_WHITE));

    ofDrawLine(centerX, centerY, px, py );
    ofSetColor(ofColor::white);

    ofDrawBitmapString(val_str, centerX - ((8 * val_str.size()) / 2), posy + height - 10);
}

void JKnob::setColor(ofColor c)
{
    color = c;
}

void JKnob::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    is_clicked = true;
    y_click_start = ofGetMouseY();
}

void JKnob::releasedCbk(ofMouseEventArgs &mouse)
{
    is_clicked = false;
    y_click_start = 0.0;
}

void JKnob::draggedCbk(ofMouseEventArgs &mouse)
{
    if(is_clicked) {
        const float cur = ofGetMouseY();
        const float diff = (y_click_start - cur) / 14;
        y_click_start = cur;
        if(y_click_start == 0) return;
        cout << "diff : " << diff <<endl;
        if(value + diff > max) value = max;
        else if(value + diff < min) value = min;
        else value += diff;
        val_str = JUtils::String::getString<double>(value);
        sigValueChanged(value);
    }
}

void JKnob::scrolledCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    value = JUtils::Math::limit<double>( (value + ((max - min) * mouse.scrollY) / 100.0) , min, max);
    val_str = JUtils::String::getString<double>(value);
}
