
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jtextinput.h"
#include"jcore.h"

JTextInput::JTextInput(ofTrueTypeFontSettings const & settings, JMultilineMode m, float w, float h) :
    JTextInputBase(w,h ), n_line(1), font_settings(settings), mode(m)
{
}

JTextInput::JTextInput(ofTrueTypeFontSettings const & settings, JMultilineMode m, float x, float y, float w, float h):
    JTextInputBase(x,y,w,h), n_line(1), font_settings(settings), mode(m)
{
}

JTextInput::~JTextInput(){}

void JTextInput::setup(){
    loadFont(font_settings);

    chrono.start();
    dclick_chrono.start();

    ofAddListener(ofEvents().keyPressed, this, &JTextInput::keyCbk);
    ofAddListener(ofEvents().mousePressed, this, &JTextInput::clickCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JTextInput::releasedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JTextInput::draggedCbk);

    calculateHeight();
}

void JTextInput::update()
{

}

void JTextInput::draw()
{
    // draw background
    ofFill();
    ofSetColor(background_color);
    ofDrawRectangle(posx, posy, width, height);

    //draw border
    ofNoFill();
    ofSetLineWidth(3);
    ofSetColor(background_color.getInverted());
    ofDrawRectangle(posx, posy, width, height);
    ofSetLineWidth(3);

    //const int max_char_per_line = width / font.stringWidth("p");

    // draw caret
    if(editing && (chrono.timeSinceStart() % 2 ) == 0) {
        ofSetLineWidth(1);

        int c_line = 0;
        string display("");
        for(int i = 0; i < caret_position ; i++) {
            display += text.substr(i, 1);
            if(i == caret_position) break;
            const float next = (i < textSize()) ? font.stringWidth(text.substr(i + 1,1)) : font.stringWidth("B");
            if(font.getStringBoundingBox(display, 0, 0).width + next >= width) {
                display.clear();
                c_line += 1;
                i -=2;
            }
        }
        const float cpx = LEFT_OFFSET + posx + font.getStringBoundingBox(display, 0, 0).width;
        const float cpy = posy + ( (font.getLineHeight() + LINE_GAP) * c_line);
        ofDrawLine(glm::vec2(cpx, cpy), glm::vec2(cpx, posy  + ( (font.getLineHeight()) * (c_line + 1 ))));
    }

    // draw text
    if(!text.empty()) {
        /*
        if(area_selected) {
            const int sel_area_begx = (selected_area.first * 8) + LEFT_OFFSET + posx;
            const int sel_area_width = ( abs(selected_area.second - selected_area.first) * 8);
            ofFill();
            ofSetColor(0, 0, 255, 150);
            ofDrawRectangle(sel_area_begx, posy, 0, sel_area_width, height);
            ofNoFill();
        }
        */

        ofSetColor(text_color);
        // calculate size of each line in characters, then


        drawString();
    }

}

void JTextInput::drawString()
{
    // The for loop should be inside the switch (not the opposite) to have the condition only once

    if(textSize() == 0) return;

    string display("");
    switch (mode) {
    case JMultilineMode::Disabled:
    {
        size_t line_width = 0;
        for(int i = 0; i < textSize(); ++i)
        {
            line_width += font.stringWidth(text.substr(i, 1));
            if(line_width >= width) break;
            display += text.substr(i, 1);
        }
        font.drawString(display, posx + LEFT_OFFSET, posy  + font.getLineHeight()  + LINE_GAP);
        break;
    }
    case JMultilineMode::Fixed:
    {
        for(int i = 0; i < n_line; ++i)
        {
            size_t cursor = 0;
            float py = posy + LINE_GAP;
            for(; cursor < size_t(textSize()); ++cursor)
            {
                const string sub = text.substr(cursor, 1);
                const float next = (cursor < size_t(textSize()))  ? font.stringWidth(text.substr(cursor  + 1, 1)) : font.stringWidth("B");
                if(font.getStringBoundingBox(display + sub, 0, 0).width  + next > width)
                {
                    cursor--;
                    break;
                }
                display += sub;
            }

            py +=  font.getLineHeight() + LINE_GAP;

            font.drawString(display, posx + LEFT_OFFSET, py);
            if(cursor >= size_t(textSize() - 1))  {
                return;
            }
        }
        break;
    }
    case JMultilineMode::AutoResize:
    {
        break;
    }
    }
}

void JTextInput::setNumberOfLines(int l)
{
    n_line = l;
    calculateHeight();
}

void JTextInput::loadFont(std::string fontPath, int size)
{
    font_settings = ofTrueTypeFontSettings(fontPath, size);
    font_settings.antialiased = true;
    font.load(font_settings);
    calculateHeight();
}

void JTextInput::loadFont(ofTrueTypeFontSettings s)
{
    font_settings = s;
    font.load(font_settings);
    calculateHeight();
}

void JTextInput::setMultilineMode(JMultilineMode m)
{
    mode = m;
    calculateHeight();
}

void JTextInput::calculateHeight()
{
    if(mode == JMultilineMode::AutoResize) {
        const int max_char_per_line = width / font.stringWidth("p") - 1;
        n_line = textSize() / max_char_per_line;
    } else if(mode == JMultilineMode::Disabled) {
        n_line = 1;
    }

    setHeight( (font.getLineHeight() + LINE_GAP) * n_line + (LINE_GAP * 2) );
}

int JTextInput::getDisplayable()
{
    return (int(width / font.stringWidth("p"))- 1) * n_line;
}

void JTextInput::keyCbk(ofKeyEventArgs &key)
{

    if(!editing) return;

    if(JUtils::Key::isPrintable(key.key) && key.modifiers == 0) {

        const int displayable = (int(width / font.stringWidth("p"))- 1) * n_line;

        if(area_selected) { // if area selected, erase area selected and replace with letter
            text.erase(selected_area.first, selected_area.second);
            if(textSize() >= displayable) return;
            text.insert(selected_area.first, JUtils::Key::toString(key.key));
            cout << text.size() << endl;
            setCaretPosition(selected_area.first + 1);
            area_selected = false;

        } else { // if no area selected, insert at caret pos
            if(textSize() >= displayable) return;
            text.insert(caret_position, JUtils::Key::toString(key.key));
            setCaretPosition(caret_position + 1);
        }
    } else if(JUtils::Key::isPrintable(key.key) && key.modifiers != 0) {
        const int mod = JUtils::Key::getModifier(key.key, key.modifiers);
        switch(mod)
        {
        case JUtils::Key::CTRL_C:
        {
            // to clipboard
            if(editing && area_selected) {
                //
                string to_copy = text.substr(selected_area.first, selected_area.second);
                JUtils::System::toClipboard(to_copy);
            }
            break;
        }
        case JUtils::Key::CTRL_V:
        {
            string clipboard = JUtils::System::getClipboard();
            if(clipboard.empty()) return;
            if(editing && area_selected)
            {
                // replace
                text.erase(selected_area.first, selected_area.second);
                text.insert(caret_position, clipboard);
                setCaretPosition(selected_area.first + clipboard.size());
                area_selected = false;
            } else if(!area_selected)
            {
                text.insert(caret_position, clipboard);
                setCaretPosition(caret_position + clipboard.size());
            }
            break;
        }
        case JUtils::Key::CTRL_X:
        {
            if(editing && area_selected) {
                string to_copy = text.substr(selected_area.first, selected_area.second);
                JUtils::System::toClipboard(to_copy);
                text.erase(selected_area.first, selected_area.second);
                setCaretPosition(selected_area.first);
                area_selected = false;
            }
            break;
        }
        case JUtils::Key::CTRL_A:
        {
            area_selected = true;
            selected_area = make_pair(0, textSize());
            break;
        }
        }

    } else {
        switch(key.key) {
        case OF_KEY_LEFT:
        {
            setCaretPosition(caret_position - 1);
            break;
        }
        case OF_KEY_RIGHT:
        {
            setCaretPosition(caret_position + 1);
            break;
        }
        case OF_KEY_UP:
        {
            const int max_char_per_line = width / font.stringWidth("p") - 1;
            setCaretPosition(caret_position - max_char_per_line);
            break;
        }
        case OF_KEY_DOWN:
        {
            const int max_char_per_line = width / font.stringWidth("p") - 1;
            setCaretPosition(caret_position + max_char_per_line);
            break;
        }
        case OF_KEY_BACKSPACE:
        {
            if(area_selected) {
                text.erase(selected_area.first, selected_area.second);
                setCaretPosition(selected_area.first);
                area_selected = false;
                break;
            }
            if(caret_position <= 0) break;
            text.erase(text.begin() + caret_position - 1);
            setCaretPosition(caret_position - 1);
            break;
        }

        }
    }
}

void JTextInput::clickCbk(ofMouseEventArgs &mouse)
{
    if(is_selected && mouse.button == 0) {
        area_selected = false;
        bool double_click = (dclick_chrono.timeSinceLastTrigger() < 150) ? true : false;
        editing = true;
        if(double_click) {
            selectText(0, textSize());
            area_selected = true;
        } else {
            // find position of caret
            const int c_pos = int(int(ofGetMouseX() - posx) / ( 8));
            setCaretPosition(c_pos);
        }

    } else {
        editing = false;
    }
}

void JTextInput::releasedCbk(ofMouseEventArgs &mouse)
{
}

void JTextInput::draggedCbk(ofMouseEventArgs &mouse)
{
    if(is_selected && editing) {
        const int c_pos = int(int(ofGetMouseX() - posx) / ( 8));
        if(c_pos != caret_position) {

            selected_area = (c_pos < caret_position) ? make_pair(c_pos, caret_position) : make_pair(caret_position, c_pos);
            area_selected = true;
        }
    }

}
