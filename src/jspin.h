
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JSPIN_H
#define JSPIN_H

#include"jwidget.h"
#include"jvalue.h"
#include"jbutton.h"
#include"jcontainer.h"

/* JSpinBase is a simple drawing rectangle for various types of numbers
*/

template<typename T>
class JSpinBase : public JWidget
{
public:
    JSpinBase(T init ,float w = 100, float h = 40);
    JSpinBase(T init, float x, float y, float w, float h);
    ~JSpinBase();

    void setup() override;
    void draw() override;
    void update() override;

    T val;

    lsignal::signal<void(bool)> sigDragged;
    lsignal::signal<void(int)> sigScrolled;
protected:
    void clickedCbk(ofMouseEventArgs &mouse);
    void draggedCbk(ofMouseEventArgs &mouse);
    void releasedCbk(ofMouseEventArgs &mouse);
    void scrollCbk(ofMouseEventArgs &mouse);
    bool is_clicked;
    float y_click_start;
};

/*
 * JSpin is a spinbox with plus and minus buttons to change value of a number. It has limits as well (min max) and a step (increment / decrement step).
 * It inherits from JValue, and has its method "setValue" used to changed a value, and its signal sigValueChanged, used to connect value changes to other events.
*/

template<typename T>
class JSpin : public JWidget, public JValue<T>
{
public:
    JSpin(T init, T min, T max = std::numeric_limits<T>::max(), T step = 1, float w = 100, float h = 40);
    JSpin(T init, T min, T max, T step, float x, float y, float w, float h);
    ~JSpin();

    void setup() override;
    void update() override;
    void draw() override;

    void setValue(T v, bool triggerSig = true) override;
    JValue<T> getValue();

    T min, max, step;
    JButton minus, plus;
    JSpinBase<T> nbr_box;
    JContainer vcont, hcont;

protected:
    void initCallback();
};

#endif // JSPIN_H
