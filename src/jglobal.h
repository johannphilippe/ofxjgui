
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JGLOBAL_H
#define JGLOBAL_H

#define JCOLOR_WHITE 255, 255, 255
#define JCOLOR_BLACK 0, 0, 0
#define JCOLOR_RED 255, 0, 0
#define JCOLOR_BLUE 0, 0, 255
#define JCOLOR_GREEN 0, 255, 0
#define JCOLOR_LIGHTGREY 180, 180, 180
#define JCOLOR_MEDIUMGREY 140, 140, 140
#define JCOLOR_DARKGREY 80, 80, 80

#define JCOLOR_COOLRED 180, 0, 100
#define JCOLOR_COOLPURPLE 100, 0, 180

namespace JGlobal {
    // Maybe remove const to enable changing this parameter
    constexpr static const int DEFAULT_LINE_WIDTH = 2;
}

#endif // JGLOBAL_H
