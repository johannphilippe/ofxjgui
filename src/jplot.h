
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JPLOT_H
#define JPLOT_H

#include"jtypes.h"
#include"jwidget.h"
#include"jglobal.h"

/*
 * ----------------- JPlot --------------------
 * Simple Plot to draw samples
 * Several grid types (full, cross, or none)
 *
 * TODO :
 * - Implement grid cross, and several samples possibilities (cross, circle, line...)
*/
class JPlot : public JWidget
{
public:
    JPlot();
    JPlot(float w = 200, float h = 200);
    JPlot(float x, float y, float w, float h);
    ~JPlot();

    virtual void setup() override;
    virtual void update() override;
    virtual void draw() override;

    void drawGrid(const float halfX, const float halfY);
    virtual void drawSamples();

    void setGridType(JGridType grid);
    JGridType grid_type;

    virtual void clear();

    vector<glm::vec3> samples_positions;
    int grid_steps;
    void setGridSteps(int gs = 10);
protected:

    virtual void clickedCbk(ofMouseEventArgs &mouse);
    virtual void releasedCbk(ofMouseEventArgs &mouse);
    virtual void draggedCbk(ofMouseEventArgs &mouse);
    virtual void keyPressed(ofKeyEventArgs &key);
    virtual void keyReleased(ofKeyEventArgs &key);
};

#endif // JPLOT_H
