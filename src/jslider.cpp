/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jslider.h"
#include "jutils.h"
JSlider::JSlider(float w, float h) :
    JWidget(w, h), value(0), orientation(JOrientation::Vertical),
    fillColor(JCOLOR_COOLPURPLE)
{
}

JSlider::JSlider(float x, float y, float w, float h) :
    JWidget(x, y , w , h), value(0), orientation(JOrientation::Vertical),
    fillColor(JCOLOR_COOLPURPLE)
{
}

JSlider::JSlider(JOrientation o, float w, float h) :
    JWidget(w, h), value(0), orientation(o),
    fillColor(JCOLOR_COOLPURPLE)
{
}

JSlider::JSlider(JOrientation o, float x, float y, float w, float h) :
    JWidget(x, y , w , h), value(0), orientation(o),
    fillColor(JCOLOR_COOLPURPLE)
{
}

JSlider::~JSlider() {
    ofRemoveListener(ofEvents().mousePressed, this, &JSlider::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JSlider::releasedCbk);
    ofRemoveListener(ofEvents().mouseScrolled, this, &JSlider::scrolledCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JSlider::draggedCbk);
}

void JSlider::setFillColor(ofColor c)
{
    fillColor = c;
}

void JSlider::setOrientation(JOrientation o)
{
    orientation = o;
}

void JSlider::setup(){
    ofAddListener(ofEvents().mousePressed, this, &JSlider::clickedCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JSlider::releasedCbk);
    ofAddListener(ofEvents().mouseScrolled, this, &JSlider::scrolledCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JSlider::draggedCbk);
}

void JSlider::update()
{
}


void JSlider::draw()
{
    ofSetColor(JCOLOR_WHITE);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofNoFill();
    ofDrawRectangle(posx, posy, 0, width, height);
    ofFill();

    ofSetColor(fillColor);
    if(orientation == JOrientation::Vertical) {
        const float fill_low = (height * value);
        const float fill_high = (posy + height ) - fill_low;

        ofDrawRectangle(posx, fill_high, width, fill_low);
        if(display_value)
        {
            ofSetColor(JCOLOR_WHITE);
            std::string valstr(std::to_string(value));

            const float middleX = (posx + (width  / 2) ) - (4) ;
            const float middleY = (posy + (height / 2) ) - ( ( valstr.size() * 11 ) / 2 );
            string vert = JUtils::String::toVerticalString(valstr);
            ofDrawBitmapString(vert, middleX, middleY);
        }
    } else {
        const float fill_left = posx;
        const float fill_right = (width * value);
        ofDrawRectangle(fill_left, posy, 0, fill_right, height);
        if(display_value) {
            ofSetColor(JCOLOR_WHITE);
            std::string valstr(std::to_string(value));
            // find middle
            const float middleX = (posx + (width  / 2) )  - ((valstr.size() * 8) / 2);
            const float middleY = (posy + (height / 2) ) + (5.5);

            ofDrawBitmapString(valstr, middleX, middleY);
        }
    }

}

void JSlider::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    clicked_state = true;
    updateValue();
}

void JSlider::releasedCbk(ofMouseEventArgs &mouse)
{
    clicked_state = false;
}


void JSlider::draggedCbk(ofMouseEventArgs &mouse)
{
    if(!clicked_state) return;
    updateValue();
}

void JSlider::scrolledCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    const float inc = mouse.scrollY / 100;
    if(value + inc > 1.0) value = 1.0;
    else if(value + inc < 0.0) value = 0.0;
    else value += mouse.scrollY / 100;
    sigSliderValue(value);
}

void JSlider::updateValue()
{
    float diff;
    if(orientation == JOrientation::Vertical)
    {
        diff =  (height + posy) - getMouseY();
        value = diff / height;
    } else
    {
        diff =  getMouseX() - posx;
        value = diff / width;
    }

    value = JUtils::Math::limit<double>(value, 0.0, 1.0);

    sigSliderValue(value);
}

void JSlider::setDisplayValue(bool v){display_value = v;}
