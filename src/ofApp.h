
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#pragma once

#include "ofMain.h"
#include"jbutton.h"
#include"jcontainer.h"
#include"jcore.h"
#include"jslider.h"
#include"jfill.h"
#include"jsimplescope.h"
#include"jmultiscope.h"
#include"jspin.h"
#include"jplot.h"
#include"jcurveeditor.h"

class ofApp : public ofBaseApp{

public:
    ofApp();

    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);


    JButton button;
    JButton b2;
    JSlider vertSlider, horSlider;
    JFill fill;

    JButton b3, b4;
    JContainer cont1, cont2, cont3;

    vector<double> audio_sig;
    vector<double> sig2;

    JMultiScope<double> scope;

    JSpin<int> intSpin;
    JSpin<double> doubleSpin;


    void updateAudio();
    int radius;

    JContainer vcontainer;
    JCurveEditor plot;

    JButton linearBut,bezierBut;
    JContainer modeContainer;

};
