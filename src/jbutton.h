
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JBUTTON_H
#define JBUTTON_H

#include"jwidget.h"
#include"ofRectangle.h"
#include"ofGraphics.h"
#include"Dependencies/lsignal/lsignal.h"
#include"jutils.h"
#include"jglobal.h"
#include"jicon.h"

/*
 * ----------------- JButton -----------------
 * A simple button.
 * Example :
 * JButton but("MyButtonName", width, height);
 * but.sigButtonPressed.connect([&](){
 * 	// do something
 * };
 *
*/
// Simple Text Button with pressed and released click signals.
class JButton : public JWidget
{
public:
    JButton(std::string text, float w = 200, float h = 50);
    JButton(std::string text,float x, float y, float w , float h);
    ~JButton();

    void setup() override;
    void update() override;
    void draw() override;

    void setRadius(float radius);
    void setColor(ofColor c);
    void setColor(int r, int g, int b);

    std::string text;
    ofColor color;
    float radius;
    float text_x, text_y;
    float text_height, text_width;

    void click();

    lsignal::signal<void(void)> sigButtonPressed;
    lsignal::signal<void(void)> sigButtonReleased;

    bool clicked;
protected:

    virtual void buttonClicked(ofMouseEventArgs &mouse);
    virtual void buttonReleased(ofMouseEventArgs &mouse);
};


/*
 *  ---------------------- JPRettyButton -------------------
 * An animated and filled button, with customizable text color, background color,
 * radius, and click animation time.
*/

class JPrettyButton : public JButton
{
public:
    JPrettyButton(std::string text, float w = 200, float h = 50 );
    JPrettyButton(std::string text, float x, float y, float w, float h);
    ~JPrettyButton();

    void setup() override;
    void update() override;
    void draw() override;

    void setColors(ofColor back_color, ofColor sel_color, ofColor txt_color);
    void setAnimationTimeMs(int ms);
    void setAnimate(bool a);
protected:
    ofColor sel_color, text_color, current, c_text, inv_text, b_from, t_from;
    bool is_animating;
    int animation_ms;
    bool animate;
    JUtils::JChrono<int, chrono::milliseconds> chrono;

    void buttonClicked(ofMouseEventArgs &mouse) override;
    void buttonReleased(ofMouseEventArgs &mouse) override;
};

/*
 * --------------------- JIconButton ------------------------
 * A simple button with only an icon.
 *
*/

class JIconButton : public JWidget
{
public:
    JIconButton(JIcon const &icon, float w = 40, float h = 40);
    JIconButton(JIcon const &icon, float x, float y, float w, float h );
    ~JIconButton();


    void setup() override;
    void update() override;
    void draw() override;

    void setColor(ofColor const &c);
    void setRadius(float rad);
    void setIcon(JIcon const &icon);

    lsignal::signal<void(void)> sigButtonPressed;
    lsignal::signal<void(void)> sigButtonReleased;
protected:

    ofColor color;
    JIcon icon;
    bool clicked;
    float radius;

    void clickedCbk(ofMouseEventArgs &mouse);
    void releasedCbk(ofMouseEventArgs &mouse);
};


#endif // JBUTTON_H
