
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JSIMPLEANIMATION_H
#define JSIMPLEANIMATION_H

#include<iostream>
#include<functional>
#include"janimationbase.h"
#include"jcore.h"
#include"Dependencies/lsignal/lsignal.h"

class JAnimation : public JAnimationBase
{
public:
    JAnimation() {}
    JAnimation( std::function<void(double)> f, int ms_dur = 1000) : JAnimationBase(ms_dur),  func(f),
        mode(JAnimationMode::Linear)
    {
    }

    static JAnimation Exponential(std::function<void(double)> f, int ms_dur = 1000)
    {
        JAnimation a(f, ms_dur);
        a.mode = JAnimationMode::Exp;
        return a;
    }

    static JAnimation Logarithmic(std::function<void(double)> f, int ms_dur = 1000)
    {
        JAnimation a(f, ms_dur);
        a.mode = JAnimationMode::Log;
        return a;
    }

    void call() override {
        if(!is_started) return;
        int elapsed = chrono.timeSinceStart();
        if(elapsed > duration_ms) {
            stop();
            return;
        }

        double elapsed_p = 0.0;
        switch (mode) {
        case JAnimationMode::Linear:
        {
            elapsed_p = (double)elapsed / duration_ms;
            break;
        }
        case JAnimationMode::Exp:
        {

            elapsed_p = JUtils::Math::getCurve(0, 1, 1024, ((double)elapsed / duration_ms) * 1024, 10);
            break;
        }
        case JAnimationMode::Log:
        {
            elapsed_p = JUtils::Math::getCurve(0, 1, 1024, ((double)elapsed / duration_ms) * 1024, -10);
            break;
        }
        }

        func(elapsed_p);
    }
    std::function<void(double)> func;

protected:
    JAnimationMode mode;

};


#endif // JSIMPLEANIMATION_H
