
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JVALUE_H
#define JVALUE_H

#include"Dependencies/lsignal/lsignal.h"

/*
 * A simple number (int, float or double) with a signal callback system.
*/
template<typename T>
class JValue
{
public:
    JValue(): value(0) {}
    JValue(T v): value(v) {}

    lsignal::signal<void(T)> sigValueChanged;

    virtual void setValue(T v, bool triggerSig = true) {
        value = v;
        if(triggerSig)
            sigValueChanged(value);
    }

    JValue<T> operator+(JValue const &o)
    {
        return value + o.value;
    }

    JValue<T> operator-(JValue const &o)
    {
        return value - o.value;
    }

    JValue<T> operator=(JValue const &o) {
        value = o.value;
        return value;
    }

    T operator()(){
        return value;
    }

    T get() {
        return value;
    }

protected:
    T value;

};

#endif // JVALUE_H
