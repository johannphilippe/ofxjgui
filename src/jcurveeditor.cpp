
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jcurveeditor.h"
#include"jcore.h"
#include"jutils.h"
#include"Dependencies/libInterpolate/src/libInterpolate/Interpolate.hpp"

JCurveEditor::JCurveEditor(float w, float h) :
    JPlot(w, h),
    domain(DEFAULT_DOMAIN),curve_mode(JCurveMode::LinearLogExp),
    segment_first_index(-1), sel_index(-1), previous_mouseY(0)
{
    chrono.start();
}

JCurveEditor::JCurveEditor(float x, float y , float w , float h) :
    JPlot(x, y, w, h),
    domain(DEFAULT_DOMAIN),curve_mode(JCurveMode::LinearLogExp),
    segment_first_index(-1), sel_index(-1), previous_mouseY(0)
{
    chrono.start();
}

JCurveEditor::~JCurveEditor(){

    ofRemoveListener(ofEvents().mousePressed, this, &JCurveEditor::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased,  this, &JCurveEditor::releasedCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JCurveEditor::draggedCbk);
    ofRemoveListener(ofEvents().keyPressed, this, &JCurveEditor::keyPressed);
    ofRemoveListener(ofEvents().keyReleased, this, &JCurveEditor::keyReleased);
}

void JCurveEditor::addSample(glm::vec2 pos)
{
    addSample(pos.x, pos.y);
}

void JCurveEditor::addSample(float x, float y)
{
    if(samples_positions.size() == samples_positions.capacity())
    {
        samples_positions.reserve(samples_positions.size() + 10);
    }
    samples_positions.push_back(glm::vec3(x, y, 0));

}

void JCurveEditor::setup() {
    ofAddListener(ofEvents().mousePressed, this, &JCurveEditor::clickedCbk);
    ofAddListener(ofEvents().mouseReleased,  this, &JCurveEditor::releasedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JCurveEditor::draggedCbk);
    ofAddListener(ofEvents().keyPressed, this, &JCurveEditor::keyPressed);
    ofAddListener(ofEvents().keyReleased, this, &JCurveEditor::keyReleased);

    sigPositionChanged.connect([&](float, float) {
       processCurve();
    });

    sigResized.connect([&](float, float) {
        processCurve();
    });
}
void JCurveEditor::update() {}

void JCurveEditor::setDomain(size_t d)
{
    domain = d;
    processCurve();
}

void JCurveEditor::draw()
{
    const float halfX = (posx + (width / 2));
    const float halfY = (posy + (height / 2));

    ofNoFill();
    ofSetColor(JCOLOR_WHITE);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectangle(posx, posy , width, height);

    drawGrid(halfX, halfY);

    drawSamples();
}

void JCurveEditor::drawSamples()
{
    if(pline.size() == 0 && samples_positions.size() > 0)
        processCurve();
    //if(pline.size() == 0) processCurve();
    // avoid out of bounds
    for(auto &  it : samples_positions)
    {
        it.x = JUtils::Math::limit<double>(it.x, 0, 1);
        it.y = JUtils::Math::limit<double>(it.y, 0, 1);
    }

    // first sort x positions
    std::sort(samples_positions.begin(), samples_positions.end(), [](const glm::vec2 i1,const  glm::vec2 i2) {
        return i1.x < i2.x;
    });


    ofSetColor(JCOLOR_RED);
    ofFill();
    float px, py;

    glm::vec2 relative = getRelativeCoordinates();
    const int near_idx = hasNearSample(relative.x, relative.y);

    ofSetColor(JCOLOR_COOLPURPLE);
    ofSetLineWidth(3);
    pline.draw();

    for(size_t i = 0; i < samples_positions.size(); i++)
    {
        px = posx + (samples_positions[i].x * width);
        py = (posy + height) - (samples_positions[i].y * height);
        if( size_t(near_idx) == (i)) {
            ofSetColor(255, 255, 0);
        } else {
            ofSetColor(JCOLOR_RED);
        }
        ofDrawCircle(px, py, 0, 3);
    }
}

void JCurveEditor::processCurve()
{
    pline.clear();
    if(samples_positions.empty()) return;
    //resizeTaby();
    int w_index = 0;
    std::sort(samples_positions.begin(), samples_positions.end(), [](const glm::vec2 i1,const  glm::vec2 i2) {
        return i1.x < i2.x;
    });
    switch(curve_mode)
    {
    case JCurveMode::LinearLogExp:
    {
        for(size_t i = 0; i < samples_positions.size() - 1; ++i) {
            const double base1_begx = samples_positions[i].x;
            const double base1_fbx = samples_positions[i + 1].x;
            const double basedom_begx = floor( base1_begx * domain);
            const double basedom_fbx = floor(base1_fbx * domain);
            const double ampx = abs(basedom_begx - basedom_fbx);
            if(samples_positions[i].z != 0)
            {
                for(int lidx = 0; lidx < ampx; ++lidx)
                {
                    double cr;
                    if(samples_positions[i].y < samples_positions[i + 1].y) cr = -samples_positions[i].z;
                    else cr = samples_positions[i].z;
                    const double yval = (posy + height) -
                            ( getCurve(samples_positions[i].y, samples_positions[i + 1].y, ampx, lidx, cr) * height);
                    // put values in pixels coord values
                    const double xval = (((lidx + basedom_begx) / domain) * width) + posx;

                    pline.lineTo(xval, yval);
                    //tab_y[w_index] = glm::vec2(xval, yval);
                    w_index ++;
                //if(!isPixelInWidget(glm::vec2(xval,yval))) cout << "not in range for sample : " << i << endl;
                }
            } else  // straight line
            {
                const double px = (base1_begx * width) + posx;
                const double py = (posy + height) - (samples_positions[i].y * height);
                const double nx = (base1_fbx * width) + posx;
                const double ny = (posy + height) - (samples_positions[i + 1].y * height);
                if(i==0)pline.lineTo(px, py);
                pline.lineTo(nx, ny);
                //tab_y[w_index] = glm::vec2(px, py);
                //tab_y[w_index + 1] = glm::vec2(nx, ny);
                w_index += 2;
            }

        }
     break;
    }
    case JCurveMode::Spline:
    {
        if(samples_positions.size() >= 3) {
            vector<double> x(samples_positions.size()), y(samples_positions.size());
            for(size_t i = 0; i < samples_positions.size(); i++) {
                  x[i] = samples_positions[i].x;
                  y[i] = samples_positions[i].y;
            }
            _1D::CubicSplineInterpolator<double> interp;
            interp.setData(x, y);

            for(size_t i = 0; i < domain ; ++i) {
                 const double pos = double(i) / domain;
                 if(pos < samples_positions.front().x) continue;
                 if(pos > samples_positions.back().x) break;
                 const double py = (posy + height) - (  JUtils::Math::limit<double>(interp(pos), 0.0, 1.0) * height) ;
                 const double px = (pos * width) + posx;
                 pline.lineTo(px, py);
            }
        }
        break;
    }
    case JCurveMode::Bezier:
    {
        if(samples_positions.size() >= 3) {
           for(size_t i = 0; i < samples_positions.size() - 2; i +=2) {
               glm::vec2 p1 = toPixelCoordinates(samples_positions[i]);
               glm::vec2 p2 = toPixelCoordinates(samples_positions[i+1]);
               glm::vec2 p3 = toPixelCoordinates(samples_positions[i+2]);
               pline.quadBezierTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
           }
        }
        /*
        double arr[6];
        if(samples_positions.size() < 3) return;
        for(size_t i = 0; i < samples_positions.size(); i += 2)
        {
            if(i >= samples_positions.size() -2) break;

            const double base1_begx = samples_positions[i].x;
            const double base1_fbx = samples_positions[i + 1].x;
            const double basedom_begx = floor( base1_begx * domain);
            const double basedom_fbx = floor(base1_fbx * domain);
            const double ampx = abs(basedom_begx - basedom_fbx);
            const double miniamp = base1_begx - base1_fbx;
            const double scaleFactor = abs(1 / miniamp);

            arr[0] = (samples_positions[i].x - base1_begx) * scaleFactor;
            arr[1] = samples_positions[i].y;
            arr[2] = (samples_positions[i+1].x - base1_begx) * scaleFactor;
            arr[3] = samples_positions[i+1].y;
            arr[4] = (samples_positions[i+2].x - base1_begx) * scaleFactor;
            arr[5] = samples_positions[i+2].y;

            for(int lidx = 0; lidx < ampx -1; lidx ++ ) {
                glm::vec2 res = getBezier(lidx / ampx, arr);
                // from domain coord to relative coord
                res = toPixelCoordinates(res);
                tab_y[w_index] = res;
                w_index++;
            }
        }
        */
        break;

    }
    }
}

int JCurveEditor::hasNearSample(float x, float y)
{
    size_t idx = 0;
    for(;idx < samples_positions.size(); idx++)
    {
        float diff_x = abs(samples_positions[idx].x - x);
        float diff_y = abs(samples_positions[idx].y - y);
        if( (diff_x < 0.01) &&  (diff_y < 0.01) ) {
            return idx;
        }

    }
    return -1;
}

void JCurveEditor::snapToGrid()
{
    if(grid_type == JGridType::None) return;
    const double px = (samples_positions[sel_index].x * width);
    const double py = (samples_positions[sel_index].y * height);
    double nx = px, ny = py;

    const double x_step = width / grid_steps;
    const double y_step = height / grid_steps;
    for(int i = 0; i < grid_steps + 1; i++) {
        const double gx =  (x_step * i ) ;
        if(gx > px) {
            //distances with the sample up and down
            const double upx = gx - px;
            const double downx = px - (gx - x_step);
            if(downx < upx)
                nx = gx - x_step;
            else if(upx < downx) {
                nx = gx;
                break;
            }
            break;
        }
    }
    for(int i = 0; i < grid_steps + 1; i++) {
        const double gy = ( y_step * i);
        if(gy > py) {
            //distances with the sample up and down
            const double upy = gy - py;
            const double downy = py - (gy - y_step);
            if(downy < upy) {
                ny = gy - y_step;
            } else if(upy  < downy) {
                ny = gy;
                break;
            }
           break;
        }
    }

    nx += (posx);
    ny = (posy + height) - ny;

    glm::vec2 relative_res = toRelativeCoordinates(glm::vec2(nx, ny));
    samples_positions[sel_index].x = relative_res.x;
    samples_positions[sel_index].y = relative_res.y;

    for(size_t i = 0; i < samples_positions.size(); i++)
    {
        if(i == size_t(sel_index) ) continue;
        // swap to keep sorted
        if( size_t(sel_index) < i && samples_positions[sel_index].x >= samples_positions[i].x) {
            std::swap(samples_positions[i], samples_positions[sel_index]);
            sel_index = i;
        } else if( size_t(sel_index) > i && samples_positions[sel_index].x <= samples_positions[i].x) {
            std::swap(samples_positions[i], samples_positions[sel_index]);
            sel_index = i;
        }

    }
    processCurve();
}

void JCurveEditor::clickedCbk(ofMouseEventArgs &mouse)
{
    if(! is_selected) return;

    int last_click_ms = chrono.timeSinceLastTrigger();
    bool double_click = (last_click_ms < 200);

    cout << " is_selected / curve editor " << endl;
    // select sample

    if(mouse.button == 0) {
        clicked = true;
        // find near sample
        glm::vec2 rel = getRelativeCoordinates();
        int idx = hasNearSample(rel.x, rel.y);
        if(idx != -1 ) {
            // remove sample
            if(mouse.hasModifier(OF_KEY_SHIFT)) {
                samples_positions.erase(samples_positions.begin() + idx);
                processCurve();
            // select sample
            } else{
                sel_index = idx;
                if(double_click) {
                    // snap to grid
                    snapToGrid();
                    processCurve();
                }
            }
        }
        // modify curve (JCurveMode::LinearExpLog)
        else if(idx == -1 && mouse.hasModifier(OF_KEY_SHIFT)) {
            for(size_t i = 0; i < samples_positions.size() - 1; i++)
            {
                double base1_mouseX = (getMouseX() - posx) / width;
                if(samples_positions[i].x < base1_mouseX && samples_positions[i+1].x > base1_mouseX)
                {
                    segment_first_index = i;
                    return;
                }
            }

        }
        // else create a sample, or delete it if alt or so
        else {
            glm::vec2 rel(getRelativeCoordinates());
            //samples_positions.push_back(glm::vec3(rel.x, rel.y, 0));
            //samples_positions.emplace_back(rel.x, rel.y, 0);
            addSample(rel);
            sel_index = samples_positions.size();
            processCurve();
        }
    }

}


void JCurveEditor::releasedCbk(ofMouseEventArgs & /*mouse*/)
{
    clicked = false;
    sel_index = -1;
    segment_first_index = -1;
    previous_mouseY = 0;
}

void JCurveEditor::draggedCbk(ofMouseEventArgs & mouse )
{

    // move a sample
    if(clicked && sel_index != -1) {
        glm::vec2 relative = getRelativeCoordinates();
        relative.x = JUtils::Math::limit<double>(relative.x, 0, 1);
        relative.y = JUtils::Math::limit<double>(relative.y, 0, 1);
        samples_positions[sel_index].x = relative.x;
        samples_positions[sel_index].y = relative.y;
        for(size_t i = 0; i < samples_positions.size(); i++)
        {
            if(i == size_t(sel_index) ) continue;
            // swap to keep sorted
            if( size_t(sel_index) < i && samples_positions[sel_index].x >= samples_positions[i].x) {
                std::swap(samples_positions[i], samples_positions[sel_index]);
                sel_index = i;
            } else if( size_t(sel_index) > i && samples_positions[sel_index].x <= samples_positions[i].x) {
                std::swap(samples_positions[i], samples_positions[sel_index]);
                sel_index = i;
            }

        }

        processCurve();
        //*selected_sample = relative;
    }
    // segment selected, change curve factor
    else if(segment_first_index != -1 && mouse.hasModifier(OF_KEY_SHIFT)) {
        // find current segment
        if(previous_mouseY != 0) {
            const double diff = (  getMouseY() - previous_mouseY)/ 60;
            samples_positions[segment_first_index].z += (-diff);
            samples_positions[segment_first_index].z =
                    JUtils::Math::limit<double>(samples_positions[segment_first_index].z, -10, 10);
            processCurve();
        }
        previous_mouseY = getMouseY();
    }

}

void JCurveEditor::keyPressed(ofKeyEventArgs & /*key*/ )
{
}

void JCurveEditor::keyReleased(ofKeyEventArgs & /*key*/)
{
}

glm::vec2 JCurveEditor::getRelativeCoordinates()
{
    return glm::vec2( (getMouseX() - posx) / width, 1 - ((getMouseY() - posy) / height));
}

void JCurveEditor::setMode(JCurveMode m)
{
    curve_mode = m;
    processCurve();
}

double JCurveEditor::getCurve(double beg, double ending, int dur, int idx, double typ)
{
    double res;
    double type;
    if(typ == 0) {
        res = beg + (idx * (( ending - beg) / dur));
    }else {
        if(typ > 10) type = 10;
        else if(typ <  -10) type = -10;
        else type = typ;
        res = beg + (ending - beg) * (1 - exp(idx * type / (dur - 1))) / (1-exp(type));
    }
    return res;
}

glm::vec2 JCurveEditor::getBezier(double idx, double *vals)
{
    const double cX = 3 * (vals[2] - vals[0]);
    const double bX = -cX;
    const double aX = vals[4] - vals[0] - cX - bX;

    const double cY = 3 * (vals[3] - vals[1]);
    const double bY = -cY;
    const double aY = vals[5] - vals[1] - cY - bY;
    glm::vec2 res;
    res.x = (aX * pow(idx, 3)) + (bX * pow(idx, 2)) + (cX * idx) + vals[0];
    res.y = (aY * pow(idx, 3)) + (bY * pow(idx, 2)) + (cY * idx) + vals[1];
    return res;
}

void JCurveEditor::clear()
{
    samples_positions.clear();
    processCurve();
}
