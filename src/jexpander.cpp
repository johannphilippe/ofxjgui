
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "jexpander.h"
#include"jglobal.h"

JExpander::JExpander(JOrientation o, JWidget *wid, float w, float h) :
    JWidget(w, h),child(wid), orientation(o), is_expanded(false),
    background_color(ofColor::black), bar_size(20)
{
}

JExpander::~JExpander()
{
    ofRemoveListener(ofEvents().mousePressed, this, &JExpander::clickedCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JExpander::releasedCbk);
}

void JExpander::setup()
{
    child->setup();

    resizeChild();
    ofAddListener(ofEvents().mousePressed, this, &JExpander::clickedCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JExpander::releasedCbk);

    child->sigResized.connect([&](float, float ){
       this->resizeChild();
    });

}

void JExpander::update()
{
    const float mouseX = getMouseX();
    const float mouseY = getMouseY();

    if(is_expanded)
    {
        child->update();

        const J2DCoord pos = child->getCoord();
        switch (orientation) {
        case JOrientation::Vertical:
        {
            if(mouseX >= pos.x && mouseX <= (pos.x + pos.w) &&
                mouseY >= pos.y + bar_size && mouseY <= (pos.y + bar_size + pos.h)) {
                child->setSelected(true);
            } else
                child->setSelected(false);
            break;
        }
        case JOrientation::Horizontal:
        {
            if(mouseX >= pos.x + bar_size && mouseX <= (pos.x + bar_size + pos.w) &&
                    mouseY >= pos.y && mouseY <= (pos.y + pos.h)) {
                child->setSelected(true);
            } else
                child->setSelected(false);
            break;
        }

        }
    }

}

void JExpander::draw()
{
    // First draw border, then draw bar
    // Then child if it is expanded
    switch (orientation) {
    case JOrientation::Vertical:
    {
        // Draw background
        ofSetColor(background_color);
        ofFill();
        ofDrawRectangle(posx, posy, width, height);

        // draw  border
        ofNoFill();
        ofSetColor(ofColor::white);
        ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
        ofDrawRectangle(posx, posy, width, height);

        // draw expand button
        ofSetLineWidth(1);
        ofDrawRectangle(posx, posy, width, bar_size);

        const float centerX = width / 2;
        const float bcenterY = (posy + bar_size) - (bar_size /2);
        ofFill();
        if(is_expanded)
            ofDrawTriangle(centerX, bcenterY - 5, centerX - 5, bcenterY + 5, centerX + 5, bcenterY + 5);
        else
            ofDrawTriangle(centerX, bcenterY + 5, centerX - 5, bcenterY - 5, centerX + 5, bcenterY - 5);

        // then if is expanded, draw the child
        if(! is_expanded) return;
        child->draw();

        break;
    }
    case JOrientation::Horizontal:
    {
        break;
    }
    }

}

void JExpander::setBarSize(float bs)
{
    bar_size = bs;
}

void JExpander::resizeChild()
{
    switch (orientation) {
    case JOrientation::Vertical:
    {
        //width = child->width;
        //height = (is_expanded ) ? (child->height + bar_size) : bar_size;

        child->setPosY(posy + bar_size);
        this->resize(child->width, (is_expanded) ? ( child->height + bar_size) : bar_size);
        break;
    }
    case JOrientation::Horizontal:
    {
        //height = child->height;
        //width = (is_expanded) ? (child->width + bar_size) : bar_size;
        child->setPosX(posx + bar_size);
        this->resize((is_expanded) ? (child->width + bar_size) : bar_size, child->height);

        break;
    }
    }
    if(handle_name.compare(string("expander")) == 0) {
        cout << "expander wh : " << width << " " << height << endl;
    }
}

void JExpander::updateChildPos()
{

}

void JExpander::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    switch(orientation)
    {
    case JOrientation::Vertical:
    {
        const float mouseY = getMouseY();
        if(mouseY > posy && mouseY < (posy + bar_size))
        {
            is_expanded = !is_expanded;
            resizeChild();
            sigExpanderStateChanged(is_expanded);
        }
        break;
    }
    case JOrientation::Horizontal:
    {
        const float mouseX = getMouseX();
        if(mouseX > posx && mouseX < (posx + bar_size))
        {
            is_expanded = !is_expanded;
            resizeChild();
            sigExpanderStateChanged(is_expanded);
        }
        break;
    }

    }
}

void JExpander::releasedCbk(ofMouseEventArgs &mouse)
{

}

