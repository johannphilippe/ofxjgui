
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "ofApp.h"

void ofApp::updateAudio()
{
    for(size_t i = 0; i < audio_sig.size(); i++)
    {
        float idx = (float)i / audio_sig.size();
        audio_sig[i] = sin(M_PI * idx * radius * 0.001) * 0.4;
        radius += 1;
        sig2[i] = sin(M_PI * idx * 0.03 * (radius * 0.0001)) * 0.5;
    }
}

//--------------------------------------------------------------
ofApp::ofApp() :
    button("helloworld", 200, 50), b2("bouton2", 180, 50), vertSlider(30, 200), horSlider(200, 30), fill(50, 50),
    b3("but3"), b4("but4"), scope(1000, 400), intSpin(1, 0, 20, 1), doubleSpin(1.0, 0.0, 5.5, .01), plot(800, 400),
    linearBut("Linear", 100, 40), bezierBut("Bezier", 100, 40)
    //vcontainer(JOrientation::Vertical, &button, &b2, &vertSlider, &horSlider) //, vcontainer(JOrientation::Vertical, &button, &b2, &vertSlider, &horSlider)
{
    cout << "Empty container address " << &vcontainer << endl;
    horSlider.setOrientation(JOrientation::Horizontal);
    horSlider.setDisplayValue(true);
    vertSlider.setOrientation(JOrientation::Vertical);
    vertSlider.setDisplayValue(true);
}

void ofApp::setup(){
    linearBut.setColor(ofColor(100, 255, 0));
    bezierBut.setColor(ofColor(JCOLOR_BLACK));

    linearBut.sigButtonPressed.connect([&](){
        linearBut.setColor(ofColor(100, 255, 0));
        bezierBut.setColor(ofColor(0, 0, 0));
       plot.setMode(JCurveMode::LinearLogExp);
    });
    bezierBut.sigButtonPressed.connect([&](){
        bezierBut.setColor(ofColor(255, 100, 0));
        linearBut.setColor(ofColor(0));
       plot.setMode(JCurveMode::Bezier);
    });


    audio_sig.resize(1024);
    sig2.resize(1024);

    scope.addSignal(JScopedSignalDescriptor<double>("sine" , &audio_sig, ofColor(JCOLOR_RED) ));
    scope.addSignal(JScopedSignalDescriptor<double>("sqrt" , &sig2, ofColor(JCOLOR_BLUE) ));
    scope.setGridtype(JGridType::Full);
    scope.setLegend(true, JCornerPosition::BottomRight);

    plot.setGridType(JGridType::Full);
    //plot.curve_mode = JCurveMode::Bezier;

    for(int i = 0; i < 10; i++)
    {
        plot.samples_positions.push_back(glm::vec3((float)i/ 10.0, (float)i / 10.0, 0));
    }

    cont1 = JContainer(JOrientation::Horizontal, &button, &b2, &fill, &vertSlider, &horSlider);
    cont2 = JContainer(JOrientation::Horizontal, &b3, &b4, &intSpin, &doubleSpin);
    modeContainer = JContainer(JOrientation::Vertical, &linearBut, &bezierBut);
    cont3 = JContainer(JOrientation::Horizontal, &scope, &plot, &modeContainer);
    vcontainer = JContainer(JOrientation::Vertical, &cont1, &cont2, &cont3);

    cout << "Full container address " << &vcontainer << endl;
    JCore::setup();
    button.setColor(ofColor(180, 0, 110));
    b2.setColor(ofColor(110, 0, 180));
    vertSlider.setFillColor(ofColor(JCOLOR_COOLRED));
}

//--------------------------------------------------------------
void ofApp::update(){
    updateAudio();
    JCore::update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    //ofBackground(0, 0, 0);
    JCore::draw();
    ofSetColor(255, 255, 255);
    ofDrawBitmapString(ofToString(ofGetFrameRate()) + " fps", 50, ofGetHeight() - 50, 0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
