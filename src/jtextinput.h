
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JTEXTINPUT_H
#define JTEXTINPUT_H

#include"jtextinputbase.h"
#include"jtypes.h"

/*
 * ------------- JTextField ------------------
 * More sophisticated text field than JTextInputBase.
 * Allows multiline, fixed number of lines or automatically resizing, font type and size...
 *
*/

class JTextInput: public JTextInputBase
{
public:
    constexpr static const int LINE_GAP = 4;

    JTextInput(ofTrueTypeFontSettings const & settings, JMultilineMode m = JMultilineMode::Disabled, float w = 200, float h = 50);
    JTextInput(ofTrueTypeFontSettings const & settings, JMultilineMode m, float x, float y, float w, float h);
    ~JTextInput();

    void setup() override;
    void update() override;
    void draw() override;

    void setNumberOfLines(int l);

    void loadFont(ofTrueTypeFontSettings s);
    void loadFont(std::string fontPath, int size);

    void setMultilineMode(JMultilineMode m);


protected:
    float caret_posx;

    bool multiline;
    int n_line;
    ofTrueTypeFont font;

    ofTrueTypeFontSettings font_settings;

    JMultilineMode mode;

    void keyCbk(ofKeyEventArgs &key) override;
    void clickCbk(ofMouseEventArgs &mouse) override;
    void releasedCbk(ofMouseEventArgs &mouse) override;
    void draggedCbk(ofMouseEventArgs &mouse) override;

    void calculateHeight();
    void drawString();

    string caret_string;

    int getDisplayable() override;
};

#endif // JTEXTINPUT_H
