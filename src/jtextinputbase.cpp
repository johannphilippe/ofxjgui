
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jtextinputbase.h"
#include"jcore.h"

JTextInputBase::JTextInputBase(float w, float h):
    JWidget(w, h), caret_position(0), background_color(0), text_color(255)
{
    if(height < 20) height = 20;
}

JTextInputBase::JTextInputBase(float x, float y , float w, float h):
    JWidget(x, y , w, h), caret_position(0), background_color(0), text_color(255)
{
    if(height < 20) height = 20;
}

JTextInputBase::~JTextInputBase()
{
    ofRemoveListener(ofEvents().keyPressed, this, &JTextInputBase::keyCbk);
    ofRemoveListener(ofEvents().mousePressed, this, &JTextInputBase::clickCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JTextInputBase::releasedCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JTextInputBase::draggedCbk);
}

void JTextInputBase::setup(){
    chrono.start();
    dclick_chrono.start();

    ofAddListener(ofEvents().keyPressed, this, &JTextInputBase::keyCbk);
    ofAddListener(ofEvents().mousePressed, this, &JTextInputBase::clickCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JTextInputBase::releasedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JTextInputBase::draggedCbk);
}

void JTextInputBase::update()
{
}

void JTextInputBase::draw()
{

    // draw limits
    ofFill();
    ofSetColor(background_color);
    ofDrawRectangle(posx, posy, width, height);
    ofNoFill();
    ofSetColor(background_color.getInverted());
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectangle(posx, posy, width, height);

    // draw caret
    if(editing && (chrono.timeSinceStart() % 2 ) == 0) {
        ofSetLineWidth(1);
        const float cpx = LEFT_OFFSET + posx + (caret_position * 8);
        ofDrawLine(glm::vec2(cpx, (posy + height) - 2), glm::vec2(cpx, posy + 2));
    }

    // draw text
    if(!text.empty()) {
        if(area_selected) {
            const int sel_area_begx = (selected_area.first * 8) + LEFT_OFFSET + posx;
            const int sel_area_width = ( abs(selected_area.second - selected_area.first) * 8);
            ofFill();
            ofSetColor(0, 0, 255, 150);
            ofDrawRectangle(sel_area_begx, posy, 0, sel_area_width, height);
            ofNoFill();
        }


        ofSetColor(text_color);
        // calculate size of each line in characters, then
        ofDrawBitmapString(text, posx + LEFT_OFFSET, (posy + height) - 5);
    }

}

void JTextInputBase::setText(string txt)
{
    if(txt.size() > size_t(getDisplayable())) {
        text = txt.substr(0, getDisplayable());
        return;
    }
    text = txt;
}

void JTextInputBase::appendText(string txt)
{
    text += txt;
}

void JTextInputBase::prependText(string txt)
{
    text = string(txt + text);
}

void JTextInputBase::insertText(int index, string txt)
{
    text.insert(index, txt);
}

void JTextInputBase::insertTextAtCaret(string txt)
{
    text.insert(caret_position, txt);
}

void JTextInputBase::clear()
{
    text.clear();
}

int JTextInputBase::textSize()
{
    return text.size();
}

void JTextInputBase::setCaretPosition(int i)
{
    caret_position = JUtils::Math::limit<int>(i, 0, textSize());
    chrono.start();
    area_selected = false;
}

string JTextInputBase::getText() {return text;}

int JTextInputBase::getDisplayable()
{
   return int(width / 8.0)- 1;
}

void JTextInputBase::keyCbk(ofKeyEventArgs &key)
{
    cout << "key " << key.key  << " mod : " << key.modifiers << endl;

    if(!editing) return;

    if(JUtils::Key::isPrintable(key.key) && key.modifiers == 0) {

        const int displayable = int(width / 8.0)- 1;
        if(area_selected) {
            text.erase(selected_area.first, selected_area.second);
            if(textSize() >= displayable) return;
            text.insert(selected_area.first, JUtils::Key::toString(key.key));
            setCaretPosition(selected_area.first + 1);
            area_selected = false;

        } else {
            if(textSize() >= displayable) return;
            text.insert(caret_position, JUtils::Key::toString(key.key));
            setCaretPosition(caret_position + 1);
        }
    } else if(JUtils::Key::isPrintable(key.key) && key.modifiers != 0) {
        int mod = JUtils::Key::getModifier(key.key, key.modifiers);
        switch(mod)
        {
        case JUtils::Key::CTRL_C:
        {
            // to clipboard
            if(editing && area_selected) {
                //
                string to_copy = text.substr(selected_area.first, selected_area.second);
                JUtils::System::toClipboard(to_copy);
            }
            break;
        }
        case JUtils::Key::CTRL_V:
        {
            string clipboard = JUtils::System::getClipboard();
            if(clipboard.empty()) return;
            if(editing && area_selected)
            {
                // replace
                text.erase(selected_area.first, selected_area.second);
                text.insert(caret_position, clipboard);
                setCaretPosition(selected_area.first + clipboard.size());
                area_selected = false;
            } else if(!area_selected)
            {
                text.insert(caret_position, clipboard);
                setCaretPosition(caret_position + clipboard.size());
            }
            break;
        }
        case JUtils::Key::CTRL_X:
        {
            if(editing && area_selected) {
                string to_copy = text.substr(selected_area.first, selected_area.second);
                JUtils::System::toClipboard(to_copy);
                text.erase(selected_area.first, selected_area.second);
                setCaretPosition(selected_area.first);
                area_selected = false;
            }
            break;
        }
        case JUtils::Key::CTRL_A:
        {
            area_selected = true;
            selected_area = make_pair(0, textSize());
            break;
        }
        }

    } else {
        cout << key.key << endl;
        switch(key.key) {
        case OF_KEY_LEFT:
        {
            setCaretPosition(caret_position - 1);
            break;
        }
        case OF_KEY_RIGHT:
        {
            setCaretPosition(caret_position + 1);
            break;
        }
        case OF_KEY_BACKSPACE:
        {
            if(area_selected) {
                text.erase(selected_area.first, selected_area.second);
                setCaretPosition(selected_area.first);
                area_selected = false;
                break;
            }
            if(caret_position <= 0) break;
            text.erase(text.begin() + caret_position - 1);
            setCaretPosition(caret_position - 1);
            break;
        }

        }
    }
}

void JTextInputBase::clickCbk(ofMouseEventArgs &mouse)
{
    if(is_selected && mouse.button == 0) {
        area_selected = false;
        bool double_click = (dclick_chrono.timeSinceLastTrigger() < 150) ? true : false;
        editing = true;
        if(double_click) {
            selectText(0, textSize());
            area_selected = true;
        } else {
            // find position of caret
            const int c_pos = int(int(ofGetMouseX() - posx) / ( 8));
            setCaretPosition(c_pos);
        }

    } else {
        editing = false;
    }
}

void JTextInputBase::releasedCbk(ofMouseEventArgs &mouse)
{
}

void JTextInputBase::draggedCbk(ofMouseEventArgs &mouse)
{
    if(is_selected && editing) {
        const int c_pos = int(int(ofGetMouseX() - posx) / ( 8));
        if(c_pos != caret_position) {

            selected_area = (c_pos < caret_position) ? make_pair(c_pos, caret_position) : make_pair(caret_position, c_pos);
            area_selected = true;
        }
    }

}

void JTextInputBase::selectText(int from, int to)
{
    if(from >= 0 && to <= textSize()) {
        selected_area = make_pair(from, to);
    }
}
