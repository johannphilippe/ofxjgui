#ifndef JWAITANIMATION_H
#define JWAITANIMATION_H

#include"jtypes.h"
#include<atomic>
#include<iostream>

/*
 *  ---------------- JWaitAnimation ----------------------------
 * An animation for threaded widgets(loading times).
 *
 * TODO :
 * - Implement a fade in / fade out  ?
 *
*/

class JWaitAnimation
{
public:
    JWaitAnimation(float w, float h);
    JWaitAnimation(float x, float y, float w, float h);
    ~JWaitAnimation();
    // to call if mode is a filled mode
    void setColors(ofColor const &back, ofColor const &fill, ofColor const &text);

    virtual void draw();
    virtual void draw(float x, float y);
    virtual void update();
    virtual void setup();


    void setPosition(float x, float y);

    void setValue(double v);
    void setMode(JWaitAnimationType mode);
protected:
    float posx, posy, width, height;
    ofColor back_color, fill_color, text_color;
    std::atomic<double> index;
    JWaitAnimationType mode;

    std::string text_to_display;
};

#endif // JWAITANIMATION_H
