
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JTOASTBASE_H
#define JTOASTBASE_H

#include<iostream>
//#include"jcore.h"
#include"jutils.h"
#include"jglobal.h"

class JToastBase
{
public:

    JToastBase(string text, int dur_ms, float x, float y) : text(text), duration_ms(dur_ms), pos(x, y)
    {
        chrono.start();
    }

    void draw() {
        ofSetColor(JCOLOR_WHITE);
        ofDrawBitmapString(text, pos.x, pos.y);
    }

    string text;
    int duration_ms;
    JUtils::JChrono<int, chrono::milliseconds> chrono;
    glm::vec2 pos;
};

#endif // JTOASTBASE_H
