
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JWIDGET_H
#define JWIDGET_H

#include"ofBaseTypes.h"
#include"ofMain.h"
#include"jtypes.h"
#include"jwaitanimation.h"
#include"Dependencies/lsignal/lsignal.h"


// base widget, does not draw anything. Any widget must inherit from JWidget.
//All inherited class must override update and draw methods (both draw methods).
class JWidget
{
public:
    JWidget();
    JWidget(float w, float h);
    JWidget(float x, float y, float w, float h);
    virtual ~JWidget();

    J2DCoord getCoord();

    virtual void setSelected(bool sel_state);
    virtual void setChildrenState(bool child_state);
    virtual bool hasParent();

    virtual void setup() = 0;
    virtual void update() = 0;
    virtual void draw() = 0;

    virtual void setMapped(bool map);

    virtual void setPosition(glm::vec2 pos);
    virtual void setPosition(float x, float y);
    virtual void resize(float w, float h);
    virtual void resize(glm::vec2 const &size);

    virtual void setPosX(const float posx);
    virtual void setPosY(const float posy);
    virtual void setWidth(const float width);
    virtual void setHeight(const float height);

    // will return coordinates from 0 to 1
    virtual glm::vec2 toRelativeCoordinates(glm::vec2 coord);
    virtual glm::vec2 toPixelCoordinates(glm::vec2 relative);
    virtual bool isPixelInWidget(glm::vec2 pix);

    lsignal::signal<void(float, float)> sigResized;
    lsignal::signal<void(float, float)> sigPositionChanged;

    float width, height;
    float posx, posy;

    void updateMask(float x, float y, float w, float h);
    void updateMask(const J2DCoord &coord);

    J2DCoord getMask();
    J2DCoord mask;

    float mouse_offsetX, mouse_offsetY;
    float getMouseY();
    float getMouseX();

    string handle_name;
protected:

    bool is_selected;
    bool has_parent;
    bool is_mapped;

private:
};


class JThreadedWidget : public JWidget, public ofThread
{
public:
    JThreadedWidget(float w, float h);
    JThreadedWidget(float x, float y, float w, float h);
    void wait(bool w);
    bool isWaiting();

protected:
    atomic<bool> should_wait;
    JWaitAnimation wait_animation;
};

#endif // JWIDGET_H
