
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JFILL_H
#define JFILL_H

#include"jwidget.h"

class JFill:  public JWidget
{
public:
    JFill(float w = 50, float h = 50) : JWidget(w, h)
    {
    }
    JFill(float x, float y, float w, float h) : JWidget(x, y, w, h)
    {
    }

    JFill(ofColor c, float w = 50, float h = 50) : JWidget(w, h),
        has_color(true), color(c)
    {
    }
    JFill(ofColor c, float x, float y, float w, float h) : JWidget(x, y, w, h),
        has_color(true), color(c)
    {
    }
    ~JFill()
    {

    }

    void setup() override
    {

    }

    void update() override
    {

    }

    void draw() override
    {
        if(has_color) {
            ofFill();
            ofSetColor(color);
            ofDrawRectangle(posx, posy, width, height);
        }
    }

    bool has_color;
    ofColor color;
};

#endif // JFILL_H
