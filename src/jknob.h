
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JKNOB_H
#define JKNOB_H

#include"jgui.h"
#include"Dependencies/lsignal/lsignal.h"

/*
 * ----------------- JKNOB ----------------------
 * Simple circular Knob, with fixed limits (defined by user)
 *
*/

class JKnob : public JWidget
{
public:
    JKnob();
    JKnob(double min, double max,float w = 50, float h = 50);
    JKnob(double min, double max, float x, float y, float w, float h);
    ~JKnob();

    void draw() override;
    void update() override;
    void setup() override;

    ofColor color;
    void setColor(ofColor c);

    double min, max, value;
    string val_str;
    lsignal::signal<void(float)> sigValueChanged;

protected:
    void clickedCbk(ofMouseEventArgs &mouse);
    void releasedCbk(ofMouseEventArgs &mouse);
    void draggedCbk(ofMouseEventArgs &mouse);
    void scrolledCbk(ofMouseEventArgs &mouse);
    bool is_clicked;
    float y_click_start;
};

#endif // JKNOB_H
