
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "jicon.h"

JIcon::JIcon(std::function<void(float, float, float, float)> f, bool _fill) :
    draw_func(f), fill(_fill)
{
}

/*
 *  Static constructors
 *
*/
JIcon JIcon::Circle(bool f)
{
    JIcon icon(&JIcon::drawCircle, f);
    return icon;
}

JIcon JIcon::Square(bool f)
{
    JIcon icon(&JIcon::drawSquare, f);
    return icon;
}

JIcon JIcon::Play(bool f)
{
   JIcon icon(&JIcon::drawPlay, f);
   return icon;
}

JIcon JIcon::Pause(bool f)
{
   JIcon icon(&JIcon::drawPause, f);
   return icon;
}

JIcon JIcon::Stop(bool fill) {return Square(fill);}
JIcon JIcon::Record(bool fill) {return Circle(fill);}

/*
 *  Static draw
 *
*/
void JIcon::drawPlay(float x, float y, float w, float h)
{
    ofDrawTriangle(x, y, x, y + h, x + w, y + (h / 2));
}

void JIcon::drawPause(float x, float y, float w, float h)
{
    ofDrawRectangle(x, y, w / 3, h);
    ofDrawRectangle(x + ((w / 3) * 2) , y, w / 3, h);
}

void JIcon::drawCircle(float x, float y, float w, float h)
{
    ofDrawCircle(x + (w / 2), y + (h / 2), (std::min(w, h) / 2));
}

void JIcon::drawSquare(float x, float y, float w, float h)
{
    ofDrawRectangle(x, y, std::min(w,h), std::min(w, h));
}

void JIcon::setup()
{
}

void JIcon::update()
{
}

void JIcon::draw(float x, float y, float w ,float h)
{
    ofSetColor(color);
    if(fill) ofFill();
    else ofNoFill();
    draw_func(x, y, w, h);
}


void JIcon::setColor(const ofColor &c)
{
    color = c;
}

void JIcon::setFill(const bool f)
{
    fill = f;
}
