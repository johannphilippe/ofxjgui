
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "jfftview.h"
#include"jglobal.h"
#include"jutils.h"
#include"jcore.h"
#include"Dependencies/ofxPixelsShape/src/ofxPixelsShape.h"
#include"sndfile.hh"
#include"Dependencies/AudioFFT/AudioFFT.h"

template<typename T>
JFFTView<T>::JFFTView(float w , float h):
    JThreadedWidget(w,h), mode(JFFTDisplayMode::Linear)
{
}

template<typename T>
JFFTView<T>::JFFTView(float x, float y, float w , float h):
    JThreadedWidget(x, y , w, h), mode(JFFTDisplayMode::Linear)
{
}

template<typename T>
void JFFTView<T>::setup() {
    pix.allocate(width, height, OF_PIXELS_RGB);
    toscreen_pix.allocate(width, height, OF_PIXELS_RGB);
    img.allocate(width, height, OF_IMAGE_COLOR);
    img.setColor(ofColor::black);

    wait_animation.setColors(ofColor(JCOLOR_COOLRED, 130), ofColor(JCOLOR_COOLPURPLE), ofColor::white);
    //wait_animation.setMode(JWaitAnimationType::ProgressBarDisplayValue);
    wait_animation.setMode(JWaitAnimationType::SimpleCircular);
}

template<typename T>
void JFFTView<T>::update()
{
    new_frame = false;
    while(channel.tryReceive(toscreen_pix)) {
        new_frame = true;
    }
    if(! new_frame) return;

    cout << img.getWidth() << " " << img.getHeight() << endl;
    img.setFromPixels(toscreen_pix);
    should_wait = false;
}

template<typename T>
void JFFTView<T>::draw()
{
    ofNoFill();
    ofSetColor(ofColor::white);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectangle(posx, posy, width, height);

    // add condition ("if ImgIsLoaded");
    img.draw(posx, posy, width, height);

    if(should_wait) {
        //JCore::drawWaitingScreen(ofColor(255, 255, 255, 100), ofColor::white, posx, posy, width, height);
        wait_animation.draw(posx, posy);
    }
    ofDrawBitmapString("fps : " + ofToString(ofGetFrameRate()), 100, 100);
}


template<typename T>
void JFFTView<T>::threadedFunction()
{
    should_wait = true;
    if(process_fft) {
        processFFTAndDraw();
    } else {
        drawFbo();
    }
}

template<typename T>
void JFFTView<T>::setSoundfile(string &path, size_t fft_len, size_t hop)
{
    this->file_path = path;
    this->fft_size = fft_len;
    this->overlap = hop;
    process_fft = true;
    startThread();
}

template<typename T>
void JFFTView<T>::processFFTAndDraw()
{
    if(!filesystem::is_regular_file(file_path))
        return;

    SndfileHandle snd(file_path.c_str());
    samplerate = snd.samplerate();
    size_t fft_size = 2048;
    vector<T> buf(fft_size * snd.channels());
    vector<T> ch_buf(fft_size);

    size_t rcnt = 0;

    audiofft::AudioFFT<T> fft;
    fft.init(fft_size);

    real_data.resize( (snd.frames() / (fft_size / overlap) )  + 2 );
    imag_data.resize( (snd.frames() / (fft_size / overlap) ) + 2 );

    for(size_t i = 0; i < real_data.size(); i++) {
        real_data[i].resize(audiofft::AudioFFT<T>::ComplexSize(fft_size));
        imag_data[i].resize(audiofft::AudioFFT<T>::ComplexSize(fft_size));
    }

    size_t cnt = 0;
    size_t passed = 0;


    ofColor c;

    ofSetLineWidth(2);
    const float luminosity = 2;

    float max = 0;

    const float x_ratio = width / float(real_data.size());
    float y_ratio = (height / (real_data[0].size() / 2));
    //const float x_step = (width / real_data.size());

    float y_step;

    ::memset(pix.getData(), 0,
             sizeof(unsigned char) * (pix.getWidth() * pix.getHeight() * pix.getNumChannels()));


    do {
        wait_animation.setValue(cnt / (double)real_data.size());
        snd.seek(passed, SF_SEEK_SET);
        rcnt = snd.readf(buf.data(), fft_size);
        for(size_t i = 0; i < fft_size; i+= snd.channels()) {
            ch_buf[i] = buf[i];
        }

        fft.fft(ch_buf.data(), real_data[cnt].data(), imag_data[cnt].data());



        // Immediatly draw
        const float px = cnt * x_ratio;
        for(size_t i = 0; i < real_data[cnt].size() / 2; ++i) {
            float val = sqrt(pow(real_data[cnt][i], 2) + pow(imag_data[cnt][i], 2)) ;
            if(val < 1.0) continue;
            val *= luminosity;

            float py = 0;
            switch(mode) {
            case JFFTDisplayMode::Linear:
            {
                y_step = (height) - ((i+1) * (y_ratio));
                py = (height) - (i *   (y_ratio) );
                break;
            }
            case JFFTDisplayMode::Log:
            {
                py =  height - (JUtils::Math::ScaledLog<float>(i / y_ratio, 1, height) - 1);
                y_step = height -  (JUtils::Math::ScaledLog<float>((i +1) / y_ratio, 1, height) - 1);
                break;
            }
            case JFFTDisplayMode::Bark:
            {
                const double bark_multiplier = (double) height / 24.0;
                const float multiplier = float( (samplerate/ 2) / (real_data[cnt].size()));
                const double bark_band = JUtils::Math::BarkBand(i * multiplier) - 1;
                py = height - (bark_band * bark_multiplier);
                y_step = JUtils::Math::BarkBand((i+1) * multiplier) - 1;
                y_step = height - (y_step * bark_multiplier);

                break;
            }
            }

            if(val > max) max = val;

            val = JUtils::Math::limit<int>(int(val), 0, 255);

            c.r = JUtils::Math::ScaledLog<int>(val + 1, 1, 256) - 1;
            c.g = (val > 150) ? c.r / 2 : 0;
            c.b = val / 4 + ((c.r > 180) ? 0 : c.r);


            shape.draw_point(pix, ofPoint(px +1 , py - 1), c);
            //shape.draw_line(pix, ofPoint(0, 0), ofPoint(rand() % int(width), rand() % int(height) ), ofColor::white);
        }

        cnt++;
        passed += fft_size / overlap;
    } while(rcnt > 0);

    channel.send( pix );
}


template<typename T>
void JFFTView<T>::drawFbo()
{
    ofColor c;

    ofSetLineWidth(2);
    const float luminosity = 2;

    float max = 0;

    const float x_ratio = width / float(real_data.size());
    float y_ratio = (height / (real_data[0].size() / 2));
    //const float x_step = (width / real_data.size());

    float y_step;

    ::memset(pix.getData(), 0, sizeof(unsigned char) * (pix.getWidth() * pix.getHeight() * pix.getNumChannels()));

    for(size_t frame = 0; frame < real_data.size(); ++frame) {
        wait_animation.setValue(frame / (double)real_data.size());
        const float px = frame * x_ratio;
        //const float npx = (frame + 1) * x_ratio;

        for(size_t i = 0; i < real_data[frame].size() / 2; ++i) {
            float val = sqrt(pow(real_data[frame][i], 2) + pow(imag_data[frame][i], 2)) ;
            if(val < 1.0) continue;
            val *= luminosity;

            float py = 0;
            switch(mode) {
            case JFFTDisplayMode::Linear:
            {
                //y_step = (height / real_data[0].size());
                y_step = (height) - ((i+1) * (y_ratio));
                py = (height) - (i *   (y_ratio) );
                break;
            }
            case JFFTDisplayMode::Log:
            {
                py =  height - (JUtils::Math::ScaledLog<float>(i / y_ratio, 1, height) - 1);
                //y_step = height / ( (JUtils::Math::ScaledLog<float>((i+1) / y_ratio, 1, height) -1) - py );
                y_step = height -  (JUtils::Math::ScaledLog<float>((i +1) / y_ratio, 1, height) - 1);
                        //(height /  JUtils::Math::ScaledLog<float>((i+1) / y_ratio, 1, height) - 1);
                break;
            }
            case JFFTDisplayMode::Bark:
            {
                const double bark_multiplier = (double) height / 24.0;
                const float multiplier = float( (samplerate/ 2) / (real_data[frame].size()));
                const double bark_band = JUtils::Math::BarkBand(i * multiplier) - 1;
                py = height - (bark_band * bark_multiplier);
                //y_step =  height / ( (bark_multiplier * (JUtils::Math::BarkBand(i + 1 * multiplier) - 1)) - py );
                y_step = JUtils::Math::BarkBand((i+1) * multiplier) - 1;
                y_step = height - (y_step * bark_multiplier);

                break;
            }
            }

            if(val > max) max = val;

            val = JUtils::Math::limit<int>(int(val), 0, 255);

            c.r = JUtils::Math::ScaledLog<int>(val + 1, 1, 256) - 1;
            c.g = (val > 150) ? c.r / 2 : 0;
            c.b = val / 4 + ((c.r > 180) ? 0 : c.r);


            shape.draw_point(pix, ofPoint(px +1 , py - 1), c);
            //shape.draw_rectangle(pix, ofRectangle(px, py,  x_step, abs(y_step - py) ), c);
        }
    }
    channel.send( pix );
}

template<typename T>
void JFFTView<T>::setMode(JFFTDisplayMode m)
{
    mode = m;
    process_fft = false;
    startThread();
}

template class JFFTView<float>;
template class JFFTView<double>;

