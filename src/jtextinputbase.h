
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JTEXTINPUTBASE_H
#define JTEXTINPUTBASE_H

#include"jwidget.h"
#include"jutils.h"
#include<sstream>

/*
 * ------------------ JTextInputBase -------------------------
 * Simple text editor (no multiline, only one font available...)
 *
*/

class JTextInputBase : public JWidget
{
public:
    constexpr static const int LEFT_OFFSET = 5;

    JTextInputBase(float w = 200, float h = 20);
    JTextInputBase(float x, float y, float w, float h);
    ~JTextInputBase();

    void setup() override;
    void update() override;
    void draw() override;


    void setText(string txt);
    void appendText(string txt);
    void prependText(string txt);
    void insertText(int index, string txt);
    void insertTextAtCaret(string txt);
    void clear();


    string getText();

    int textSize();

    void setCaretPosition(int i);

    void selectText(int from, int to);
protected:
    string text;
    int caret_position;
    bool editing;

    virtual void keyCbk(ofKeyEventArgs &key);
    virtual void clickCbk(ofMouseEventArgs &mouse);
    virtual void releasedCbk(ofMouseEventArgs &mouse);
    virtual void draggedCbk(ofMouseEventArgs &mouse);

    JUtils::JChrono<int, chrono::seconds> chrono;
    JUtils::JChrono<int, chrono::milliseconds> dclick_chrono;

    ofColor background_color, text_color;

    pair<int, int> selected_area;
    bool area_selected;

    virtual int getDisplayable();
};

#endif // JTEXTINPUTBASE_H
