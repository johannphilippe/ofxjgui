
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jtoggle.h"
#include"jglobal.h"

JToggle::JToggle(float w, float h):
    JWidget(w, h), background_color(ofColor(JCOLOR_BLACK)),
    selected_solor(background_color)
{

}

JToggle::JToggle(float x, float y, float w, float h):
    JWidget(x, y, w, h), background_color(ofColor(JCOLOR_BLACK)),
    selected_solor(background_color)
{

}

JToggle::~JToggle()
{
    ofRemoveListener(ofEvents().mousePressed, this, &JToggle::clickedCbk);
}

void JToggle::setup()
{
    cout << "setup called " << endl;
    ofAddListener(ofEvents().mousePressed, this, &JToggle::clickedCbk);
}

void JToggle::draw()
{
    ofFill();
    ofSetColor( (state) ? selected_solor : background_color );
    ofDrawRectRounded(posx, posy, 0, width, height, 0);
    ofNoFill();
    ofSetColor(255);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectRounded(posx, posy, 0, width, height, 0);
    if(state) {
        ofDrawLine(posx, posy, posx + width, posy + height);
        ofDrawLine(posx, posy + height, posx + width, posy);
    }

}

void JToggle::update()
{
}

void JToggle::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    state = !state;
    sigStateChanged(state);
}

void JToggle::setBackgroundColor(const ofColor &color)
{
    background_color = color;
}

void JToggle::setSelectedColor(const ofColor &color)
{
    selected_solor = color;
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//---------------------------Multi toggle-------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

JMultiToggle::JMultiToggle(vector<ofColor> list, float w, float h):
    JWidget(w,h), color_list(list), state(0)
{

}

JMultiToggle::JMultiToggle(vector<ofColor> list, float  x, float y, float w, float h):
    JWidget(x, y, w, h), color_list(list), state(0)
{

}

JMultiToggle::~JMultiToggle(){
    ofRemoveListener(ofEvents().mousePressed, this, &JMultiToggle::clickedCbk);
}

void JMultiToggle::setup()
{
    ofAddListener(ofEvents().mousePressed, this, &JMultiToggle::clickedCbk);
}

void JMultiToggle::update()
{

}

void JMultiToggle::draw()
{
    ofFill();
    ofSetColor( color_list[state] );
    ofDrawRectRounded(posx, posy, 0, width, height, 0);
    ofNoFill();
    ofSetColor(255);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectRounded(posx, posy, 0, width, height, 0);
}

void JMultiToggle::clickedCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    if(state == color_list.size() - 1) state = 0;
    else state ++;
    sigStateChanged(state);
}
