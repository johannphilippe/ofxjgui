
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jsimplescope.h"

JSimpleScope::JSimpleScope() :
    JWidget(), grid_type(JGridType::None)
{
}

JSimpleScope::JSimpleScope(vector<double> *sig, float w, float h):
    JWidget(w, h), signal_data(sig), grid_type(JGridType::None)
{
}

JSimpleScope::JSimpleScope(vector<double> *sig, float x, float y, float w, float h):
    JWidget(x, y, w, h), signal_data(sig), grid_type(JGridType::None)
{
}

JSimpleScope::~JSimpleScope()
{
}

void JSimpleScope::setup(){}
void JSimpleScope::update() {}

void JSimpleScope::draw()
{
    ofNoFill();
    ofSetColor(JCOLOR_WHITE);

    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);

    ofDrawRectangle(posx, posy, 0, width, height);
    const float halfY = (float) posy + (height / 2);
    const float halfX = (float) posx + (width / 2);

    // draw sig
    ofSetColor(ofColor(JCOLOR_COOLRED));
    float x1, y1, y2;
    x1 = posx;
    const float x_grain = (float)width / signal_data->size();

    ofNoFill();
    for(size_t i = 1; i < signal_data->size(); i++)
    {
        y1 =  halfY - ((*signal_data)[i - 1] * (height / 2 ) );
        y2 =  halfY - ((*signal_data)[i] * (height / 2) );

        // draw segment of signal
        if(getCoord().isInBounds(x1, y1) && getCoord().isInBounds(x1 + x_grain, y2))
            ofDrawLine( glm::vec2(x1, y1), glm::vec2(x1 + x_grain, y2));

        x1 += x_grain;
    }

    drawGrid(halfX, halfY);
}

void JSimpleScope::drawGrid(const float halfX, const float halfY)
{
    if(grid_type == JGridType::None) return;
    // if grid is enabled, draw grid
    ofSetColor(JCOLOR_WHITE);
    if(grid_type == JGridType::Cross)
    {
        ofDrawLine(posx, halfY, posx + width, halfY);
        ofDrawLine( halfX, posy,  halfX, posy + height);
    } else if(grid_type == JGridType::Full)
    {
        ofDrawLine(posx, halfY, posx + width, halfY);
        ofDrawLine( halfX, posy,  halfX, posy + height);

        ofSetLineWidth(0.5);
        ofSetColor(JCOLOR_DARKGREY);

        const float ymult = height / 10;
        const float xmult = width / 10;
        for(int i = 1; i <= 5; i++)
        {
            // horizontal lines
            ofDrawLine(posx,  halfY + (ymult * i), posx + width, halfY +(ymult * i));
            ofDrawLine(posx,  halfY + -(ymult * i), posx + width, halfY + -(ymult * i));
            ofDrawLine(halfX + (xmult * i), posy, halfX + (xmult * i), posy + height);
            ofDrawLine(halfX + -(xmult * i), posy, halfX + -(xmult * i), posy + height);
        }
    }
}

void JSimpleScope::setGridtype(JGridType t)
{
    grid_type = t;
}
