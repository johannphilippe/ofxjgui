
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#include "jcontainer.h"


void JContainerBase::append(JWidget *wid)
{
    wid->setChildrenState(true);
    this->children.push_back(wid);
    if(orientation == JOrientation::Vertical)
    {
        // adapt width to max width in children
        if(auto_resize) {
            if(wid->getCoord().w > width) width = wid->getCoord().w;
            height = children_height;
        }
        wid->setPosition(posx, children_height);
        children_height += wid->height;
    } else {
        // adapt height to max height in children
        if(auto_resize) {
            if(wid->getCoord().h > height) height = wid->getCoord().h;
            width += children_width;
        }
        wid->setPosition(children_width, posy);
        children_width += wid->width;
    }

    wid->sigResized.connect([&](float, float) {
       updateChildrenPos();
    });
}

void JContainerBase::swap(JWidget *first, JWidget *second)
{
    int found1 = -1;
    int found2 = -1;
    for(size_t i= 0; i < children.size(); i++) {
        if(children[i] == first) found1 = i;
        else if(children[i] == second) found2 = i;
        if(found1 != -1 && found2 != -1)
        {
            std::swap(children[found1], children[found2]);
            updateChildrenPos();
        }
    }
}

void JContainerBase::swap(size_t i1, size_t i2)
{
    if(i1 < children.size() && i2 < children.size()) {
        std::swap(children[i1], children[i2]);
        updateChildrenPos();
    }
}

void JContainerBase::setGap(float gap)
{
    this->gap = gap;
    updateChildrenPos();
}

void JContainerBase::setupChildren()
{
    for(auto & it : children)
    {
        it->setup();
    }

    this->sigPositionChanged.connect([&](float ,float )
    {
       updateChildrenPos();
    });

    this->sigResized.connect([&](float , float) {
       updateChildrenPos();
    });

    updateChildrenPos();

}

void JContainerBase::updateChildren()
{
    float mouseX = ofGetMouseX();
    float mouseY = ofGetMouseY();

    for(auto & it : children) {
        J2DCoord pos = it->getCoord();
        if(mouseX >= pos.x - it->mouse_offsetX
                && mouseX <= (pos.x + pos.w) - it->mouse_offsetX &&
                mouseY >= pos.y - it->mouse_offsetY
                && mouseY <= (pos.y + pos.h - it->mouse_offsetY)) {
            it->setSelected(true);
        } else
            it->setSelected(false);
        it->update();
    }
}

void JContainerBase::updateChildrenPos( )
{
    float passed;
    switch(orientation)
    {
    case JOrientation::Vertical:
    {
        passed = posy;
        if(alignment == JAlignment::Center) {
            // error here, not exactly centered
            passed += ((height / 2) - (children_height / 2)   - ((gap * children.size())  / 2 ) );
        }

        for(auto & it : children)
        {
            it->setPosition(posx, passed);
            passed += (it->getCoord().h + gap);
            if(it->width > this->width) this->setWidth(it->width);
        }
        break;
    }

    case JOrientation::Horizontal:
    {
        passed = posx;
        if(alignment == JAlignment::Center) {
            passed += ((width / 2) - (children_width / 2) - (gap * children.size() / 2) );
        }

        for(auto & it : children)
        {
            it->setPosition(passed, posy);
            passed += it->getCoord().w + gap;
            if(it->height > this->height) this->setHeight(it->height);
        }
        break;
    }
    }

}

void JContainerBase::drawChildren()
{
    if(orientation == JOrientation::Vertical) {
        for(auto & it : children)
        {
            it->draw();
        }
    } else {
        for(auto & it : children)
        {
            it->draw();
        }
    }
}

void JContainerBase::drawChildren(float x, float y)
{
    if(orientation == JOrientation::Vertical) {
        for(auto & it : children)
        {
            it->draw();
        }
    } else {
        for(auto & it : children)
        {
            it->draw();
        }
    }
}






/*
 *
 * --------------- JCONTAINER --------------
 * Layout system (box containing children, and aligning them)
 *
*/

JContainer::JContainer() :
    orientation(JOrientation::Vertical)
{
}

JContainer::~JContainer()
{

}

void JContainer::append(JWidget *wid)
{
    wid->setChildrenState(true);
    this->children.push_back(wid);
    if(orientation == JOrientation::Vertical)
    {
        wid->setPosition(posx, children_height);
        children_height += wid->height + gap;
        // adapt width to max width in children
        if(auto_resize) {
            if(wid->getCoord().w > width ) width = wid->getCoord().w;
            height = children_height;
            sigResized(width, height);
        }
    } else { // Horizontal
        wid->setPosition(children_width, posy);
        children_width += wid->width + gap;
        if(handle_name.compare(string("cont")) == 0)
            cout << "gap " << " " << gap << endl;
        // adapt height to max height in children
        if(auto_resize) {
            if(wid->getCoord().h > height) height = wid->getCoord().h;
            //height = wid->getCoord().h;
            width = children_width;
            sigResized(width, height);
        }
    }

    wid->sigResized.connect([&](float, float) {
       updateChildrenPos();
    });

    if(wid->handle_name.compare(string("multiline")) == 0) cout << "CONT1 >> APPEND > width chwidth : " << width << "  " << children_width <<  " " << wid->width << endl;
}

void JContainer::updateChildrenPos()
{
    float passed;
    float sz = 0.0f;
    switch(orientation)
    {
    case JOrientation::Vertical:
    {
        passed = posy;
        if(alignment == JAlignment::Center) {
            // error here, not exactly centered
            passed += ((height / 2) - (children_height / 2));
        }

        for(auto & it : children)
        {
            it->setPosition(posx, passed);
            passed += (it->getCoord().h + gap);
            sz += (it->getCoord().h + gap);
            //if(it->width > this->width) this->setWidth(it->width);
            //if(it->width > this->children_width) this->children_width = it->width;
        }
        //height = passed;
        children_height = sz;
        //setHeight(passed);
        break;
    }

    case JOrientation::Horizontal:
    {
        passed = posx;

        if(alignment == JAlignment::Center) {
            if(handle_name.compare(string("cont")) == 0) cout << "width chwidth : " << width << "  " << children_width << endl;
            passed += ((width / 2) - (children_width / 2));
        }

        for(auto & it : children)
        {
            it->setPosition(passed, posy);
            passed += it->getCoord().w + gap;
            sz += (it->getCoord().w + gap);
            //if(it->height > this->height) this->setHeight(it->height);
            //if(it->height > this->children_height) this->children_height = it->height;
        }
        //width = passed;
        children_width = sz;
        //setWidth(passed);
        break;
    }
    }
        if(handle_name.compare(string("cont1")) == 0) cout << "CONT1 width chwidth : " << width << "  " << children_width << endl;
}

void JContainer::setup()
{
    for(auto & it : children)
    {
        it->setup();
    }

    this->sigPositionChanged.connect([&](float ,float )
    {
       updateChildrenPos();
    });

    this->sigResized.connect([&](float , float) {
       updateChildrenPos();
    });

    updateChildrenPos();

}

void JContainer::update()
{
    float mouseX = ofGetMouseX();
    float mouseY = ofGetMouseY();

    for(auto & it : children) {
            const J2DCoord pos = it->getCoord();
            if(mouseX >= pos.x && mouseX <= (pos.x + pos.w) &&
                mouseY >= pos.y && mouseY <= (pos.y + pos.h))
                it->setSelected(true);
            else
                it->setSelected(false);
        it->update();
    }
}

void JContainer::draw()
{
    if(orientation == JOrientation::Vertical) {
        for(auto & it : children)
        {
            it->draw();
        }
    } else {
        for(auto & it : children)
        {
            it->draw();
        }
    }
}

void JContainer::swap(JWidget *first, JWidget *second)
{
    int found1 = -1;
    int found2 = -1;
    for(size_t i= 0; i < children.size(); i++) {
        if(children[i] == first) found1 = i;
        else if(children[i] == second) found2 = i;
        if(found1 != -1 && found2 != -1)
        {
            std::swap(children[found1], children[found2]);
            updateChildrenPos();
        }
    }
}

void JContainer::swap(size_t i1, size_t i2)
{
    if(i1 < children.size() && i2 < children.size()) {
        std::swap(children[i1], children[i2]);
        updateChildrenPos();
    }
}

void JContainer::setGap(float g)
{
    gap = g;
    updateChildrenPos();
}

void JContainer::setAlignment(JAlignment a)
{
    alignment = a;
    if(alignment == JAlignment::Center) {
        auto_resize = false;
    }
    updateChildrenPos();
}

