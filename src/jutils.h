
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/ 


#ifndef JUTILS_H
#define JUTILS_H

#include<iostream>
#include<sstream>
#include<fstream>
#include<chrono>
#include<math.h>
#include"ofMain.h"

namespace JUtils {
using namespace std;

/*
 *
 * Math Utilities
 *
 *
*/
namespace Math
{

template<typename T>
inline T limit(T val, T min, T max)
{
    if(val > max) return max;
    if(val < min) return min;
    return val;
}

// Linear or exp interpolation
inline double getCurve(double beg, double ending, int dur, int idx, double typ)
{
    double res;
    double type;
    if(typ == 0) {
        res = beg + (idx * (( ending - beg) / dur));
    }else {
        if(typ > 10) type = 10;
        else if(typ <  -10) type = -10;
        else type = typ;
        res = beg + (ending - beg) * (1 - exp(idx * type / (dur - 1))) / (1-exp(type));
    }
    return res;
}

template<typename T>
inline T ScaledLog(T val, T min, T max) {
    return ((log(val) - log(min)) / (log(max) - log(min))) * max;
}


inline double Hanning(int index, int length) {
    return  0.5 * (1 - cos(2 * M_PI * index / (length - 1 )));
}

inline double Hamming(int index, int length) {
    return 0.54 - (0.46 * cos(2 * M_PI * index / (length - 1)));
}

inline double Blackman(int index, int length) {
    return 0.42 - (0.5 * cos(2 * M_PI * index / (length - 1))) + (0.08 * cos(4 * M_PI * index / (length - 1)));
}

inline double BarkBand(int freq)
{
    return (13 * atan(0.00076 * double(freq) )) + (3.5 * atan(pow( double(freq) / 7500, 2)));
}


// Be careful, it is cutoff frequencies. The band 1, ends in cutoff 2. So the index 1 corresponds to start index 0, and width 1.
static constexpr int BarkBandsCutoffFrequencies[] = {
    20, 100,200,300,400,510,630, 770, 920, 1080, 1270, 1480, 1720, 2000, 2320, 2700, 3150, 3700, 4400, 5300, 6400, 7700, 9500, 12000, 15500
};

static constexpr int BarkBandWidths[] = {
  -1, 80, 100, 100, 100, 110, 120, 140, 150, 160, 190, 210, 240, 280, 320, 380, 450, 550, 700, 900, 1100, 1300, 1800, 2500, 3500
};


static int BarkFrequency(double band)
{
    int band_nbr = int(floor(band));
    if(band_nbr == 0) {
        std::cout << "Error, bark band cannot be 0" << std::endl;
        return -1;
    }
    int begin = BarkBandsCutoffFrequencies[band_nbr - 1];
    int width = BarkBandWidths[band_nbr];
    double decimal = band - band_nbr;
    double freq = begin + (decimal * double(width));

    return freq;
}

}

/*
 *
 * String Utils
 *
 *
*/
namespace String {

template<typename T>
static string getString(T val, int precision = 5)
{
    ostringstream ss;
    ss.precision(precision);
    ss << val;
    return ss.str();
}

static string toVerticalString(const std::string &str)
{
    string vert;
    for(size_t i = 0; i < str.size(); i++) {
        vert.push_back(str[i]);
        vert.push_back(char(*"\n"));
    }
    return vert;

}
}


namespace System
{

static string getClipboard()
{
    return ofGetWindowPtr()->getClipboardString();
}

static void toClipboard(string to)
{
    return ofGetWindowPtr()->setClipboardString(to);
}

}

namespace Key {

#define OF_KEY_SPACE 32

constexpr static const int CTRL_C = 611;
constexpr static const int CTRL_X = 632;
constexpr static const int CTRL_A = 609;
constexpr static const int CTRL_D = 612;
constexpr static const int CTRL_V = 630;

constexpr static const int CTRL_N = 622;
constexpr static const int CTRL_P = 624;
constexpr static const int CTRL_Z = 634;

static int getModifier(int key, int modifier)
{
    int v = modifier;
    v = v << 8;
    v += key;
    cout << "modifier : " << v << endl ;
    return v;
}


static bool isPrintable(int key)
{
    if(key >= 32 && key <= 126) return true;
    return false;
}

static string toString(int key) {
    ostringstream ss;
    ss.precision(5);
    ss << char(key);
    return ss.str();
}

}


namespace JColor {
static ofColor getRandomColor(bool alpha = false)
{
    const int r = rand() % 255;
    const int g = rand() % 255;
    const int b = rand() % 255;
    if(alpha)
        return ofColor(r, g, b, (rand() % 255) );
    return ofColor(r, g, b);
}

}

/*
 *  My wonderful simple chrono
 *
*/
template<typename T, typename D>
class JChrono {

public:
    JChrono() {

    }

    void start() {
        begin = chrono::high_resolution_clock::now();
        trigger();
    }

    T timeSinceLastTrigger() {
        chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
        T dur = chrono::duration_cast<D>(t2 - t).count();
        trigger();
        return dur;
    }

    T timeSinceStart() {
        chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
        T dur = chrono::duration_cast<D>(t2 - begin).count();
        return dur;
    }

    void trigger() {
        t = chrono::high_resolution_clock::now();
    }

private:
    chrono::high_resolution_clock::time_point t, begin;

};
}

#endif // JUTILS_H
