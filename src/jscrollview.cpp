
/*
The MIT License (MIT)
Copyright © 2020, Johann Philippe
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "jscrollview.h"

JScrollView::~JScrollView() {
    ofRemoveListener(ofEvents().mouseScrolled, this, &JScrollView::scrolledCbk);
}


void JScrollView::setup() {
    ofAddListener(ofEvents().mouseScrolled, this, &JScrollView::scrolledCbk);

    fbo.allocate(width, height, GL_RGBA);

    setupChildren();

    cam.removeAllInteractions();
    //cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY,OF_MOUSE_BUTTON_LEFT);
    cam.disableMouseMiddleButton();
    cam.disableMouseInput();
    //cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT);

    cam.enableOrtho();
    cam.setNearClip(-1000000);
    cam.setFarClip(1000000);
    cam.setVFlip(true);
    if(orientation == JOrientation::Vertical)
        cam.setPosition(width / 2, (height/2) - (scroll_idx * scroll_mult), 0);
    else
        cam.setPosition(width / 2 + (scroll_idx * scroll_mult), height / 2, 0);

}

void JScrollView::update()
{
    // replace following line with sigResized or sigPositionChanged (avoid doing it all the time, so quicker)
    //updateChildren();

    const float mouseX = ofGetMouseX();
    const float mouseY = ofGetMouseY();
    // implement a mask system to se what area of the widget is selected

    if(!is_selected) {
        for (auto & it : children) it->setSelected(false);
        return;
    }

    if(mask.isInBounds(mouseX, mouseY)) {
        if(orientation == JOrientation::Vertical)
        {
            for(auto & it : children) {
                // two things to find. If element is visible (first).
                // Then if element is selected, second.

                const float scroll = scroll_idx * scroll_mult;
                const float child_y = posy + it->posy + (scroll);
                if( (scroll) >= -(child_y + it->height) && (scroll) <= 0) {
                    // element is visible
                    if(mouseY >= child_y && mouseY < child_y + it->height
                            && mouseX > posx + it->posx && mouseX < (posx + it->posx + it->width)) {
                        // element is under mouse
                        it->setSelected(true);
                        it->mouse_offsetX = posx;
                        it->mouse_offsetY = posy + (scroll_idx * scroll_mult);
                    } else it->setSelected(false);
                } else {
                    it->setSelected(false);
                }
            }
        } else // Horizontal
        {

            for(auto & it : children) {
               const float scroll = scroll_idx * scroll_mult;
               const float child_x = posx + it->posx - scroll ;
               if ( scroll >= 0 && scroll <= (child_x + it->width) ) {
               // is visible
                   if( (mouseX >= child_x) && (mouseX< child_x + it->width)
                           && (mouseY >= posy + it->posy) && (mouseY < posy + it->posy + it->height) ) {
                       it->setSelected(true);
                       it->mouse_offsetX = posx - scroll;
                       it->mouse_offsetY = posy + (it->posy);
                   } else {
                       it->setSelected(false);
                   }

               } else {
                   it->setSelected(false);
               }
            }

        }
    }

}

void JScrollView::draw() {
    ofNoFill();
    ofSetColor(ofColor::white);
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofDrawRectangle(posx, posy, width, height);
    fbo.begin();
    ofClear(0);
        cam.begin();
            drawChildren(0, 0);
            ofFill();
        cam.end();
    fbo.end();

    fbo.draw(posx, posy, width, height);
}

void JScrollView::setOrientation(JOrientation o)
{
    orientation = o;
}

void JScrollView::updateCameraPosition()
{
    if(orientation == JOrientation::Vertical)
        cam.setPosition(width / 2, (height/2) - (scroll_idx * scroll_mult), 0);
    else
        cam.setPosition(width / 2 + (scroll_idx * scroll_mult), height / 2, 0);
}

void JScrollView::scrollToIndex(float index, bool sync)
{
    index = JUtils::Math::limit<float>(index, 0.0f, 1.0f);
    if(orientation == JOrientation::Vertical)
    {
        scroll_idx =  ( (children_height - height) / scroll_mult ) * -index;
    } else // Horizontal
    {
        scroll_idx = (index * (children_width - width)) / scroll_mult;
    }
    updateCameraPosition();
    //if(sync && scrollbar != nullptr) scrollbar->setScrollPosition(index, false);
    syncScrollbar();
    cout << "scroll index : " << scroll_idx << endl;
}

void JScrollView::scrollToChild(JWidget *child, bool sync)
{
    float passed_x = 0, passed_y = 0;
    bool found = false;
    size_t child_idx = 0;
    for(; child_idx < children.size(); child_idx++) {
        if(children[child_idx] == child) {
            found = true;
            break;
        }
        passed_x += children[child_idx]->width + gap;
        passed_y += children[child_idx]->height + gap;
    }
    if(!found) return;

    if(orientation == JOrientation::Vertical)
    {
        scroll_idx = -(passed_y / scroll_mult);
        // child pos

    } else // Horizontal
    {
        scroll_idx = passed_x / scroll_mult;
    }

    updateCameraPosition();
    syncScrollbar();
}

void JScrollView::syncScrollbar()
{
    if(scrollbar == nullptr) return;
    float index;
    if(orientation == JOrientation::Vertical)
    {
        index = ( (scroll_mult * scroll_idx) / (children_height - height) ) + 1;

    } else // Horizontal
    {
        index = (scroll_idx * scroll_mult) / (children_width - width);
    }
    cout << "index : " << index << endl;
    scrollbar->setScrollPosition(index, false);
}

void JScrollView::scrolledCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    if(orientation == JOrientation::Vertical) {
        scroll_idx += mouse.scrollY;
        if( scroll_idx  > 0) scroll_idx = 0;
        else if( ((scroll_idx * scroll_mult) - height) < (-children_height) )
            scroll_idx = ( ( -children_height + height) / scroll_mult); //scroll_idx = -children_height;
    } else // horizontal
    {
        scroll_idx += mouse.scrollX;
        if(scroll_idx < 0) scroll_idx = 0;
        else if( ( (scroll_idx * scroll_mult) + width) > (children_width) )
            scroll_idx = (children_width - width) / scroll_mult;
    }
    updateCameraPosition();
    syncScrollbar();
}

void JScrollView::setScrollBar(JScrollBar *bar)
{
    scrollbar = bar;
    scrollbar->setScrollView(this);
    // sync value
    syncScrollbar();
}

void JScrollView::updateChildrenPos()
{

}


/*
 *
 *  SCROLLBAR
 *
 *
*/


JScrollBar::JScrollBar(JOrientation o, float w, float h):
    JWidget(w, h), orientation(o), view(nullptr)
{
}

JScrollBar::JScrollBar(JOrientation o, float x, float y, float w, float h):
    JWidget(x, y, w, h), orientation(o), view(nullptr)
{
}

JScrollBar::~JScrollBar() {
    ofRemoveListener(ofEvents().mousePressed, this, &JScrollBar::clickedCbk);
    ofRemoveListener(ofEvents().mouseDragged, this, &JScrollBar::draggedCbk);
    ofRemoveListener(ofEvents().mouseReleased, this, &JScrollBar::releasedCbk);
    ofRemoveListener(ofEvents().mouseScrolled, this, &JScrollBar::scrolledCbk);
}

void JScrollBar::setup()
{
    ofAddListener(ofEvents().mousePressed, this, &JScrollBar::clickedCbk);
    ofAddListener(ofEvents().mouseDragged, this, &JScrollBar::draggedCbk);
    ofAddListener(ofEvents().mouseReleased, this, &JScrollBar::releasedCbk);
    ofAddListener(ofEvents().mouseScrolled, this, &JScrollBar::scrolledCbk);
}

void JScrollBar::update()
{

}

void JScrollBar::draw()
{
    ofNoFill();
    ofSetLineWidth(JGlobal::DEFAULT_LINE_WIDTH);
    ofSetColor(ofColor::white);
    ofDrawRectangle(posx, posy, width, height);
    // then draw the square

    ofSetColor(ofColor::lightSlateGray);
    ofFill();

    if(orientation == JOrientation::Vertical)
    {
        const float py = (posy + (height  - width) ) - (value * (height - width) );
        ofDrawRectangle(posx, py, width, width);

    } else // Horizontal
    {
        const float px = (posx) + (value * (width - height) );
        ofDrawRectangle(px, posy, height, height);
    }
}

void JScrollBar::setScrollView(JScrollView *view)
{
    this->view = view;
}

void JScrollBar::setScrollPosition(float index, bool sync)
{
    value = index;
    if(sync && view != nullptr) {
        if(orientation == JOrientation::Vertical) {
            view->scrollToIndex(1.0f - value);
        } else // Horizontal
        {
            view->scrollToIndex(value);
        }
    }
}

void JScrollBar::clickedCbk(ofMouseEventArgs & mouse)
{
    if(!is_selected) return;
    clicked = true;
    updateValue();
}

void JScrollBar::draggedCbk(ofMouseEventArgs &mouse)
{
     if(clicked) updateValue();
}

void JScrollBar::releasedCbk(ofMouseEventArgs &mouse)
{
    clicked = false;
}

void JScrollBar::updateValue()
{
    float diff;
    if(orientation == JOrientation::Vertical)
    {
        diff =  (height + posy) - getMouseY();
        value = diff / height;
        value = JUtils::Math::limit<double>(value, 0.0, 1.0);

        if(view != nullptr) {
            view->scrollToIndex(1.0f - value, false);
        }
    } else
    {
        diff =  getMouseX() - posx;
        value = diff / width;
        value = JUtils::Math::limit<double>(value, 0.0, 1.0);

        if(view != nullptr) {
            view->scrollToIndex(value, false);
        }
    }

}

void JScrollBar::scrolledCbk(ofMouseEventArgs &mouse)
{
    if(!is_selected) return;
    value = JUtils::Math::limit<float>(value + (mouse.scrollY / 100.0), 0.0, 1.0);

    if(view != nullptr) {
        if(orientation == JOrientation::Vertical) {
            view->scrollToIndex(1.0 - value, false);
        } else // Horizontal
        {
            view->scrollToIndex(value, false);
        }
    }
}

