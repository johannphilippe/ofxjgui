#require "mini_magick"
require "rubygems"
require "rmagick"

puts "Compiling assets to binary embeddable header"

MAX_SIZE = 128

def getImageString(path)

  

 # img = MiniMagick::Image.open(path)
  img = Magick::Image.read(path).first
  puts "alpha : #{img.alpha?}"

  puts img.columns, img.rows
  if(img.columns > MAX_SIZE or img.rows > MAX_SIZE) then
    img = img.resize_to_fit(MAX_SIZE, MAX_SIZE)
  end
  puts img.columns, img.rows
  

  filename = File.basename(path, ".*")

  dim_str = "static const int #{filename}_width = " + img.columns.to_s + ";\n"
  dim_str += "static const int #{filename}_height = " + img.rows.to_s + ";\n"
  if(img.alpha?) then
    dim_str += "static const ofImageType #{filename}_type = OF_IMAGE_COLOR_ALPHA; \n"
  else
    dim_str += "static const ofImageType #{filename}_type = OF_IMAGE_COLOR; \n"
  end
  

  data_str = "static unsigned char #{filename}_data[] = {"


  img.each_pixel do |pix, c, r|
    data_str += (pix.red >> 8).to_s + ", " 
    data_str += (pix.green >> 8).to_s + ", " 
    data_str += (pix.blue >> 8).to_s + ", " 
    if(img.alpha?) then
      data_str += (pix.alpha >> 8).to_s + ", " 
    end

  end
  data_str = data_str[0...-2]
  data_str += "}; \n"
  str = dim_str + "\n" + data_str + "\n"

  str += "const JIconData IconData_#{filename}(#{filename}_data, #{filename}_width, #{filename}_height, #{filename}_type); \n"
  return str
end


  
fw = File.open("#{Dir.pwd}/src/resources/core.h", "w")
fw.write("#ifndef IMG_CORE_RESOURCE \n#define IMG_CORE_RESOURCE \n#include \"ofMain.h\" \n#include\"jtypes.h\" \n")
Dir.entries("#{Dir.pwd}/assets/core").select { |f|
  puts f
  if(File.file? File.join("#{Dir.pwd}/assets/core", f) ) then
    puts File.join("#{Dir.pwd}/assets/core", f)
    data_str = getImageString( File.join("#{Dir.pwd}/assets/core", f))
    fw.write(data_str)
  end
}
  
fw.write("#endif // IMG_CORE_RESOURCE")
fw.close


fw = File.open("#{Dir.pwd}/src/resources/user.h", "w")  
fw.write("#ifndef IMG_USER_RESOURCE \n#define IMG_USER_RESOURCE \n#include\"ofMain.h\" \n#include\"jtypes.h\" \n")
Dir.entries("#{Dir.pwd}/assets/user").select { |f|
  if(File.file? File.join("#{Dir.pwd}/assets/user", f)) then
    puts File.join("#{Dir.pwd}/assets/user", f)
    fw.write(getImageString(File.join("#{Dir.pwd}/assets/user", f)))
  end
}
fw.write("#endif // IMG_USER_RESOURCE")
fw.close
