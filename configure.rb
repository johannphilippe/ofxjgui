#!/usr/bin/ruby

puts "----------------------------------------------------------------------------"
puts "----------------------------------------------------------------------------"
puts "--------------------------------JGUI----------------------------------------"
puts "----------------------------------------------------------------------------"
puts "----------------------------------------------------------------------------"

print("Enter openframeworks source path :")
path = gets 
path = path.chomp
count = 0
rp = false
if path =~ /../ then
  rp = true
  rpath = Dir.pwd
end

while path =~ /../ do
  path, rest = File.split(path)
  rpath, rest = File.split(rpath)
  count += 1
end

if(rp == true) then
  path = rpath
end

pwd = Dir.pwd
root_path = pwd

config_file = File.open("paths.rb", "w")

str = "OF_PATH = \"#{path}\"\n"
str += "JGUI_PATH = \"#{root_path}\"\n"
config_file.write str
config_file.close


