#include "ofMain.h"
#include"jgui.h"
#include"Dependencies/nlohmann/json.hpp"

using json = nlohmann::json;
//========================================================================
string toGen8(vector<glm::vec3> const &data, int index, int domain)
{
    string res = "f " + std::to_string(index) + " 0 " + std::to_string(domain) + " -8 ";
    int seen = 0;
    for(size_t i = 0; i < data.size(); i++) {
        res += JUtils::String::getString<double>(data[i].y) + " ";
        if(i < data.size() - 1) {
            const double time = data[i + 1].x * domain;
            res += JUtils::String::getString<double>(floor(time - seen)) + " ";
            seen += (time - seen);
        }
    }
    return res;

}

string toGen16(vector<glm::vec3> const &data, int index, int domain)
{
    string res = "f " + std::to_string(index) + " 0 " + std::to_string(domain) + " -16 ";
    int seen = 0;
    for(size_t i = 0; i < data.size(); i++)
    {
        double crv = data[i].z;
        res += JUtils::String::getString<double>(data[i].y) + " ";
        if(i < data.size() - 1)
        {
            if(data[i+1].y > data[i].y) crv *= -1;
            const double time = data[i + 1].x * domain;
            res += JUtils::String::getString<double>(floor(time - seen)) + " " + JUtils::String::getString<double>(crv) + " ";
            seen += (time - seen);
        }
    }
    return res;
}

string toGenBezier(vector<glm::vec3> const &data, int index, int domain)
{
    string res = "f " + std::to_string(index) + " 0 " + std::to_string(domain) + " \"quadbezier\" ";
    for(size_t i = 0; i < data.size(); i++)
    {
        if(i < data.size() - 1) {
            const double x = data[i].x * domain;
            res += JUtils::String::getString<double>(x) + " ";
        }
        res += JUtils::String::getString<double>(data[i].y) + " " ;
    }

    return res;
}

string getGen(vector<glm::vec3> const &data, int index, int domain, JCurveMode mode)
{
    switch (mode) {
    case JCurveMode::LinearLogExp:
        return toGen16(data, index, domain);
        break;
    case JCurveMode::Bezier:
        return toGenBezier(data, index, domain);
        break;
    case JCurveMode::Spline:
        return toGen8(data, index, domain);
        break;
    }
}

int main( ){

    const int domain = 2048;

    vector<vector< glm::vec3> > data;
    vector<JCurveMode> modes;
    modes.resize(999);
    data.resize(999);

    const float w_width = 1920, w_height = 1080;

    JCurveEditor editor(1800, 800);
    editor.setGridType(JGridType::Full);
    for(int i = 0; i < 10; i++)
    {
        editor.samples_positions.push_back(glm::vec3((float)i/ 10.0, (float)i / 10.0, 0));
    }

    JButton linear("Linear/log/exp", 150, 40),
            bezier("Bezier", 150, 40),
            spline("Spline", 150, 40),
            clear("Clear", 150, 40),
            store("Store", 150, 40),
            copy("ToClipboard", 150, 40);

    linear.setColor(ofColor::green);
    bezier.setColor(ofColor::darkGrey);
    spline.setColor(ofColor::darkGrey);

    JSpin<int> func_nbr(1, 1, 999, 1);

    JTextInput text(ofTrueTypeFontSettings("verdana.ttf", 12), JMultilineMode::Disabled, 1800);

    JContainer mode_container(JOrientation::Horizontal, &linear, &bezier, &spline);
    mode_container.setGap(5);
    mode_container.setAlignment(JAlignment::Center);
    mode_container.width = 1920;

    JContainer b_container(JOrientation::Horizontal, &func_nbr, &clear, &store, &copy);
    b_container.setAlignment(JAlignment::Center);
    b_container.width = 1920;
    b_container.setGap(5);

    JContainer text_container(JOrientation::Horizontal, &text);
    text_container.setAlignment(JAlignment::Center);
    text_container.width = 1920;

    JContainer editor_container(JOrientation::Horizontal, &editor);
    editor_container.setAlignment(JAlignment::Center);
    editor_container.width = 1920;


    JContainer main_container(JOrientation::Vertical, &editor_container , &mode_container, &b_container, &text_container );
    main_container.setGap(10);
    main_container.posy = 20;

    JDialogSettings settings("Curve editor", OF_WINDOW, glm::vec2(w_width, w_height), glm::vec2(0, 0));
    JDialog dial(settings, &main_container, true);

    linear.sigButtonPressed.connect([&](){
       linear.setColor(ofColor::green);
       bezier.setColor(ofColor::darkGrey);
       spline.setColor(ofColor::darkGrey);
       editor.setMode(JCurveMode::LinearLogExp);
    });
    bezier.sigButtonPressed.connect([&](){
       bezier.setColor(ofColor::green);
       linear.setColor(ofColor::darkGrey);
       spline.setColor(ofColor::darkGrey);
       editor.setMode(JCurveMode::Bezier);
    });
    spline.sigButtonPressed.connect([&](){
       spline.setColor(ofColor::green);
       bezier.setColor(ofColor::darkGrey);
       linear.setColor(ofColor::darkGrey);
       editor.setMode(JCurveMode::Spline);
    });

    clear.sigButtonPressed.connect(&editor, &JCurveEditor::clear);

    store.sigButtonPressed.connect([&](){
        data[func_nbr.get() - 1] = editor.samples_positions;
        modes[func_nbr.get() - 1] = editor.curve_mode;
        string txt = getGen(data[func_nbr.get() - 1], func_nbr.get(), domain, modes[func_nbr.get() - 1]);
        text.setText(txt);
    });

    copy.sigButtonPressed.connect([&](){
       JUtils::System::toClipboard(text.getText());
    });

    func_nbr.sigValueChanged.connect([&]( int value) {
       if(data[value - 1].empty()) {
           editor.clear();
           linear.click();
           text.clear();
       } else {
           editor.samples_positions = data[value - 1];
           switch(modes[value - 1]) {
           case JCurveMode::LinearLogExp:
               linear.click();
               break;
           case JCurveMode::Bezier:
               bezier.click();
               break;
           case JCurveMode::Spline:
               spline.click();
               break;
           }

           editor.processCurve();
           string txt = getGen(data[value - 1], func_nbr.get(), domain, modes[value - 1]);
           text.setText(txt);
       }
       return;
    });

    ofRunMainLoop();
}
