#!/usr/bin/ruby

require "bundler/inline"
require "rubygems/package"
require_relative "../paths.rb"
gemfile do
    gem "fileutils", require: true
end


OF_PATH_TAG = /###OF_PATH###/

OF_FILES_TAG = /###OF_FILES###/


puts "-------------------------------------------------------------------------------------"
puts "-------------------------------------------------------------------------------------"
puts "-------------------------ofxJGUI Project Generator-----------------------------------"
puts "-------------------------------------------------------------------------------------"
puts "-------------------------------------------------------------------------------------"

print "Please enter project name : "
project_name = gets
project_name = project_name.downcase.strip
project_name = project_name.chomp
project_dir_path = Dir.pwd + "/" + project_name
project_dir_path.chomp
FileUtils.mkdir_p project_dir_path

my_of_path = OF_PATH


def desired_path(path)
        if(path == "ofApp.cpp") then return false end
        if(path == "ofApp.h") then return false end
        if(path == "main.cpp") then return false end
        if(path == ".") then return false end
        if(path == "..") then return false end
        return true
end


filestr = File.read(Dir.pwd + "/template/ofxJGUI.qbs")

project_file = ""


File.open(Dir.pwd + "/template/ofxJGUI.qbs").each_line do |line|
    if line =~ OF_PATH_TAG then
        lstr = my_of_path + "/libs/openFrameworksCompiled/project/qtcreator/ofApp.qbs"
        #line["###OF_PATH###"] = lstr
        line.sub! "###OF_PATH###" , lstr
        project_file += line
    else
        project_file += line
    end
end

File.write(project_dir_path + "/#{project_name}.qbs", project_file)
srcpath = project_dir_path + "/src"
Dir.mkdir(srcpath)

template_src = Dir.pwd + "/template/src"

ofapp_h = srcpath + "/ofApp.h"
ofapp_cpp = srcpath + "/ofApp.cpp"
main_cpp = srcpath + "/main.cpp"
File.write(ofapp_h,  File.read(template_src + "/ofApp.h") )
File.write(ofapp_cpp,  File.read(template_src + "/ofApp.cpp") )
File.write(main_cpp, File.read(template_src + "/main.cpp") )

