import qbs
import qbs.Process
import qbs.File
import qbs.FileInfo
import qbs.TextFile
import "../../../../libs/openFrameworksCompiled/project/qtcreator/ofApp.qbs" as ofApp

Project{
    property string of_root: "../../../.."

    ofApp {
        name: { return FileInfo.baseName(sourceDirectory) }

        files: [
            'src/main.cpp',
            "../../src/Dependencies/lsignal/lsignal.h",
            "../../src/Dependencies/nlohmann/json.hpp",
            "../../src/Dependencies/AudioFFT/AudioFFT.h",
            "../../src/Dependencies/AudioFFT/AudioFFT.cpp",
            "../../src/Dependencies/spline/spline.h",
            "../../src/Dependencies/ofxPixelsShape/src/ofxPixelsShape.h",
            "../../src/Dependencies/libInterpolate/src/libInterpolate/Interpolate.hpp",
            "../../src/resources/core.h",
            "../../src/resources/user.h",
            "../../src/jcontainer.cpp",
            "../../src/jtextinput.cpp",
            "../../src/jglobal.h",
            "../../src/jmultiscope.h",
            "../../src/jdebug.h",
            "../../src/janimationbase.h",
            "../../src/jsimplescope.cpp",
            "../../src/janimationbase.cpp",
            "../../src/jcontainer.h",
            "../../src/jcurveeditor.h",
            "../../src/jbutton.cpp",
            "../../src/janimations.h",
            "../../src/jslider.cpp",
            "../../src/jtextinputbase.cpp",
            "../../src/jmultiscope.cpp",
            "../../src/jcurveeditor.cpp",
            "../../src/jspin.h",
            "../../src/jslider.h",
            "../../src/jspin.cpp",
            "../../src/jcore.h",
            "../../src/jtoastbase.h",
            "../../src/jtextinput.h",
            "../../src/jfill.h",
            "../../src/jbutton.h",
            "../../src/jtextinputbase.h",
            "../../src/jvalue.h",
            "../../src/jutils.h",
            "../../src/jgui.h",
            "../../src/jcore.cpp",
            "../../src/jtypes.h",
            "../../src/jwidget.cpp",
            "../../src/jwidget.h",
            "../../src/jplot.h",
            "../../src/jplot.cpp",
            "../../src/jsimplescope.h",
            "../../src/jtoggle.h",
            "../../src/jtoggle.cpp",
            "../../src/jdialog.h",
            "../../src/jdialog.cpp",
            "../../src/jknob.cpp",
            "../../src/jknob.h",
            "../../src/jscrollview.h",
            "../../src/jscrollview.cpp",
            "../../src/jfftview.cpp",
            "../../src/jfftview.h",
            "../../src/jwaitanimation.h",
            "../../src/jwaitanimation.cpp",
            "../../src/jexpander.h",
            "../../src/jexpander.cpp",
            "../../src/jicon.cpp",
            "../../src/jicon.h",
        ]

        of.addons: [
        ]

        // additional flags for the project. the of module sets some
        // flags by default to add the core libraries, search paths...
        // this flags can be augmented through the following properties:
        of.pkgConfigs: []       // list of additional system pkgs to include
        of.includePaths: ["../../src"]     // include search paths
        of.cFlags: []           // flags passed to the c compiler
        of.cxxFlags: []         // flags passed to the c++ compiler
        of.linkerFlags: []      // flags passed to the linker
        of.defines: []          // defines are passed as -D to the compiler
                                // and can be checked with #ifdef or #if in the code
        of.frameworks: []       // osx only, additional frameworks to link with the project
        of.staticLibraries: []  // static libraries
        of.dynamicLibraries: ["sndfile"] // dynamic libraries

        // other flags can be set through the cpp module: http://doc.qt.io/qbs/cpp-module.html
        // eg: this will enable ccache when compiling
        //
        // cpp.compilerWrapper: 'ccache'

        Depends{
            name: "cpp"
        }

        // common rules that parse the include search paths, core libraries...
        Depends{
            name: "of"
        }

        // dependency with the OF library
        Depends{
            name: "openFrameworks"
        }
    }

    property bool makeOF: true  // use makfiles to compile the OF library
                                // will compile OF only once for all your projects
                                // otherwise compiled per project with qbs
    

    property bool precompileOfMain: false  // precompile ofMain.h
                                           // faster to recompile when including ofMain.h 
                                           // but might use a lot of space per project

    references: [FileInfo.joinPaths(of_root, "/libs/openFrameworksCompiled/project/qtcreator/openFrameworks.qbs")]
}
