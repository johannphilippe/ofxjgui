#include "ofMain.h"
#include"ofApp.h"
#include"ofAppNoWindow.h"
#include"jgui.h"
//========================================================================
#include"sndfile.hh"
#include"Dependencies/AudioFFT/AudioFFT.h"

/*
 * ----------------- Complete example ----------------------
 *  This example demonstrate the use of ofxJGUI, showing an example on all the widgets available.
*/


// a simple audio simulation used by oscilloscopes.
void updateAudio(vector<double> &audio_sig, vector<double>&sig2, float *radius){
    for(size_t i = 0; i < audio_sig.size(); i++)
    {
        float idx = (float)i / audio_sig.size();
        audio_sig[i] = sin(M_PI * idx * (*radius) * 0.001) * 0.4;
        (*radius) += 1;
        sig2[i] = sin(M_PI * idx * 0.03 * ((*radius) * 0.0001)) * 0.5;
    }
}

int main( ){

    // simple button
    JButton button("helloworld");
    button.setColor(ofColor(JCOLOR_COOLPURPLE));
    JButton b2("hi there");
    b2.setColor(ofColor(JCOLOR_COOLRED));

    // simple slider. Can be constructed with orientation argument as well `JSlider slider(JOrientation::Vertical, 30, 150);`
    JSlider vertSlider(50, 200);
    vertSlider.setOrientation(JOrientation::Vertical);
    vertSlider.setDisplayValue(true);
    vertSlider.setFillColor(ofColor(JCOLOR_COOLRED));
    JSlider horSlider;
    horSlider.setOrientation(JOrientation::Horizontal);
    horSlider.setDisplayValue(true);

    // Fill widget, filling some space.
    JFill fill;

    JButton b3("Button3");
    b3.sigButtonPressed.connect([&](){
       ofFileDialogResult res =  ofSystemSaveDialog("save", "save something");
    });

    JButton b4("but 4");

    vector<double> audio_sig;
    vector<double> sig2;
    audio_sig.resize(1024);
    sig2.resize(1024);

    // Oscilloscope able to draw multiple audio (or else) 2D signals
    JMultiScope<double> scope(1000, 400);
    scope.addSignal(JScopedSignalDescriptor<double>("sine" , &audio_sig, ofColor(JCOLOR_RED) ));
    scope.addSignal(JScopedSignalDescriptor<double>("sqrt" , &sig2, ofColor(JCOLOR_BLUE) ));
    scope.setGridtype(JGridType::Full);
    scope.setLegend(true, JCornerPosition::BottomRight);

    // JSpin is the equivalent of a number box in Max MSP, or SpinBox in IUP. It is a number box with scroll and buttons to change it
    // User can define its default value, its min and max, and its step.
    JSpin<int> intSpin(1, 1);
    JSpin<double> doubleSpin(1.0, 0.5, 10.0, 0.5);

    // Curve editor is the same as in jo_tracker. It allows to edit curves (3 modes  : linear - log - exp, bezier, or spline)
    JCurveEditor plot(500, 300);
    plot.setGridType(JGridType::Full);

    JButton linearBut("Linear", 100, 40);
    JButton bezierBut("Bezier", 100, 40);
    JButton splineBut("Spline", 100, 40);
    linearBut.setColor(ofColor(100, 255, 0));
    bezierBut.setColor(ofColor(JCOLOR_DARKGREY));
    splineBut.setColor(ofColor(JCOLOR_DARKGREY));

    linearBut.sigButtonReleased.connect([&](){
        linearBut.setColor(ofColor(100, 255, 0));
        bezierBut.setColor(ofColor(JCOLOR_DARKGREY));
        splineBut.setColor(ofColor(JCOLOR_DARKGREY));
        plot.setMode(JCurveMode::LinearLogExp);
    });
    bezierBut.sigButtonPressed.connect([&](){
        bezierBut.setColor(ofColor(255, 100, 0));
        linearBut.setColor(ofColor(JCOLOR_DARKGREY));
        splineBut.setColor(ofColor(JCOLOR_DARKGREY));
        plot.setMode(JCurveMode::Bezier);
    });
    splineBut.sigButtonPressed.connect([&](){
        linearBut.setColor(ofColor(JCOLOR_DARKGREY));
        bezierBut.setColor(ofColor(JCOLOR_DARKGREY));
        splineBut.setColor(ofColor(100, 0, 255));
        plot.setMode(JCurveMode::Spline);
    });

    //plot.curve_mode = JCurveMode::Bezier;

    for(int i = 0; i < 10; i++)
    {
        plot.samples_positions.push_back(glm::vec3((float)i/ 10.0, (float)i / 10.0, 0));
    }

    // Containers are vertical or horizontal layouts able to contain multiple children. They adjust their width height to children.
    JContainer modeContainer(JOrientation::Vertical, &linearBut, &bezierBut, &splineBut);

    // Pretty button is a proof of concept to show how animation could work. It is a simple button progressively changing its color when clicked.
    JPrettyButton pbut("Click me <3");
    pbut.setColors(ofColor(JCOLOR_COOLRED), ofColor(JCOLOR_BLUE), ofColor(JCOLOR_WHITE));
    pbut.setAnimationTimeMs(300);
    pbut.setAnimate(true);

    // Text input base : a single line text input (no font size or font seleciton)
    JTextInputBase text_field;
    // Text input allows to do multiline text, with custom font and font size.
    JTextInput multiline(ofTrueTypeFontSettings("verdana.ttf", 13), JMultilineMode::Fixed);
    multiline.setNumberOfLines(4);
    multiline.handle_name = "multiline";

    // Two state toggle.
    JToggle toggle;
    toggle.setSelectedColor(ofColor(JCOLOR_COOLPURPLE));

    // Multi state toggle with different colors.
    JMultiToggle multi_toggle({ofColor(JCOLOR_COOLRED), ofColor(JCOLOR_COOLPURPLE), ofColor(JCOLOR_GREEN), ofColor(JCOLOR_BLUE)});

    //scrollview
    JCurveEditor nce(600, 600);
    JButton scbut("scrolled button");
    JButton scbut2("scrolled button2");
    nce.setGridType(JGridType::Full);

    // JScrollview is a scrolling view. It can be associated with a scrollbar.
    JScrollView sv(600, 200, JOrientation::Vertical,  &scbut, &scbut2, &nce);

    JScrollBar bar(JOrientation::Vertical, 30, 200);
    sv.setScrollBar(&bar);


    JSlider scroll_slid(50, 200);
    scroll_slid.setOrientation(JOrientation::Vertical);
    scroll_slid.setDisplayValue(true);

    scroll_slid.sigSliderValue.connect([&](double v)
    {
       sv.scrollToIndex(1.0 - v);
    });

    scbut.sigButtonPressed.connect([&](){
       sv.scrollToChild(&nce);
    });

    scbut2.sigButtonPressed.connect([&](){
        float index = (rand() % 100) / 100.0;
        cout << "index :: " << index << endl;
        sv.scrollToIndex(index);
    });

    cout << "WIDTHS // <> >>> >> >> >> " << button.width << " "  << b2.width << " " << horSlider.width << " " << pbut.width <<
            " " << toggle.width << " " << multi_toggle.width << " " << text_field.width << " " << multiline.width << endl;

    JContainer cont1(JOrientation::Horizontal , &button, &b2 , &horSlider ,  &pbut , &toggle , &multi_toggle  , &text_field, &multiline );
    cont1.handle_name = "cont1";
    JExpander expander(JOrientation::Vertical, &cont1);
    expander.handle_name = "expander";

    JIcon icon = JIcon::Play();
    JIconButton icon_button(JIcon::Play(), 40, 40);
    icon_button.setRadius(10);
    icon_button.setColor(ofColor::green);

    bool play = false;

    icon_button.sigButtonPressed.connect([&](){
       if(play)
       {
           icon_button.setIcon(JIcon::Play());
           icon_button.setColor(ofColor::green);
       } else
       {
           icon_button.setIcon(JIcon::Square());
           icon_button.setColor(ofColor::red);
       }
       play = !play;
    });

    JContainer cont2(JOrientation::Horizontal, &b3, &b4, &intSpin, &doubleSpin, &fill, &plot, &modeContainer, &vertSlider, &icon_button);
    JContainer cont3(JOrientation::Horizontal, &scope, &sv, &bar, &scroll_slid);
    JContainer vcontainer(JOrientation::Vertical, &expander, &cont2, &cont3);


    // Dialog is the main component of ofxJGUI. It is like windows in openframeworks : it handles an OF window, and runs it internally as an app
    // This allows its setup/update/draw methods to be triggered by OF, and to propagate it to its child
    JDialog mainDial(JDialogSettings("Main dialog", OF_WINDOW, glm::vec2(1920, 1080), glm::vec2(0, 0)), &vcontainer, true);


    shared_ptr<JToastBase> toast(new JToastBase("fps : ", 1500, 10, 800) );

    b2.sigButtonPressed.connect([&](){
        toast->text = "fps : " + ofToString(ofGetFrameRate());
        toast->chrono.start();
        mainDial.showToast(toast);
    });

    float radius = 0;
    mainDial.sigPreUpdate.connect([&](){
       updateAudio(audio_sig, sig2, &radius);
    });

    cout << "creating &mainbut" << endl;
    JButton mainbut("&mainbut");
    JButton tbut("tbut");
    JSpin<int> dspin(13, 10, 15);

    //a simple knob, with custom min and max.
    JKnob knob(0.0, 10.0, 60, 60);
    knob.setColor(ofColor(JCOLOR_BLUE));

    cout << " mainbut address : " << &mainbut << endl;
    JContainer cont = JContainer::Centered(JOrientation::Horizontal, glm::vec2(500, 50), &tbut, &mainbut);
    cont.handle_name = "cont";
    cont.setGap(25);
    JContainer vcont(JOrientation::Vertical, &cont, &dspin, &knob);
    vcont.setGap(25);
    cout << "adding &mainbut" << endl;
    JDialog dlg(JDialogSettings("second", OF_WINDOW, glm::vec2(500, 500), glm::vec2(100, 100)), &vcont);
    cout << "done &mainbut" << endl;

    button.sigButtonPressed.connect([&](){
        dlg.show();
    });
    tbut.sigButtonPressed.connect([&](){
       dlg.hide();
    });

    mainbut.sigButtonPressed.connect([&](){
       cout << "button pressed  " << endl;
    });


    ofColor new_color;
    ofColor old_color;
    // Animation is a simple object that will be called in a dialog update method. It will perform a phasor (0.0 to 1.0) operation in
    // specified duration, and call a lambda/function with the current phasor value.
    JAnimation animation = JAnimation::Logarithmic([&](double v) {
        const double inv = 1 - v;
        const int r = (old_color.r * inv) + (new_color.r * v);
        const int g = (old_color.g * inv) + (new_color.g * v);
        const int b = (old_color.b * inv) + (new_color.b * v);
        ofColor itp(r, g, b);
        b4.setColor(itp);
    }, 3000);

    shared_ptr<JAnimation> a_ptr(&animation);
    mainDial.registerAnimation(a_ptr);


    // A static FFT view, allowing to represent spectrum of a soundfile.
    JFFTView<float>fft_view( 1800, 800);
    cout << "FFT VIew created, drawing fbo... " << endl;

    fft_view.mode = JFFTDisplayMode::Linear;

    JButton f_linearBut("Linear", 100, 40);
    JButton f_logBut("Log", 100, 40);
    JButton f_barkBut("Bark", 100, 40);

    JButton f_changeFile("Select file" , 100, 40);
    JFill f_fill(30, 10);

    f_linearBut.setColor(ofColor(100, 255, 0));
    f_logBut.setColor(ofColor(JCOLOR_DARKGREY));
    f_barkBut.setColor(ofColor(JCOLOR_DARKGREY));

    // Connect buttons to actions
    f_linearBut.sigButtonReleased.connect([&](){
        if(fft_view.isWaiting()) return;
        f_linearBut.setColor(ofColor(100, 255, 0));
        f_logBut.setColor(ofColor(JCOLOR_DARKGREY));
        f_barkBut.setColor(ofColor(JCOLOR_DARKGREY));
        fft_view.setMode(JFFTDisplayMode::Linear);

    });
    f_logBut.sigButtonPressed.connect([&](){
        if(fft_view.isWaiting()) return;
        f_logBut.setColor(ofColor(255, 100, 0));
        f_linearBut.setColor(ofColor(JCOLOR_DARKGREY));
        f_barkBut.setColor(ofColor(JCOLOR_DARKGREY));
        fft_view.setMode(JFFTDisplayMode::Log);
    });
    f_barkBut.sigButtonPressed.connect([&](){
        if(fft_view.isWaiting()) return;
        f_linearBut.setColor(ofColor(JCOLOR_DARKGREY));
        f_logBut.setColor(ofColor(JCOLOR_DARKGREY));
        f_barkBut.setColor(ofColor(100, 0, 255));
        fft_view.setMode(JFFTDisplayMode::Bark);
    });

    JContainer f_buts = JContainer::Centered(JOrientation::Horizontal, glm::vec2(1920, 40), &f_linearBut,  &f_logBut,  &f_barkBut,  &f_changeFile);
    // Gap attribute defines spacing between children
    f_buts.setGap(5);
    JContainer fft_cont = JContainer::Centered(JOrientation::Horizontal, glm::vec2(1920, 800), &fft_view);
    JContainer f_cont = JContainer::Centered(JOrientation::Vertical, glm::vec2(1920, 1080), &fft_cont, &f_buts);
    f_cont.setGap(30);

    JDialog fft_dial(JDialogSettings("fft_view", OF_WINDOW, glm::vec2(1920, 1080), glm::vec2(10, 10)), &f_cont);

    b4.sigButtonPressed.connect([&](){
        cout << "start animation " << endl;
        new_color = JUtils::JColor::getRandomColor();
        old_color = b4.color;
        animation.start();
        fft_dial.show();
        //fft_view.drawFbo();
    });

    f_changeFile.sigButtonPressed.connect([&](){
        ofFileDialogResult res = ofSystemLoadDialog("Choose soundfile", false, "/home/johann/Documents/tmp");
        if(res.bSuccess) {
            fft_view.setSoundfile(res.filePath, 2048, 2);
        }
    });


    cout << "main thread id : " << std::this_thread::get_id() << endl;




    // The ofRunMainLoop must be called after JGUI widgets have been created and customized.

    ofRunMainLoop();
   // JCore::MainLoop();

}
