#pragma once

#include "ofMain.h"
#include"jgui.h"

class ofApp : public ofBaseApp{

public:
    ofApp();

    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    JButton button;
    JButton b2;
    JSlider vertSlider, horSlider;
    JFill fill;

    JButton b3, b4;
    JContainer cont1, cont2, cont3;

    vector<double> audio_sig;
    vector<double> sig2;

    JMultiScope<double> scope;

    JSpin<int> intSpin;
    JSpin<double> doubleSpin;


    void updateAudio();
    int radius;

    JContainer vcontainer;
    JCurveEditor plot;

    JButton linearBut,bezierBut;
    JContainer modeContainer;

    JPrettyButton pbut;
    JTextInputBase text_field;

    JTextInput multiline;
    JToggle toggle;
    JMultiToggle multi_toggle;

    JButton dial_button;
    JDialog dialog;

};
