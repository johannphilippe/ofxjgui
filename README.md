# ofxJGUI

ofxJGUI is a light C++ GUI system for musical applications based on OpenFrameworks. 

## Install
To install it, you will need to have OpenFrameworks setup. You can clone or download this repository, and copy it to you OF addons directory. 
You will need [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) to be installed on your system. On Linux this can be done by running `sudo apt install libeigen3-dev` in the command line.

You can then setup a new OF project with the project generator or with OF QtCreator project generator. You need to set ofxJGUI as an addon to your project (in the project generator, or in the .qbs file. Then you are ready.

Unlike usual OF projects, ofxJGUI can easily be called from main function. This is because JDialog system contains its own window, and calls `ofRunApp()`.

## Available widgets
Several widgets are available. 

### Base Widgets

- JWidget : abstract class. Base for all widgets.
- JThreadedWidget : base for all threaded widgets.
- JWaitAnmation : animation for threaded widgets. 


### High level widgets

- JButton : A simple text button.
- JPrettyButton : an animated button changing color when clicked.
- JIconButton : a small button drawing an icon
- JSlider : a simple slider scaled between 0 and 1.
- JContainer : vertical or horizontal container, able ton contain multiple children.
- JSimpleScope : display one audio signal.
- JMultiScope : display several signals. Templated to allow double or float precision.
- JPlot : A simple plotting system, used as a base for curve editor
- JCurveEditor : draw and edit 2D curves (linear, log exp, bezier, cubic spline...)
- JFill : spacing with static size.
- JToastBase : base for a toast. Able to temporarily print a message on screen.
- JTextInputBase : simple text edit (single line, not custom font) using ofDrawBitmapString
- JTextInput : a simple text edit with font selection, font size, colors... Using ofTrueTypeFont.
- JToggle : a two states toggle
- JMultiToggle : toggle with user defined number of states.
- JAnimation : an animation system based on an iteration between 0 and 1. Can be linear, Exp, or Log.
- JSpin : a template with a number box (variable number with + and - buttons).
- JKnob : a rotary encoder with limits.
- JScrollView : a container (can contain multiple childs) with fixed size that can be scrolled vertically or horizontally.
- JScrollBar : a scrollbar that must be passed to scrollview with JScrollView::setScrollBar(JScrollbar *bar) method.
- JDialog : base container, can contain one child (usually any another container that is not a dialog). 
- JFFTView : an offline (non-realtime) FFT view. It needs two matrices (vector of vector) of FFT data (real and imaginary).
- JIcon : drawn icon. Some basic icons with static constructors (Square, Play, Pause...). But it can be user defined with a lambda.

## Simple button
```cpp
#include "jgui.h"

int main()
{
	float width, height, x, y;
	width = 150;
	height = 40;
	// first create a widget
	JButton button("Hello world!", width, height);
	
	// Then create dialog settings
	// JDialogSettings internally handles a ofGLFWWindowSettings object
	JDialogSettings settings("my_dialog_name", OF_WINDOW, glm::vec2(1920, 1080), glm::vec2(0, 0));
	
	// Put your widget in the dialog
	// last parameter - bool - is "should of open this window as soon as possible ?" 
	JDialog dial(settings, &button, true);

	// Optionnaly, you can connect your button to an action
	// Action can be a lambda or a function with the same 
	// signature as the signal. Button pressed signal has no arguments
	// and no return value.
	button.sigButtonPressed.connect([&](){
	// Toast is a temporary message, here "hello there" 
	// displayed for 2000 ms, at center of the window.
		dial.showToast("hello there", 2000, 
			1920 / 2, 1080 /2);
	});	

	// Then start OF main loop
	ofRunMainLoop();

	return 0;
}
```

## Reference
Reference is not completed yet. 

## Examples
Several examples are in the example folder : 

- Complete example : it shows all widget's behavior.
- Curve editor : a Curve editor, based on the one I wrote in [jo_tracker](https://framagit.org/johannphilippe/jo_tracker-3.0).
- Fuck Button : a funny button. Try it and see. 

## Libraries

This repository includes a few source codes from other repositories : 

- [AudioFFT](https://github.com/HiFi-LoFi/AudioFFT) - MIT License
- [nlohmann-json](https://github.com/nlohmann/json) - MIT License
- [lsignal](https://github.com/cpp11nullptr/lsignal) - MIT License
- [libInterpolate](https://github.com/CD3/libInterpolate) - MIT License
- [ofxPixelsShape](https://github.com/kashimAstro/ofxPixelsShape.git) - OF addon - Unlicensed

## Dependencies

The libInterpolate library, included in the repository, requies [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) library to work. You will need to install in order to use some of ofxGUI widgets.

## License 
This work is licensed under MIT.  

