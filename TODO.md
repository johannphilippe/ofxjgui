# ofxJGUI

Openframeworks addon for GUI drawing . It contains basic GUI elements, and musical specific GUI elements.

## Missing Examples

- A real audio example
- Simple audio player with timeline

## Working widgets

- JButton : A simple text button.
- JPrettyButton : an animated button changing color when clicked.
- JSlider : a simple slider scaled between 0 and 1.
- JContainer : vertical or horizontal container, able ton contain multiple children. 
- JSimpleScope : display one signal.
- JMultiScope : display several signals. Templated to allow double or float precision.
- JPlot : base for curve editor
- JCurveEditor : draw and edit curves (linear, log exp, bezier, cubic spline...)
- JFill : spacing with static size.
- JTextInputBase : simple text edit
- JTextInput : a simple text edit with font selection, font size, colors...
- JToggle : a two states toggle
- JMultiToggle : toggle with user defined number of states.
- JAnimation : an animation system based on an iteration between 0 and 1. Can be linear, Exp, or Log.
- JSpin : a template with a number box (variable number with + and - buttons). 
- JScrollView : a container (can contain multiple childs) with fixed size that can be scrolled vertically or horizontally.
- JScrollBar : a scrollbar that must be passed to scrollview with JScrollView::setScrollBar(JScrollbar *bar) method.
- JDialog : base container, can contain one child (usually any another container that is not a dialog). 
- JFFTView : FFT View offline. Need to move the FFT calculation inside class (?), with a threading model.
- JIcon : drawn icon. Some basic icons with static constructors (Square, Play, Pause...). But it can be user defined with a lambda.

## To do


### Improve 

- !!! JContainer :: We must think three situations : Container has children height and its own position, container has its own height and can center, 
I must think about : 
- How does the container knows full dialog size  (if constraint) ? 
- Also make container inherit containerbase for more simplicity

- Container situation : 
    - Container only draws its children, adapting its size to children
    - Container has a different size, and can center (or constrain)
    - Container can understand a resize situation ? 
    - Minimum size thing ? 
    - All must be relative then...


- Probably remove the icon system

- JScrollbar : write updatechildrenPos method (override) -- already written with empty body, see if it is useful to write...

- Improve global mode with extern values that can be modifier (line color for borders, bool for show_border, default line width, default radius ...)
- Or add a basic settings Type/Class that can be passed to every children and that will be interpreted by all of them

- JExpander : create horizontal mode (draw)
- JFFTView : add grid (for different modes, with frequency scaling)
- JFFTView : multi channel
- Add callbacks to dialog (window resized etc)
- Add a constraint system (a constrained container, that will resize all children to fit available size, and constrain them to it).
- JButton (add image, and icon only button). 
- JScope(s) : scaling (currently -1, 1), grid step, and grid values display
- JScope(s) : add changing window size (if block size is small, allow to have a different window size than the signal vector size  ) 
- JScope(s) : sync mode (sync to signal ...)
- JSlider (add new types of slider, with scaling, and other attributes) . Possibly a different slider with a simple bar (?). 

- JTextInput : many bugs... A mode to write (auto resize)

- Proper documentation with several examples
- A widget creation guide (what is necesssary in a widget, for example methods, and remove listeners...)

- Use JCore system as a global variable holder. For example, it could contain the variable `extern bool use_theme`, so that every widget check if it should use a theme. 
- Remove JCore ? see in what it can still be useful

- Performance : 14 fps less when opening a new dialog  (seems not related to ofxGui but to Dialogs sizes)

### Missing Widgets

#### Basics

- Icon Button (with icon system, and likely a script to convert an icon set to .h representation (base64, ofPixels or anything else) ).
- Constrained container
- Matrix (toggle matrix, and mixage matrix, knob matrix (or spinbase matrix) )
- Spreadsheet - tracker base
- Tree (file tree ?)
- Label
- TextField
- Range Slider
- Radio (mutual exclusive toggle)

#### Audio

- LevelMeter (with calibration, colors, and DB modes, etc...)
- FFT View (realtime)
- Waveform (offline)
- The incredible Plot system
