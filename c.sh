#!/bin/bash
value=$(convert play.png -alpha extract -format "%[fx:mean]" info:)

or

value=$(convert play.png -alpha extract -scale 1x1! -format "%[fx:u]" info:)

if [ "$value" = 1 ]; then
echo "opaque"
else
echo "transparent"
fi

